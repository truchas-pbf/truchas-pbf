// NIST single trace demo problem
//
// This problem yses In625 material properties and process parameters from
// the NIST single trace demo problems.
//
// Model is posed in mm, g, ms, kelvin units.
//
// Conversion factors from MKS units:
//   1 Joule [kg m^2 / s^2] = 1e3 g mm^2 / ms^2
//   1 Watt [J/s] = 1 [g mm^2 / ms^3]
//   1 W/m^2 = 1e-6 [g mm^2 / s^3 mm^2]
//   1 N [kg m / s^2] = 1 g mm / ms^2
//   1 Pa [N/m^2] = 1e-6 (g-mm/ms^2)/mm^2
//   1 Pa-s = 1 kg / m s = 1e-3 g/mm-ms

{
   "physical-constants": {
      "stefan-boltzmann": 5.67e-14
   },
   "mesh": {
      "num-levels": 5,
      "lo": [0,0,0,    2,4,4,    8,12,12,  20,30,30,  46,64,66],
      "hi": [19,9,4,  37,15,9,  71,27,19, 139,49,39,  273,95,79],
      "box-size": 8,
      "prob-lo": [-0.8, -0.4, -0.4],
      "prob-hi": [0.8, 0.4, 0.0]
   },
   "laser-scan-path": {
      "laser": {
         "type": "gaussian",
         "power": 137.9, // [W]
         "sigma": 0.019  // [mm]
      },
      "laser-absorp": 0.4,
      "laser-time-constant": 0.01,
      "scan-path": {
         "start-coord": [-0.50, 0.0],
         "command-file": "single-trace-path.json"
      },
      "write-plotfile": true,
      "plotfile-dt": 0.02 // [ms]
   },
   "model": {
       "material": {
          "density": 7.57e-3, // [g/mm^3]
          "solid": {
              "conductivity": { // [(g-mm^2/ms^3)/mm-K]
                 "type": "polynomial",
                 "poly-coef": [12.3e-3, 1.472e-5],
                 "poly-powers": [0, 1],
                 "poly-center": 273.0
              },
              "specific-heat": { // [(g-mm^2/ms^2)/g-K]
                 "type": "polynomial",
                 "poly-coef": [428.38, 0.23638],
                 "poly-powers": [0, 1],
                 "poly-center": 273.0
              }
          },
          "liquid": {
              "conductivity": { // [(g-mm^2/ms^3)/mm-K]
                 "type": "polynomial",
                 "poly-coef": [8.92e-3, 1.474e-5],
                 "poly-powers": [0, 1],
                 "poly-center": 273.0
              },
              "specific-heat": 750.65 // [(g-mm^2/ms^2)/g-K]
          },
          "solidus-temp":  1410.0, // [K] adjusted downward to "match" Scheil model
          "liquidus-temp": 1616.0, // [K]
          "latent-heat": 2.1754e5  // [g-mm^2/ms^2/g]
       },
       "heat": {
          "discretization": "finite-volume",
          "bc": {
             "top": {
                "type": "flux",
                "sides": ["zhi"],
                "data": "laser"
             },
             "top-rad": {
                "type": "radiation",
                "sides": ["zhi"],
                "emissivity": 0.6,
                "ambient-temp": 298.0
             },
             "top-evap": {
                 "type": "evaporation",
                 "sides": ["zhi"],
                 "prefactor": 3.38e10,
                 "temp-exponent": -0.5,
                 "activation-energy": 3.72e5
             },
             "adiabatic": {
                "type": "flux",
                "sides": ["xlo", "xhi", "ylo", "yhi", "zlo"],
                "data": 0.0
             }
          }
       },
       "disable-flow": true
    },
    "solver": {
        "microstructure": { // optional sublist
           "gv": {"theta1": 0.05, "theta2":0.95, "theta":0.10} // optional
        },
        "heat": {
           "temp-rel-tol": 1.0e-2,
           "num-cycles": 2,
           "nlk-max-itr": 5,
           "nlk-tol": 0.01
        }
    },
    "sim-control": {
       "initial-time": 0.0,
       "initial-time-step": 1.0e-6,
       "min-time-step": 1.0e-9,
       "output-times": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0,
                        1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0,
                        2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0]
    },
    "initial-temperature": {
       "type": "constant",
       "value": 298.0
    },

    // This sublist is consumed by AMREX_INIT
    "amrex": {
       "amrex": {"fpe_trap_invalid": 1}
    }
}
