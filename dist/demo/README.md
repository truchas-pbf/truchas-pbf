To run the demo, first add the binary directory containing the Truchas-PBF
executable and MPI programs to your path
```shell
$ export PATH=../bin:$PATH
```
and run
```shell
$ mpiexec -np 12 tpbf single-trace-demo.json
```
Set the number of MPI processes to use (here 12) as desired. This takes
about 5 minutes on a 12 core AMD Threadripper 2920X.

This creates a numbered sequence of `plt*` subdirectories each containing
the computed solution at one of the output times. This can be visualized
using paraview.

Under the paraview File/Open dialog those output directories are shown
collapsed as a single line item "plt..". Highlight that sequence and press
"OK". You'll then be presented with another dialog to select a reader. The
correct reader "AMReX/BoxLib Grid Reader" should already be selected; press
"OK". Next, on the left panel select the cell arrays you want to load --
probably just liquid (liquid volume fraction) and temp (temperature). Just
above where you made those selections is a "Level" slider. This sets the
maximum grid level for which data will be loaded. By default this is only
set to 1.  You'll want to set this to at least 4 to load all the data (the
demo mesh has 5 levels, which paraview numbers starting with 0). Then finally
press the green "Apply" button to load the data. The displayed field, display
style and choice of time snapshot are selected near the top of the paraview
window as usual.

The folder `plt-gv` contains the recorded GV solidification data as of the
end of the simulation and can be opened in paraview as well.
