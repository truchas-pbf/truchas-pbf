#!/usr/bin/env python

import os

def run(x):
    r = os.system(x)
    if r != 0:
        raise Exception("The command '%s' failed." % x)

def copy_deps(binary_executable):
    os.system("""\
    ldd %s \
        | grep "=>" \
        | sed -e '/^[^\t]/ d' \
        | sed -e 's/\t//' \
        | sed -e 's/.*=..//' \
        | sed -e 's/ (0.*)//' \
        | grep -v -e '^$' \
        > ldd_output
    """ % binary_executable)

    for l in open("ldd_output").readlines():
        lib = l.strip()
        filename = os.path.basename(lib)
        print("Direct dependency %s" % filename)
        os.system("cp %s %s/lib" % (lib, dist))
        lib = "%s/lib/%s" % (dist, filename)
        os.system("patchelf --set-rpath '$ORIGIN/.' %s" % lib)
        os.system("""\
        ldd %s \
            | grep "=>" \
            | sed -e '/^[^\t]/ d' \
            | sed -e 's/\t//' \
            | sed -e 's/.*=..//' \
            | sed -e 's/ (0.*)//' \
            | grep -v -e '^$' \
            > ldd_output
        """ % lib)
        for m in open("ldd_output").readlines():
            lib2 = m.strip()
            filename2 = os.path.basename(lib2)
            print("    Indirect dependency %s" % filename2)
            os.system("cp %s %s/lib" % (lib2, dist))
            lib2 = "%s/lib/%s" % (dist, filename2)
            os.system("patchelf --set-rpath '$ORIGIN/.' %s" % lib2)

this_dir = os.path.dirname(os.path.abspath(__file__))
root_dir = os.path.realpath(this_dir + "/..")
mpich_root = "/home/swuser/ext"
dist = "truchas-pbf"

run("rm -rf %s" % dist)
run("mkdir -p %s/bin" % dist)
run("mkdir -p %s/lib" % dist)

run("cp src/tpbf/tpbf %s/bin" % (dist))
run("cp %s/bin/mpiexec.hydra %s/bin/mpiexec" % (mpich_root, dist))
run("cp %s/bin/hydra_pmi_proxy %s/bin" % (mpich_root, dist))
run("cp -a %s/demo %s" % (this_dir, dist))

for b in ["tpbf", "mpiexec", "hydra_pmi_proxy"]:
    copy_deps("%s/bin/%s" % (dist, b))
    os.system("patchelf --set-rpath '$ORIGIN/../lib' %s/bin/%s" % (dist, b))

# Remove libraries that we need to use from the system
#for lib in ["libc", "libm", "libpthread", "libdl", "librt"]:
for lib in ["libc", "libm", "libpthread", "librt"]:
    run("rm %s/lib/%s.so.*" % (dist, lib))

run("tar zcf %s.tar.gz %s" % (dist, dist))
run("tar ztf %s.tar.gz" % dist)


