\documentclass{article}

\usepackage{listings}
\lstset{language=Fortran}

\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{array}
\usepackage[margin=1in]{geometry}

\def\Dpartial#1#2{ \dfrac{\partial #1}{\partial #2} }
\def\Dparttwo#1#2{ \dfrac{\partial^2 #1 }{ \partial #2^2} }
\def\Dnorm#1#2{ \dfrac{d #1 }{ d #2} }
\def\Dnormtwo#1#2{ \dfrac{d^2 #1 }{ d #2 ^2} }
\def\Dtotal#1#2{ \dfrac{D #1 }{ D #2} }
% continuous vector operator
\def\cvo#1{\mbox{{\boldmath $\mathcal{#1}$}}}
% discrete vector operator overline
\def\dvo#1{\mbox{{\boldmath $#1$}}}
\def\jump#1{ [ #1 ]_\Gamma }
\def\bigO#1{\mathcal{O}\left( #1 \right)}

\def\div{\nabla\cdot}
\def\divc{\nabla^c\cdot}
\def\divg{\nabla^{c,g}\cdot}
\def\grad{\nabla}
\def\gradf{\nabla^f}
\def\Dx{\Delta x}
\def\Dy{\Delta y}
\def\Dz{\Delta z}
\def\Dt{\Delta t}
\def\dP{\delta P}
\def\fluid#1{\hat{ #1 }}
\def\from{\leftarrow}
\def\cv{\Omega}
\def\fv{\partial\cv}
\def\dV{\delta V}

\begin{document}
This document describes aspects of the fluid algorithm to be implemented on a Cartesian mesh.  In general, familiarity with the Truchas Physics and Algorithms manual is assumed and information contained therein will not be repeated (mostly).

\section{Mesh}

To simplify the descriptions, we'll use the following conventions,
\begin{itemize}
\item Cell-centered quantities will be denoted by a $c$ superscript.  A cell centered quantity at location $i,j,k$ is: $\phi^c_{ijk}$.
\item Face-centered quantites will be denoted by an $x$, $y$, or $z$ superscript.  In Amrex speak, these are quantites which are nodal in $x$, nodal in $y$ and nodal in $z$.
\item If a cell-centered quantity is defined on a index space $\dvo{I^c} = (i,j,k)$ for $i \in [i_s,i_e)$, $j \in [j_s,j_e)$ and $k \in [k_s,k_e)$, then a face based quantity in $x$ (for example) is defined on $\dvo{I^x} = (i,j,k)$ for $i \in [i_s, i_e]$, $j \in [j_s,j_e)$ and $k \in [k_s,k_e)$. In perhaps simpler terms, a cell-centered quantity $\phi^c_{ijk}$ is bounded by face-centered quantities $\phi^x_{ijk}, \phi^x_{i+1,jk}$, $\phi^y_{ijk}, \phi^y_{i,j+1,k}$, $\phi^z_{ijk}$, $\phi^z_{ij,k+1}$.  In an ``absolute'' index space, face-centered quantities exist at half indices with cell-centered quantities at whole indices.
\end{itemize}


\section{General Considerations}

The algorithms in truchas are written in a finite-volume formulation suitable for general unstructured meshes.  Broadly, one can write the momentum update for a control volume, $\Omega$, bounded by faces, $\partial \Omega$, with outward facing normals, $\hat{n}$ as:
\begin{align}
  \Dpartial{}{t}\int_\Omega \rho u_l d\Omega = \int_{\partial \Omega} \dvo{F_l} \cdot \hat{n} d\partial\Omega,
\end{align}
where $\dvo{F}$ represents the sum of all forces on a given face.  On Cartesian meshes, the second (and lower) order discretizations of the finite volume formulations are identical to finite difference formulations. For example, using the typical second order midpoint approximation for cell and face values, the semi-discretization of the above (dropping the vector/tensor component subscript, $l$) is,
\begin{align}
  \Dpartial{}{t} (\rho u)^c_{ijk} = \dfrac{1}{\Omega}\left(\partial\Omega^x(F^x_{i+1,jk}-F^x_{ijk}) +
  \partial\Omega^y(F^y_{i,j+1,k}-F^y_{ijk}) +
  \partial\Omega^z(F^z_{ij,k+1}-F^z_{ijk})\right)
\end{align}
This is identical to second order finite difference approximation of
\begin{align}
  \Dpartial{}{t} (\rho u_l)^c = \div \dvo{F_l}
\end{align}
Things get weird when the fluid equations are not valid over all of a given control volume.  In this case, it is useful to represent a control volume as a union of three disjoint volumes associated with the fluid, $\Omega_f$, solid, $\Omega_s$, and void, $\Omega_v$ such that $\Omega = \cup\left(\Omega_f,\Omega_s,\Omega_v \right)$.  We can write the same for faces: $\fv^{x/y/z} = \cup\left(\fv^{x/y/z}_f,\fv^{x/y/z}_s,\fv^{x/y/z}_v  \right)$.  The fluid equations could then be written as,
\begin{align}
  \notag
  \dfrac{1}{\cv} \Dpartial{}{t} (\cv_f \rho u)^c_{ijk}  = \dfrac{1}{\cv}\big(
  & (\fv_f F)^x_{i+1,jk}-(\fv_f F)^x_{ijk} + \\
  \notag
  & (\fv_f F)^y_{i,j+1,k}-(\fv_f F)^y_{ijk} + \\
  \notag
  & (\fv_f F)^z_{ij,k+1}-(\fv_f F)^z_{ijk} + \\
  \label{eq:fluid_disc}
  & \fv_{f,s} F^n_{f,s} + \fv_{f,v} F^n_{f,v}\big),
\end{align}
where $\fv_{f,s}$ is area of the shared solid/fluid face and $F^n_{f,s}$ is the force on exerted on fluid by the solid.

An equivalent finite-difference approximation can be written down, but the approximations that truchas employs seem more intuitive using the above finite-volume formulation.

\begin{itemize}
\item The fluid volume fraction is $\cv_f/\cv$
\item The fluid area of a cell face is now dependent on position as in $\fv^x_{f,ijk}$.  The geometric transport algorithm handles this for the advection terms.
\item The corresponding evaluation of a flux $F^x_{f,ijk}$ (or any face-based quantity) should take place at the centroid of $\fv_f$ rather than the full $\fv$.  This is not done.
\item One can imagine that if $\cv_f$ becomes small, and likewise the $\fv_f$, the equations become ill-posed.  To avoid this, truchas employs a form of limiting.
\item All forces are applied at cell faces.  Body forces are first transferred to cell faces before they are applied.
\item The boundary condition, $F^n_{f,s}$, is ignored.
\end{itemize}

The initial implementation outlined below is for $\cv_v = 0$.


\section{Operators}
\subsubsection{Linear cell-to-face Interpolation}
Assuming the indexing conventions given in the previous section:
\begin{align}
  \phi^x_{ijk} = & L^{c,x} r^c_{ijk} \phi^c_{ijk} = \left(r^c_{i,j,k} \phi^c_{i,j,k}+ r^c_{i-1,j,k}\phi^c_{i-1,j,k}\right)/ \left(r^c_{i,j,k}+r^c_{i-1,j,k}\right) \\
  \phi^y_{ijk} = & L^{c,y} r^c_{ijk} \phi^c_{ijk} = \left(r^c_{i,j,k}\phi^c_{i,j,k}+r^c_{i,j-1,k}\phi^c_{i,j-1,k}\right)/ \left(r^c_{i,j,k}+r^c_{i,j-1,k}\right) \\
  \phi^z_{ijk} = & L^{c,z} r^c_{ijk}\phi^c_{ijk} = \left(r^c_{i,j,k} \phi^c_{i,j,k}+r^c_{i,j,k-1}\phi^c_{i,j,k-1}\right)/ \left(r^c_{i,j,k}+r^c_{i,j,k-1}\right)
\end{align}
where $r^c_{ijk}$ is some weight associated with cell $(ijk)$.  If no weight is given, assume $r^c = 1$.

\subsubsection{Harmonic cell-to-face Interpolation}
Assuming the indexing conventions given in the previous section:
\begin{align}
  \phi^x_{ijk} = & H^{c,x} \phi^c_{ijk} = \dfrac{2}{1/\phi^c_{i,j,k}+1/\phi^c_{i-1,j,k}} \\
  \phi^y_{ijk} = & H^{c,y} \phi^c_{ijk} = \dfrac{2}{1/\phi^c_{i,j,k}+1/\phi^c_{i,j-1,k}} \\
  \phi^z_{ijk} = & H^{c,z} \phi^c_{ijk} = \dfrac{2}{1/\phi^c_{i,j,k}+1/\phi^c_{i,j,k-1}}
\end{align}

\subsubsection{Cell-Centered Derivatives of Cell-Centered Quantities}
The components of the cell-centered gradient operator, $\nabla^{c,c} = [\nabla^{c,c}_{x},\nabla^{c,c}_{z},\nabla^{c,c}_{z}]$, are
\begin{align}
  \nabla^{c,c}_x \phi^c_{ijk} = &\dfrac{\phi^c_{i+1,j,k}-\phi^c_{i-1,j,k}}{2\Dx} \\
  \nabla^{c,c}_y \phi^c_{ijk} = &\dfrac{\phi^c_{i,j+1,k}-\phi^c_{i,j-1,k}}{2\Dy} \\
  \nabla^{c,c}_z \phi^c_{ijk} = &\dfrac{\phi^c_{i,j,k+1}-\phi^c_{i,j,k-1}}{2\Dz} \\
\end{align}

\subsubsection{Face-Centered Derivatives of Cell-Centered Quantities}
For face-centered derivatives evaluated at the $x$ face, $\nabla^{c,x} = [\nabla^{c,x}_{x},\nabla^{c,x}_{y},\nabla^{c,x}_{z}]$
\begin{align}
  \nabla^{c,x}_x \phi^c_{ijk} = & \dfrac{\phi^c_{ijk}-\phi^c_{i-1,j,k}}{\Dx} \\
  \nabla^{c,x}_y \phi^c_{ijk} = & \dfrac{\nabla^{c,c}_y\phi^c_{ijk}+\nabla^{c,c}_y\phi^c_{i-1,j,k}}{2}\\
  \nabla^{c,x}_z \phi^c_{ijk} = & \dfrac{\nabla^{c,c}_z\phi^c_{ijk}+\nabla^{c,c}_z\phi^c_{i-1,j,k}}{2}
\end{align}
For face-centered derivatives evaluated at the $y$ face, $\nabla^{c,y} = [\nabla^{c,y}_{x},\nabla^{c,y}_{y},\nabla^{c,y}_{z}]$
\begin{align}
  \nabla^{c,y}_x \phi^c_{ijk} = & \dfrac{\nabla^{c,c}_x\phi^c_{ijk}+\nabla^{c,c}_x\phi^c_{i,j-1,k}}{2} \\
  \nabla^{c,y}_y \phi^c_{ijk} = & \dfrac{\phi^c_{ijk}-\phi^c_{i,j-1,k}}{\Dy} \\
  \nabla^{c,y}_z \phi^c_{ijk} = & \dfrac{\nabla^{c,c}_z\phi^c_{ijk}+\nabla^{c,c}_z\phi^c_{i,j-1,k}}{2}
\end{align}
For face-centered derivatives evaluated at the $z$ face, $\nabla^{c,z} = [\nabla^{c,z}_{x},\nabla^{c,z}_{y},\nabla^{c,z}_{z}]$
\begin{align}
  \nabla^{c,z}_x \phi^c_{ijk} = & \dfrac{\nabla^{c,c}_x\phi^c_{ijk}+\nabla^{c,c}_x\phi^c_{i,j,k-1}}{2} \\
  \nabla^{c,z}_y \phi^c_{ijk} = & \dfrac{\nabla^{c,c}_y\phi^c_{ijk}+\nabla^{c,c}_y\phi^c_{i,j,k-1}}{2}\\
  \nabla^{c,z}_z \phi^c_{ijk} = & \dfrac{\phi^c_{ijk}-\phi^c_{i,j,k-1}}{\Dz}
\end{align}

\subsubsection{Linear average over all materials in a cell}
Assuming a quantity, $\phi_{m}$, associated with material $m$, and a material volume fraction for the material in a cell given by $f^c_{ijk,m}$, the material weighted linear average is given by:
\begin{align}
  \phi^c_{ijk} = & W \phi_m = \sum_m \phi_m f^c_{ijk,m}
\end{align}

\subsubsection{Linear average over fluids materials in a cell}
Assume $m_f$ are the subset of $m$ materials that are fluid:
\begin{align}
  \phi^c_{ijk} = & \fluid{W} \phi_m = \sum_{m_f} \phi_m f^c_{ijk,m}
\end{align}


\section{Coupling with VOF and Heat Transfer}
The time-split advancement scheme can be found in truchas source at \textit{truchas/src/truchas/drivers/drivers.F90:DRIVERS:CYCLE\_DRIVER}. This is outlined below.  All data at time level $n$ is available.
\begin{itemize}
\item The Vof algorithm is passed in face-based velocities, $u^{x,n}, v^{y,n}, w^{z,n}$, which it uses to compute flux volumes, $\dV^{x/y/z,n}_m$, and advance the vof field $f^{c,n}_m \to f^{c,\tau}_m$ using these flux volumes
\item The heat transfer algorithm updates the enthalpy and temperatures $h^{c,n}, T^{c,n} \to h^{c,n+1}, T^{c,n+1}$ and handles phase change which results in updated vofs, $f^{c,\tau}_m \to f^{c,n+1}_m$.
\item The flow solver updates the velocity fields $\dvo{u}^{c,n} \to \dvo{u}^{c,n+1}$ and pressure $p^{c,n} \to p^{c,n+1}$ using a pressure projection step and computes fluxing velocities, $u^{x,n+1}$, $v^{y,n+1}$, and $w^{z,n+1}$
\end{itemize}

\section{Material Properties}
In general, each material will have various properties associated with it, such as density $\rho_m$.  In the flow solver, we are typically concerned with fluids and fluid properties.  At various places in the algorithm, we will simply assign some value for a property in solid cells which serves as an extrapolation procedure to enforce boundary conditions.

\subsection{Cell}
Given a cell-centered volume fraction, $f^c_{ijk,m}$, the fluid properties of a cell are given by:

\begin{align}
  f^c_{ijk} & = \fluid{W} \dvo{1}_m \\
  \label{eq:rho}
  \rho^c_{ijk} & = \dfrac{1}{f^c_{ijk}} \fluid{W} \rho_m \\
  \Delta\rho_{ijk}^c & = \dfrac{1}{f^c_{ijk}} \fluid{W} \Delta\rho_{m}\\
  \mu^c_{ijk} & = \dfrac{1}{f^c_{ijk}} \fluid{W} \mu_m
\end{align}
In general, $\mu_m$ and $\Delta\rho_{m}$ may be functions of temperature. $\Delta\rho_{m}$ is the density deviation found in the Boussinesq approximation.

\subsection{Face-viscosity}
\begin{align}
  \mu^{x/y/z}_{ijk}  & = H^{c,x/y/z} \mu^c_{ijk}
\end{align}
If both $\mu^c = 0$, which happens in solid cells, then $\mu^f_{ijk} \from 0$.  If only one of the $\mu^c$ is zero then the face viscosity is the non-zero $\mu^c$ (i.e. constant extrapolation).

\subsection{Face-density}
\begin{align}
  \rho^{x/y/z}_{ijk} & = L^{c,x/y/z} f^c_{ijk} \rho^c_{ijk}
\end{align}
If both $\rho^c = 0$, which happens in solid cells, then $\rho^f \from 0$.  If only one of the $\rho^c$ is zero, then the face density is the non-zero $\rho^c$.

\section{Conservation of Mass}
Conservation of mass follows directly from the vof geometric transport algorithm and is simply a matter of computing density according to Eqn.~\ref{eq:rho}.

\section{Conservation of Momentum}
The fully consistent equations which we are approximating are,
\begin{align}\label{eq:mtm_cons}
  \Dpartial{}{t}\int_{\cv_f} (\rho \dvo{u})^c = \int_{\fv_f} \big[-\rho\dvo{uu}+\dvo{\tau} -p\dvo{I}+\dvo{f_{S}}+\dvo{f_{D}}+\dvo{f_{B}} \big] \cdot \hat{\dvo{n}}\fv_f + \int_{\cv_f} \dvo{f_\rho} d\cv_f
\end{align}
For most of the fluid domain $\cv_f = \cv$ and $\Dpartial{\cv_f}{t} = 0$.  However, the solution method must deal with the general case where these two simplifications do not hold.

\subsection{Mask off $\cv_s$}
The first step is to mask off parts of the domain where the momentum equations will not be solved.  We will accomplish this using a boolean indicator, $D^c_{ijk}$ which is true when a cell is ``inside'' the fluid domain,
\begin{align*}
  D^{c,n+1}_{ijk} = \left\{\begin{array}{*2{>{\displaystyle}ll}}
                       1 & \text{for } f^{c,n+1}_{ijk} \ge C \\
                       0 & \text{otherwise }
                       \end{array}\right.,
\end{align*}
where $C$ is some cutoff value for the fluid vof.  The default in truchas is $C = 0.01$.  The momentum equations will only be solved for cells with $D^{c,n+1}_{ijk} = 1$
For cells with $D^{c,n+1}_{ijk} = 0$ we do the following,
\begin{align*}
  \dvo{u}^{c,n}_{ijk} & \from 0 \\
  \dvo{u}^{c,n+1}_{ijk} & \from 0 \\
  u^{x,n}_{ijk} & \from 0 \\
  u^{x,n}_{i+1,jk} & \from 0 \\
  v^{y,n}_{ijk} & \from 0 \\
  v^{y,n}_{i,j+1,k} & \from 0 \\
  w^{z,n}_{ijk} & \from 0 \\
  w^{z,n}_{i,j,k+1} & \from 0
\end{align*}

\subsection{LHS Discretization}
Where $D^{c,n+1}_{ijk} = 1$ we discretize the lhs of Eq.~\ref{eq:mtm_cons} as,
\begin{align}
  \dfrac{1}{\cv}\Dpartial{}{t}\int_{\cv_f} (\rho \dvo{u})^c \to \dfrac{\rho^{c,n+1}_{ijk}\dvo{u}^{c,*}_{ijk}\cv^{n+1}_{f,ijk}-\rho^{c,n}_{ijk}\dvo{u}^{c,n}_{ijk}\cv^{n}_{f,ijk}}{\cv\Dt} = \dfrac{\rho^{c,n+1}_{ijk}\dvo{u}^{c,*}_{ijk}f^{c,n+1}_{ijk}-\rho^{c,n}_{ijk}\dvo{u}^{c,n}_{ijk}f^{c,n}_{ijk}}{\Dt}
\end{align}
where we have made the assumption that $\cv$ is independent of location.  Note that the factor of $1/\cv$ is not necessary but it does allow for writing things in terms of fluid vof since $f^{c}_{ijk} = \cv_{f,ijk}/\cv$.  To make the equations consistent, the rhs will also incorporate the $1/\cv$ factor as was done in Eq.~\ref{eq:fluid_disc}

\subsection{RHS - Advection Discretization}
In approximating the momentum advection term, we first introduce upwind-biased face velocities
\begin{align}
  U^x_{ijk} &= \left\{\begin{array}{*2{>{\displaystyle}ll}}
                       u^c_{i-1,jk} & \text{if } u^x_{ijk} > 0 \\
                       u^c_{ijk} & \text{otherwise }
                     \end{array}\right. \\
  U^y_{ijk} &= \left\{\begin{array}{*2{>{\displaystyle}ll}}
                       u^c_{i,j-1,k} & \text{if } v^y_{ijk} > 0 \\
                       u^c_{ijk} & \text{otherwise }
                      \end{array}\right. \\
  U^z_{ijk} &= \left\{\begin{array}{*2{>{\displaystyle}ll}}
                       u^c_{i,j,k-1} & \text{if } w^z_{ijk} > 0 \\
                       u^c_{ijk} & \text{otherwise }
                      \end{array}\right. \\
  V^x_{ijk} &= \left\{\begin{array}{*2{>{\displaystyle}ll}}
                       v^c_{i-1,jk} & \text{if } u^x_{ijk} > 0 \\
                       v^c_{ijk} & \text{otherwise }
                     \end{array}\right. \\
  V^y_{ijk} &= \left\{\begin{array}{*2{>{\displaystyle}ll}}
                       v^c_{i,j-1,k} & \text{if } v^y_{ijk} > 0 \\
                       v^c_{ijk} & \text{otherwise }
                      \end{array}\right. \\
  V^z_{ijk} &= \left\{\begin{array}{*2{>{\displaystyle}ll}}
                       v^c_{i,j,k-1} & \text{if } w^z_{ijk} > 0 \\
                       v^c_{ijk} & \text{otherwise }
                      \end{array}\right. \\
  W^x_{ijk} &= \left\{\begin{array}{*2{>{\displaystyle}ll}}
                       w^c_{i-1,jk} & \text{if } u^x_{ijk} > 0 \\
                       w^c_{ijk} & \text{otherwise }
                     \end{array}\right. \\
  W^y_{ijk} &= \left\{\begin{array}{*2{>{\displaystyle}ll}}
                       w^c_{i,j-1,k} & \text{if } v^y_{ijk} > 0 \\
                       w^c_{ijk} & \text{otherwise }
                      \end{array}\right. \\
  W^z_{ijk} &= \left\{\begin{array}{*2{>{\displaystyle}ll}}
                       w^c_{i,j,k-1} & \text{if } w^z_{ijk} > 0 \\
                       w^c_{ijk} & \text{otherwise }
                       \end{array}\right.
\end{align}
For reasons which are hopefully apparent later, we write the momentum advection for velocity component, $l$, as
\begin{align}
  \dfrac{\Dt}{\Dt}\dfrac{1}{\cv}\int_{\fv_f}\rho\dvo{u}u_l \cdot \hat{\dvo{n}} d\fv_f
\end{align}
We can split the integral into it's fluid material components, $m$, and write the momentum advection term as
\begin{align}
  \dfrac{\Dt}{\Dt}\dfrac{1}{\cv}\sum_m\int_{\fv_m}\rho_m\dvo{u}u_l \cdot \hat{\dvo{n}} d\fv_m
\end{align}
However, the per-material flux volumes, $\dV_m$, satisfy
\begin{align}
  \dV^x_{m,ijk} & \approx \Dt \fv^x_{m,ijk} u^x_{ijk} \\
  \dV^y_{m,ijk} & \approx \Dt \fv^y_{m,ijk} v^y_{ijk} \\
  \dV^z_{m,ijk} & \approx \Dt \fv^z_{m,ijk} w^z_{ijk}
\end{align}
Using this and a midpoint approximation to the face integrals, the contribution to the $x$ component of the momentum update will be:
\begin{align*}
  -\dfrac{1}{\Dt\cv}\sum_m \rho_m & \bigg( \dV^x_{m,i+1,j,k}U^x_{i+1,j,k} - \dV^x_{m,ijk}U^x_{ijk} \\
                                  & + \dV^y_{m,i,j+1,k}U^y_{i,j+1,k} - \dV^y_{m,ijk}U^y_{ijk} \\
                                  & + \dV^z_{m,i,j,k+1}U^z_{i,j,k+1} - \dV^z_{m,ijk}U^z_{ijk}\bigg)
\end{align*}
Similarly, the contribution to the $y$ component of the momentum update will be:
\begin{align*}
  -\dfrac{1}{\Dt\cv}\sum_m \rho_m & \bigg( \dV^x_{m,i+1,j,k}V^x_{i+1,j,k} - \dV^x_{m,ijk}V^x_{ijk} \\
                                  & + \dV^y_{m,i,j+1,k}V^y_{i,j+1,k} - \dV^y_{m,ijk}V^y_{ijk} \\
                                  & + \dV^z_{m,i,j,k+1}V^z_{i,j,k+1} - \dV^z_{m,ijk}V^z_{ijk}\bigg)
\end{align*}
Similarly, the contribution to the $z$ component of the momentum update will be:
\begin{align*}
  -\dfrac{1}{\Dt\cv}\sum_m \rho_m & \bigg( \dV^x_{m,i+1,j,k}W^x_{i+1,j,k} - \dV^x_{m,ijk}W^x_{ijk} \\
                                  & + \dV^y_{m,i,j+1,k}W^y_{i,j+1,k} - \dV^y_{m,ijk}W^y_{ijk} \\
                                  & + \dV^z_{m,i,j,k+1}W^z_{i,j,k+1} - \dV^z_{m,ijk}W^z_{ijk}\bigg)
\end{align*}

\subsection{RHS - Diffusion}
The Newtonian stress tensor has components $\tau_{ij} = \mu(\Dpartial{u_i}{x_j}+\Dpartial{u_j}{x_i})$.  The semi-discrete form of the contribution to the x-momentum is then,
\begin{align*}
  \dfrac{1}{\cv}\int_{\fv_f} \dvo{\tau_x} \cdot \dvo{\hat{n}}d\fv_f = \dfrac{1}{\cv} \big(  & (\fv_f \tau_{xx})^x_{i+1,jk}-(\fv_f \tau_{xx})^x_{ijk} + \\
                                                                                            & (\fv_f \tau_{xy})^y_{i,j+1,k}-(\fv_f \tau_{xy})^y_{ijk} + \\
                                                                                            & (\fv_f \tau_{xz})^z_{ij,k+1}-(\fv_f \tau_{xz})^z_{ijk} + \\
                                                                                            & \fv_{f,s} \tau_{xn}\big).
\end{align*}
However, truchas employs the following approximations
\begin{itemize}
\item $\tau_{xn} = 0$
\item rather than scaling the fluxes by the correct $\fv_f$ for a given face, we simply use the entire face area, $\fv$ and then scale the entire contribution by $\cv_f/\cv$.
\end{itemize}
So the actual contribution to the x-component of the momentum update is,
\begin{align*}
  \dfrac{f^c_{ijk}}{\cv} \bigg(  & \fv^x\big( \mu^x_{i+1,jk}(2\nabla^{c,x}_xu^c_{i+1,j,k}) - \mu^x_{i,jk}(2\nabla^{c,x}_xu^c_{i,j,k}) \big) \\
                                 & + \fv^y\big( \mu^y_{i,j+1,k}(\nabla^{c,y}_yu^c_{i,j+1,k}+\nabla^{c,y}_xv^c_{i,j+1,k}) - \mu^y_{i,j,k}(\nabla^{c,y}_yu^c_{i,j,k}+\nabla^{c,y}_xv^c_{i,j,k}) \big) \\
                                 & + \fv^z\big( \mu^z_{i,j,k+1}(\nabla^{c,z}_zu^c_{i,j,k+1}+\nabla^{c,z}_xw^c_{i,j,k+1}) - \mu^z_{i,j,k}(\nabla^{c,z}_zu^c_{i,j,k}+\nabla^{c,z}_xw^c_{i,j,k}) \big)
\end{align*}
Similarly, the contribution to the y-component of the momentum update is,
\begin{align*}
  \dfrac{f^c_{ijk}}{\cv} \bigg(  & \fv^x\big(  \mu^x_{i+1,j,k}(\nabla^{c,x}_xv^c_{i+1,j,k}+\nabla^{c,x}_yu^c_{i+1,j,k}) - \mu^x_{i,j,k}(\nabla^{c,x}_xv^c_{i,j,k}+\nabla^{c,x}_yu^c_{i,j,k}) \big) \\
                                 & + \fv^y\big( \mu^y_{i,j+1,k}(2\nabla^{c,y}_yv^c_{i,j+1,k}) - \mu^y_{i,j,k}(2\nabla^{c,y}_yv^c_{i,j,k}) \big) \\
                                 & + \fv^z\big( \mu^z_{i,j,k+1}(\nabla^{c,z}_zv^c_{i,j,k+1}+\nabla^{c,z}_yw^c_{i,j,k+1}) - \mu^z_{i,j,k}(\nabla^{c,z}_zv^c_{i,j,k}+\nabla^{c,z}_yw^c_{i,j,k}) \big)
\end{align*}
Similarly, the contribution to the z-component of the momentum update is,
\begin{align*}
  \dfrac{f^c_{ijk}}{\cv} \bigg(  & \fv^x\big(  \mu^x_{i+1,j,k}(\nabla^{c,x}_xw^c_{i+1,j,k}+\nabla^{c,x}_zu^c_{i+1,j,k}) - \mu^x_{i,j,k}(\nabla^{c,x}_xw^c_{i,j,k}+\nabla^{c,x}_zu^c_{i,j,k}) \big) \\
                                 & + \fv^y\big(
                                   \mu^y_{i,j+1,k}(\nabla^{c,y}_yw^c_{i,j+1,k}+\nabla^{c,y}_zv^c_{i,j+1,k}) - \mu^y_{i,j,k}(\nabla^{c,y}_yw^c_{i,j,k}+\nabla^{c,y}_zv^c_{i,j,k}) \big) \\
                                 & + \fv^z\big(                                   \mu^z_{i,j,k+1}(2\nabla^{c,z}_zw^c_{i,j,k+1}) - \mu^z_{i,j,k}(2\nabla^{c,z}_zw^c_{i,j,k}) \big)
\end{align*}

Truchas allows for implicit treatment of the viscous terms using the $\theta$ method so the time-level of the contribution of the viscous terms is:
\begin{align*}
  \theta_\mu \dfrac{1}{\cv}\int_{\fv} \dvo{\tau}^{*} \cdot \dvo{\hat{n}}d\fv + (1-\theta_\mu) \dfrac{1}{\cv}\int_{\fv} \dvo{\tau}^{n} \cdot \dvo{\hat{n}}d\fv
\end{align*}
where the time-level refers to that of the velocity.  The vof scaling and face viscosity is always based on $f^{c,n+1}$.
\subsection{RHS - Pressure/Bouyancy}
The pressure and bouyancy forces are combined to ensure proper hydrostatic behavior.  The semi-discrete form of is:
\begin{align}
  \dfrac{1}{\cv}\int_{\fv} \left(-p\dvo{I}+\dvo{f_B}\right)\cdot \dvo{\hat{n}} d\fv_f.
\end{align}
As with the viscous terms, rather than scaling the forces by the correct $\fv_f$, the entire expression is scaled by $\cv_f/\cv$.  Furthermore, to ensure correct hydrostatic behavior, the cell-centered dynamic pressure gradient is actually evaluated at the faces and averaged to the cell-center.  To ensure that everything is consistent with the Poisson system for the pressure, the contribution to the momentum equation is weighted with face and cell densities at different time-levels.  Thus, if $\dvo{f_B} = [g_x, g_y, g_z]$, then we can define the gradient of the dynamic pressure, $P$, (as opposed to total pressure $p$), at a face as:
\begin{align}
  G^{x,n}_{ijk} & = \dfrac{\left(p^{c,n}_{ijk}-(\rho^{c,n}_{ijk}+\Delta\rho^{c,n}_{ijk} )g_x\Dx/2 \right) - \left(p^{c,n}_{i-1,jk}+(\rho^{c,n}_{i-1,jk}+\Delta\rho^{c,n}_{i-1,j,k})g_x\Dx/2 \right)}{\Dx} \\
  G^{y,n}_{ijk} & = \dfrac{\left(p^{c,n}_{ijk}-(\rho^{c,n}_{ijk}+\Delta\rho^{c,n}_{ijk} )g_y\Dy/2 \right) - \left(p^{c,n}_{i,j-1,k}+(\rho^{c,n}_{i,j-1,k}+\Delta\rho^{c,n}_{i,j-1,k} )g_y\Dy/2 \right)}{\Dy} \\
  G^{z,n}_{ijk} & = \dfrac{\left(p^{c,n}_{ijk}-(\rho^{c,n}_{ijk}+\Delta\rho^{c,n}_{ijk} )g_z\Dz/2 \right) - \left(p^{c,n}_{i,j,k-1}+(\rho^{c,n}_{i,j,k-1}+\Delta\rho^{c,n}_{i,j,k-1} )g_z\Dz/2 \right)}{\Dz}
\end{align}
The contribution of the pressure/bouyancy terms to the x component of the momentum is:
\begin{align}
  - \dfrac{f^{c,n+1}_{ijk} \rho^{c,n+1}_{ijk}}{2} \left[\dfrac{G^{x,n}_{i+1,j,k}}{\rho^{x,n}_{i+1,j,k}} + \dfrac{G^{x,n}_{i,j,k}}{\rho^{x,n}_{i,j,k}}\right]
\end{align}
The contribution of the pressure/bouyancy terms to the y component of the momentum is:
\begin{align}
  - \dfrac{f^{c,n+1}_{ijk} \rho^{c,n+1}_{ijk}}{2} \left[\dfrac{G^{y,n}_{i,j+1,k}}{\rho^{y,n}_{i,j+1,k}} + \dfrac{G^{y,n}_{i,j,k}}{\rho^{y,n}_{i,j,k}}\right]
\end{align}
The contribution of the pressure/bouyancy terms to the z component of the momentum is:
\begin{align}
  - \dfrac{f^{c,n+1}_{ijk} \rho^{c,n+1}_{ijk}}{2} \left[\dfrac{G^{z,n}_{i,j,k+1}}{\rho^{z,n}_{i,j,k+1}} + \dfrac{G^{z,n}_{i,j,k}}{\rho^{z,n}_{i,j,k}}\right]
\end{align}
On solid faces, $G^{x/y/z} \from 0$

\subsection{RHS - Phase Change}
Phase change for a cell is treated as a simple cell centered body force, $\dvo{f_\rho}$, integrated in time using the $\theta$ method.  The contribution to the $x$ momentum component is:
\begin{align}
  - \theta_\rho \left(f^{c,\tau}_{ijk}\rho^{c,\tau}_{ijk}-f^{c,n+1}_{ijk}\rho^{c,n+1}_{ijk}\right) u^{c,*}_{ijk} -
  (1-\theta_\rho)\left(f^{c,\tau}_{ijk}\rho^{c,\tau}_{ijk}-f^{c,n+1}_{ijk}\rho^{c,n+1}_{ijk}\right) u^{c,n}_{ijk}
\end{align}
The contribution to the $y$ momentum component is:
\begin{align}
  - \theta_\rho \left(f^{c,\tau}_{ijk}\rho^{c,\tau}_{ijk}-f^{c,n+1}_{ijk}\rho^{c,n+1}_{ijk}\right) v^{c,*}_{ijk} -
  (1-\theta_\rho)\left(f^{c,\tau}_{ijk}\rho^{c,\tau}_{ijk}-f^{c,n+1}_{ijk}\rho^{c,n+1}_{ijk}\right) v^{c,n}_{ijk}
\end{align}
The contribution to the $z$ momentum component is:
\begin{align}
   - \theta_\rho \left(f^{c,\tau}_{ijk}\rho^{c,\tau}_{ijk}-f^{c,n+1}_{ijk}\rho^{c,n+1}_{ijk}\right) w^{c,*}_{ijk} -
  (1-\theta_\rho)\left(f^{c,\tau}_{ijk}\rho^{c,\tau}_{ijk}-f^{c,n+1}_{ijk}\rho^{c,n+1}_{ijk}\right) w^{c,n}_{ijk}
\end{align}
Note that $f^{c}_{ijk}\rho^{c}_{ijk} = \fluid{W} \rho_m$ but the form with the explicit $f$'s was chosen because it matches the $\cv_f/\cv$ term arising from the finite volume approximation.

\subsection{RHS - Tangential Surface Tension}
At the moment we are only interested in surface tension as a tangential boundary force.  In truchas, it is assumed that the boundary of interest is in the xy plane.  The contribution to the $x$-component of the momentum is:

\begin{align}
  \dfrac{\sigma_T}{\Dz} \dfrac{\fluid{W}^{n+1} \rho_m} {W^{n+1} \rho_m} \nabla^{c,c}_x T^{c,n+1}_{i,j,k}
\end{align}
The contribution to the $y$-component of the momentum rhs is:
\begin{align}
  \dfrac{\sigma_T}{\Dz} \dfrac{\fluid{W}^{n+1} \rho_m} {W^{n+1} \rho_m} \nabla^{c,c}_y T^{c,n+1}_{i,j,k}
\end{align}
I don't see where the term $\fluid{W}^{n+1} \rho_m /W^{n+1} \rho_m$, could come from (since it introduces the solid phase density into a strictly fluid/fluid force).  I would expect to see $f^{c,n+1}_{ijk}$ which is simply $\cv_f/\cv$ and follows directly from the finite volume formulation.

\section{Conservation of Momentum - Projection}
Solving the incompressible conservation equations in the above manner does not ensure that flow remains incompressible.  To this end

\subsection{Computing face based velocities}
A straight forward linear interpolation fails for collocated meshes. Instead a Rhie-Chow interpolation is used which is modified to account for density variations.
\begin{align}
  u^{x,*}_{ijk} & \from \dfrac{1}{2}\left[u^{c,*}_{i-1,j,k}+u^{c,*}_{ijk}\right] + \dfrac{\Dt}{4}\left[ \dfrac{G^{x,n}_{i-1,j,k}}{\rho^{x,n}_{i-1,j,k}}+ 2\dfrac{G^{x,n}_{i,j,k}}{\rho^{x,n}_{i,j,k}} +
                  \dfrac{G^{x,n}_{i+1,j,k}}{\rho^{x,n}_{i+1,j,k}} \right] - \Dt \dfrac{G^{x,n}_{ijk}}{\rho^{x,n+1}_{ijk}} \\
  v^{y,*}_{ijk} & \from \dfrac{1}{2}\left[v^{c,*}_{i,j-1,k}+v^{c,*}_{ijk}\right] + \dfrac{\Dt}{4}\left[ \dfrac{G^{y,n}_{i,j-1,k}}{\rho^{y,n}_{i,j-1,k}}+ 2\dfrac{G^{y,n}_{i,j,k}}{\rho^{y,n}_{i,j,k}} +
                  \dfrac{G^{y,n}_{i,j+1,k}}{\rho^{y,n}_{i,j+1,k}} \right] - \Dt \dfrac{G^{y,n}_{ijk}}{\rho^{y,n+1}_{ijk}} \\
  w^{z,*}_{ijk} & \from \dfrac{1}{2}\left[w^{c,*}_{i,j,k-1}+w^{c,*}_{ijk}\right] + \dfrac{\Dt}{4}\left[ \dfrac{G^{z,n}_{i,j,k-1}}{\rho^{z,n}_{i,j,k-1}}+ 2\dfrac{G^{z,n}_{i,j,k}}{\rho^{z,n}_{i,j,k}} +
\dfrac{G^{z,n}_{i,j,k+1}}{\rho^{z,n}_{i,j,k+1}} \right] - \Dt \dfrac{G^{z,n}_{ijk}}{\rho^{z,n+1}_{ijk}}
\end{align}
Where $D^{c,n+1}_{ijk} = 0$ :
\begin{align}
  u^{x,*}_{ijk}, u^{x,*}_{i+1,j,k} & \from 0 \\
  v^{y,*}_{ijk}, v^{y,*}_{i,j+1,k} & \from 0 \\
  w^{z,*}_{ijk}, w^{z,*}_{i,j,k+1} & \from 0
\end{align}

\subsection{Pressure Poisson System}
Using the face velocities, we can solve the standard pressure Poisson system for $\dP^{c,n+1}$:
\begin{align}
  \dfrac{1}{\cv}\int_{\fv_f}\dfrac{\nabla\dP^{f,n+1}}{\rho^{f,n+1}} \cdot \dvo{\fv_f} = \dfrac{1}{\cv}\int_{\fv_f}\dfrac{\dvo{u^{f,*}}}{\Dt} \cdot \dvo{\fv_f}
\end{align}
Truchas makes the assumption here that $\fv^{x/y/z}_f = \fv^{x/y/z}$.  So the rhs of this system becomes:
\begin{align}
  \bigg[\dfrac{1}{\cv}\int_{\fv_f}\dfrac{\dvo{u^{f,*}}}{\Dt} \cdot \dvo{\fv_f}\bigg]_{ijk}  \approx &
    \dfrac{1}{\Dt}\big[
                                                                                                      \dfrac{u^{x,*}_{i+1,j,k}-u^{x,*}_{ijk}}{\Dx} +
                                                                                                      \dfrac{v^{y,*}_{i,j+1,k}-v^{y,*}_{ijk}}{\Dy} +
                                                                                                      \dfrac{w^{z,*}_{i,j,k+1}-w^{z,*}_{ijk}}{\Dz}
                                                                                                    \big]
\end{align}
The lhs of this system becomes:
\begin{align}\notag
  \bigg[\dfrac{1}{\cv}\int_{\fv_f}\dfrac{\nabla\dP^{f,n+1}}{\rho^{f,n+1}} \cdot \dvo{\fv_f}\bigg] \approx & \dfrac{1}{\Dx^2}\left[\dfrac{\dP^{c,n+1}_{i+1,j,k}-\dP^{c,n+1}_{i,j,k}}{\rho^{x,n+1}_{i+1,j,k}} - \dfrac{\dP^{c,n+1}_{i,j,k}-\dP^{c,n+1}_{i-1,j,k}}{\rho^{x,n+1}_{i,j,k}}\right] \\
  \notag
  + & \dfrac{1}{\Dy^2}\left[\dfrac{\dP^{c,n+1}_{i,j+1,k}-\dP^{c,n+1}_{i,j,k}}{\rho^{y,n+1}_{i,j+1,k}} - \dfrac{\dP^{c,n+1}_{i,j,k}-\dP^{c,n+1}_{i,j-1,k}}{\rho^{y,n+1}_{i,j-1,k}}\right] \\
  + & \dfrac{1}{\Dz^2}\left[\dfrac{\dP^{c,n+1}_{i,j,k+1}-\dP^{c,n+1}_{i,j,k}}{\rho^{z,n+1}_{i,j,k+1}} - \dfrac{\dP^{c,n+1}_{i,j,k}-\dP^{c,n+1}_{i,j,k-1}}{\rho^{z,n+1}_{i,j,k-1}}\right]
\end{align}
\subsection{Face Velocity Correction}
The face velocities at the $*$ time are corrected to the $n+1$ time by
\begin{align}
  u^{x,n+1}_{ijk} & \from u^{x,*} - \Dt\dfrac{\nabla^{c,x}_x\dP^{c,n+1}_{ijk}}{\rho^{x,n+1}_{ijk}} \\
  v^{y,n+1}_{ijk} & \from v^{y,*} - \Dt\dfrac{\nabla^{c,y}_y\dP^{c,n+1}_{ijk}}{\rho^{y,n+1}_{ijk}} \\
  w^{z,n+1}_{ijk} & \from w^{z,*} - \Dt\dfrac{\nabla^{c,z}_z\dP^{c,n+1}_{ijk}}{\rho^{z,n+1}_{ijk}}
\end{align}
\subsection{Pressure Update}
In the absence of Dirichlet pressure boundary conditions, $\dP^{n+1}$ is demeaned followed by the update:
\begin{align}
  p^{n+1} = p^{n} + \dP^{n+1}
\end{align}
\subsection{Cell Centered Velocity Correction}
The cell centered velocities at the $*$ time are corrected to the $n+1$ time by
\begin{align}
  u^{c,n+1}_{ijk} & \from u^{c,*}_{ijk} - \dfrac{\Dt}{2}\left[ \dfrac{G^{x,n+1}_{i+1,j,k}}{\rho^{x,n+1}_{i+1,j,k}} + \dfrac{G^{x,n+1}_{i,j,k}}{\rho^{x,n+1}_{i,j,k}} \right] + \dfrac{\Dt}{2}\left[ \dfrac{G^{x,n}_{i+1,j,k}}{\rho^{x,n}_{i+1,j,k}} + \dfrac{G^{x,n}_{i,j,k}}{\rho^{x,n}_{i,j,k}} \right] \\
  v^{c,n+1}_{ijk} & \from v^{c,*}_{ijk} - \dfrac{\Dt}{2}\left[ \dfrac{G^{y,n+1}_{i,j+1,k}}{\rho^{y,n+1}_{i,j+1,k}} + \dfrac{G^{y,n+1}_{i,j,k}}{\rho^{y,n+1}_{i,j,k}} \right] + \dfrac{\Dt}{2}\left[ \dfrac{G^{y,n}_{i,j+1,k}}{\rho^{y,n}_{i,j+1,k}} + \dfrac{G^{y,n}_{i,j,k}}{\rho^{y,n}_{i,j,k}} \right] \\
  w^{c,n+1}_{ijk} & \from w^{c,*}_{ijk} - \dfrac{\Dt}{2}\left[ \dfrac{G^{z,n+1}_{i,j,k+1}}{\rho^{z,n+1}_{i,j,k+1}} + \dfrac{G^{z,n+1}_{i,j,k}}{\rho^{z,n+1}_{i,j,k}} \right] + \dfrac{\Dt}{2}\left[ \dfrac{G^{z,n}_{i,j,k+1}}{\rho^{z,n}_{i,j,k+1}} + \dfrac{G^{z,n}_{i,j,k}}{\rho^{z,n}_{i,j,k}} \right]
\end{align}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
