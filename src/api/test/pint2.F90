!!
!! PINT2
!!
!! This test/example is focused on getting the full HT+flow state from a
!! simulation, and using it to start a second simulation. The problem is
!! based on the spot weld example problem. The first simulation starts at
!! time 0, runs to t1 where the solution state is saved, and then continues
!! to the final time t2. The second simulation starts at time t1 with the
!! saved solution state used as the initial state, and runs to t2. The final
!! temperature and velocity states of the two simulations are compared.
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! June 2019
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! NB: This is best run with 1, 2, or 3 MPI processes; 6 or 9 may work too,
!! but a number that doesn't divide 18 evenly (# of amrex boxes) will result
!! in a different distribution of boxes across processes from one simulation
!! to the next, which this program is not designed to handle. See
!! https://github.com/AMReX-Codes/amrex/issues/500
!!

!NB: input file must disable flow if this is defined
!#define NO_FLOW

program main

  use,intrinsic :: iso_fortran_env, only: r8 => real64, error_unit, output_unit
  use mpi
  use pint_sim_type
  implicit none

  integer :: ierr, stat, this_rank, nproc, comm
  character(128) :: arg
  character(:), allocatable :: infile
  integer :: ncell, nxface, nyface, nzface, ncell2
  real(r8), allocatable :: temp1(:), vcell1(:,:), vxface1(:), vyface1(:), vzface1(:), press1(:)
  real(r8), allocatable :: temp2(:), vcell2(:,:), vxface2(:), vyface2(:), vzface2(:), press2(:)
  real(r8) :: t0, t1, t2, initial_temp, rel_temp_err, rel_vel_err, vmax
  real(r8), allocatable :: coord0(:,:)

  !! Get the input file from the command line
  if (command_argument_count() >= 1) then
    call get_command_argument(1, arg)
    infile = trim(arg)
  else
    write(output_unit,'(a)') 'usage: pint2 INFILE'
    error stop
  end if

  call MPI_Init(ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)
  call MPI_Comm_rank(MPI_COMM_WORLD, this_rank, ierr)
  comm = MPI_COMM_WORLD

  initial_temp = 300.0

  t0 = 0
  t1 = 0.8_r8
  t2 = 1.0_r8

  call run_sim1('pint2-out1', stat)
  if (stat /= 0) then
    if (this_rank == 0) &
        write(error_unit,*) 'Simulation failed; see pint2-out1/sim.log for details'
    call MPI_Finalize(ierr)
    stop 1
  end if

  call run_sim2('pint2-out2', stat)
  if (stat /= 0) then
    if (this_rank == 0) &
        write(error_unit,*) 'Simulation failed; see pint2-out2/sim.log for details'
    call MPI_Finalize(ierr)
    stop 1
  end if

  rel_temp_err = maxval(abs((temp2 - temp1)/temp1))
  call MPI_Allreduce(MPI_IN_PLACE, rel_temp_err, 1, MPI_REAL8, MPI_MAX, comm, ierr)
  if (this_rank == 0) write(output_unit,*) 'max rel temp error =', rel_temp_err

#ifndef NO_FLOW
  vmax = sqrt(maxval(sum(vcell1**2,dim=1)))
  call MPI_Allreduce(MPI_IN_PLACE, vmax, 1, MPI_REAL8, MPI_MAX, comm, ierr)

  rel_vel_err = maxval(abs(vcell2 - vcell1)/vmax)
  call MPI_Allreduce(MPI_IN_PLACE, rel_vel_err, 1, MPI_REAL8, MPI_MAX, comm, ierr)
  if (this_rank == 0) write(output_unit,*) 'rel max vel error =', rel_vel_err
#endif

  call MPI_Barrier(MPI_COMM_WORLD, ierr)
  call MPI_Finalize(ierr)

  !! Things should be bit for bit identical at the end.
#ifdef NO_FLOW
  if (rel_temp_err > 0) stop 2
#else
  if (rel_temp_err > 0 .or. rel_vel_err > 0) stop 2
#endif

  !! GFortran doesn't bother with doing this at exit, so just to avoid
  !! having valgrind report these as leaks we do it manually.
  deallocate(temp1, vcell1, vxface1, vyface1, vzface1, press1)
  deallocate(temp2, vcell2, vxface2, vyface2, vzface2, press2)
  deallocate(coord0, infile)

contains

  !! Full reference simulation.
  !! Grab solution midway at T1 to use for starting a second simulation.

  subroutine run_sim1(outdir, stat)

    character(*), intent(in) :: outdir
    integer, intent(out) :: stat
    type(pint_sim) :: sim

    call sim%init(comm, infile, outdir, stat)
    if (stat /= 0) return

    call sim%get_ht_mesh_sizes(ncell)
    allocate(coord0(3,ncell))
    call sim%get_ht_cell_centers(coord0)

    allocate(temp1(ncell))
    temp1 = initial_temp

    call sim%set_initial_state(t0, temp1)
    call sim%write_solution(0)

    call sim%step_to(t1, stat)
    if (stat /= 0) return
    call sim%write_solution(1)

    allocate(temp2(ncell))
    call sim%get_current_ht_state(temp2)
#ifndef NO_FLOW
    call sim%get_flow_mesh_sizes(ncell2, nxface, nyface, nzface)
    allocate(vcell2(3,ncell2), vxface2(nxface), vyface2(nyface), vzface2(nzface), press2(ncell2))
    call sim%get_current_flow_state(vcell2, vxface2, vyface2, vzface2, press2)
#endif

    !call sim%set_debug(.true.)
    call sim%step_to(t2, stat)
    if (stat /= 0) return
    call sim%write_solution(2)

    call sim%get_current_ht_state(temp1)
#ifndef NO_FLOW
    allocate(vcell1(3,ncell2), vxface1(nxface), vyface1(nyface), vzface1(nzface), press1(ncell2))
    call sim%get_current_flow_state(vcell1, vxface1, vyface1, vzface1, press1)
#endif

  end subroutine

  !! Simulation starting from reference simulation solution state at T1

  subroutine run_sim2(outdir, stat)

    character(*), intent(in) :: outdir
    integer, intent(out) :: stat
    type(pint_sim) :: sim

    call sim%init(comm, infile, outdir, stat)
    if (stat /= 0) return

    call check_box_distribution(sim, stat)
    if (stat /= 0) return

    call sim%set_initial_state(t1, temp2)
#ifndef NO_FLOW
    call sim%set_initial_flow_state(vcell2, vxface2, vyface2, vzface2, press2)
#endif
    call sim%write_solution(1)

    !call sim%set_debug(.true.)
    call sim%step_to(t2, stat)
    if (stat /= 0) return
    call sim%write_solution(2)

    call sim%get_current_ht_state(temp2)
#ifndef NO_FLOW
    call sim%get_current_flow_state(vcell2, vxface2, vyface2, vzface2, press2)
#endif

  end subroutine


  subroutine check_box_distribution(sim, stat)

    type(pint_sim), intent(in) :: sim
    integer, intent(out) :: stat

    logical :: same
    integer :: n1, ierr
    real(r8), allocatable :: coord(:,:)

    call sim%get_ht_mesh_sizes(n1)
    call MPI_Allreduce(n1==ncell, same, 1, MPI_LOGICAL, MPI_LAND, comm, ierr)
    if (.not.same) then
      if (this_rank == 0) write(output_unit,'(a)') 'Box distribution (size) is different!'
      stat = 1
      return
    end if

    allocate(coord(3,ncell))
    call sim%get_ht_cell_centers(coord)
    if (any(coord /= coord0)) then
      if (this_rank == 0) write(output_unit,'(a)') 'Box distribution (coord) is different!'
      stat = 1
      return
    end if

    stat = 0

  end subroutine

end program
