//
// C-PINT4
//
// This is a C version of PINT4.F90 which is only intended to exercise the
// ability to pass a JSON string as the input rather than the name of a JSON
// input file. It first does a simulation passing the file name to the solver,
// and then reads the input file into a character string buffer and does a
// second simulation passing the buffer instead. It checks that the final
// result of both are identical. For simplicity in creating the JSON string
// for this test, the input file needs to consist of a single line.
//
// Neil N. Carlson <nnc@lanl.gov>
// July 2020
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tpbf-pint.h"

int main(int argc, char **argv)
{
  int ierr, this_rank, nproc;

  if (argc < 2) {
    fprintf(stderr,"usage: %s INFILE\n", argv[0]);
    return 1;
  }

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &this_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  double temp0;
  char *outdir1, *outdir2;
  temp0   = 400.0;
  outdir1 = "c-pint4-outA";
  outdir2 = "c-pint4-outB";

  double t0 = 0;
  double t1 = 0.01;

  void *solver;
  int ncell, nxface, nyface, nzface;

  // First simulation

  solver = tpbf_create_solver(MPI_COMM_WORLD, argv[1], outdir1);
  if (!solver) goto sim1_exit;

  tpbf_get_mesh_sizes(solver, &ncell, &nxface, &nyface, &nzface);
  double *temp1 = (double*) malloc(sizeof(double)*ncell);
  for (int j = 0; j < ncell; j++) temp1[j] = temp0;
  tpbf_set_initial_state_ht(solver, t0, ncell, temp1);
  tpbf_write_solution(solver, 0);

  ierr = tpbf_integrate_to(solver, t1);
  if (ierr != 0) goto sim1_exit;
  tpbf_write_solution(solver, 1);

  tpbf_get_current_state_ht(solver, ncell, temp1);

  tpbf_destroy_solver(solver);

  sim1_exit: if (ierr != 0) {
    if (this_rank == 0)
        fprintf(stderr,"Simulation failed; see %s/sim.log for details\n", outdir1);
    MPI_Finalize();
    return 1;
  }

  // Second simulation

  FILE *fp;
  char *buffer = NULL;
  size_t len;
  fp = fopen(argv[1], "r");
  getline(&buffer, &len, fp);
  fclose(fp);

  solver = tpbf_create_solver(MPI_COMM_WORLD, buffer, outdir2);
  if (!solver) goto sim2_exit;

  double *temp2 = (double*) malloc(sizeof(double)*ncell);
  for (int j = 0; j < ncell; j++) temp2[j] = temp0;
  tpbf_set_initial_state_ht(solver, t0, ncell, temp2);
  tpbf_write_solution(solver, 0);

  ierr = tpbf_integrate_to(solver, t1);
  if (ierr != 0) goto sim2_exit;
  tpbf_write_solution(solver, 1);

  tpbf_get_current_state_ht(solver, ncell, temp2);

  tpbf_destroy_solver(solver);

  sim2_exit: if (ierr != 0) {
    if (this_rank == 0)
        fprintf(stderr,"Simulation failed; see %s/sim.log for details\n", outdir2);
    MPI_Finalize();
    return 2;
  }

  // Compute the error

  double relerr = 0.0, tmp;
  for (int j=0; j < ncell; j++) {
    tmp = fabs((temp2[j] - temp1[j])/temp1[j]);
    if (tmp > relerr) relerr = tmp;
  }
  MPI_Allreduce(MPI_IN_PLACE, &relerr, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  if (this_rank == 0) printf("max rel temp error = %13.5e\n", relerr);
  if (relerr > 5.0e-5) {
    MPI_Finalize();
    return 3;
  }

  MPI_Finalize();
  return 0;
}
