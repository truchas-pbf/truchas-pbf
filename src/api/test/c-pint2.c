//
// C-PINT2
//
// This is C version of PINT2.F90 which exercises getting the full HT+flow
// state from a simulation, and using it to start a second simulation. The
// problem is based on the spot weld example problem. The first simulation
// starts at time 0, runs to t1 where the solution state is saved, and then
// continues to the final time t2. The second simulation starts at time t1
// with the saved solution state used as the initial state, and runs to t2.
// The final temperature and velocity states of the two simulations are
// compared.
//
// Neil N. Carlson <nnc@lanl.gov>
// June 2019
//
// -----------------------------------------------------------------------------
//
// NB: This is best run with 1, 2, or 3 MPI processes; 6 or 9 may work too,
// but a number that doesn't divide 18 evenly (# of amrex boxes) will result
// in a different distribution of boxes across processes from one simulation
// to the next, which this program is not designed to handle. See
// https://github.com/AMReX-Codes/amrex/issues/500
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tpbf-pint.h"

int check_box_distribution(void *solver, int, double*, MPI_Comm);

int main(int argc, char **argv)
{
  int ierr, this_rank, nproc;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &this_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);

  double initial_temp = 300.0;
  char *infile = "pint2.json";

  double t0 = 0;
  double t1 = 0.8;
  double t2 = 1.0;

  void *solver;
  int ncell, nxface, nyface, nzface;

  // Simulation 1

  solver = tpbf_create_solver(MPI_COMM_WORLD, infile, "out1");
  if (!solver) goto sim1_exit;

  tpbf_get_mesh_sizes(solver, &ncell, &nxface, &nyface, &nzface);

  double *coord0 = (double*) malloc(3*sizeof(double)*ncell);
  tpbf_get_cell_centers(solver, ncell, coord0);

  double *temp1 = (double*) malloc(sizeof(double)*ncell);
  for (int j = 0; j < ncell; j++) temp1[j] = initial_temp;

  tpbf_set_initial_state_ht(solver, t0, ncell, temp1);
  tpbf_write_solution(solver, 0);

  ierr = tpbf_integrate_to(solver, t1);
  if (ierr != 0) goto sim1_exit;
  tpbf_write_solution(solver, 1);

  double *temp2   = (double*) malloc(sizeof(double)*ncell);
  double *vcell2  = (double*) malloc(3*sizeof(double)*ncell);
  double *vxface2 = (double*) malloc(sizeof(double)*nxface);
  double *vyface2 = (double*) malloc(sizeof(double)*nyface);
  double *vzface2 = (double*) malloc(sizeof(double)*nzface);
  double *press2  = (double*) malloc(sizeof(double)*ncell);
  tpbf_get_current_state(solver, ncell, nxface, nyface, nzface,
      temp2, vcell2, vxface2, vyface2, vzface2, press2);

  ierr = tpbf_integrate_to(solver, t2);
  if (ierr != 0) goto sim1_exit;
  tpbf_write_solution(solver, 2);

  double *vcell1  = (double*) malloc(3*sizeof(double)*ncell);
  double *vxface1 = (double*) malloc(sizeof(double)*nxface);
  double *vyface1 = (double*) malloc(sizeof(double)*nyface);
  double *vzface1 = (double*) malloc(sizeof(double)*nzface);
  double *press1  = (double*) malloc(sizeof(double)*ncell);
  tpbf_get_current_state(solver, ncell, nxface, nyface, nzface,
      temp1, vcell1, vxface1, vyface1, vzface1, press1);

  tpbf_destroy_solver(solver);

  sim1_exit: if (ierr != 0) {
    if (this_rank == 0)
        fprintf(stderr,"Simulation failed; see out1/sim.log for details\n");
    MPI_Finalize();
    return 1;
  }

  // Second simulation pair

  solver = tpbf_create_solver(MPI_COMM_WORLD, infile, "out2");
  if (!solver) goto sim2_exit;

  ierr = check_box_distribution(solver, ncell, coord0, MPI_COMM_WORLD);
  if (ierr != 0) goto sim2_exit;

  tpbf_set_initial_state(solver, t1, ncell, nxface, nyface, nzface,
      temp2, vcell2, vxface2, vyface2, vzface2, press2);
  tpbf_write_solution(solver, 1);

  ierr = tpbf_integrate_to(solver, t2);
  if (ierr != 0) goto sim2_exit;
  tpbf_write_solution(solver, 2);

  tpbf_get_current_state(solver, ncell, nxface, nyface, nzface,
      temp2, vcell2, vxface2, vyface2, vzface2, press2);

  tpbf_destroy_solver(solver);

  sim2_exit: if (ierr != 0) {
    if (this_rank == 0)
        fprintf(stderr,"Simulation failed; see out2/sim.log for details\n");
    MPI_Finalize();
    return 1;
  }

  // Compute the error

  double rel_temp_err = 0.0, tmp;
  for (int j=0; j < ncell; j++) {
    tmp = fabs((temp2[j] - temp1[j])/temp1[j]);
    if (tmp > rel_temp_err) rel_temp_err = tmp;
  }
  MPI_Allreduce(MPI_IN_PLACE, &rel_temp_err, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  if (this_rank == 0) printf("max rel temp error = %13.5e\n", rel_temp_err);

  double vmax = 0.0;
  for (int j=0; j < ncell; j++) {
    tmp = vcell1[3*j]*vcell1[3*j] + vcell1[3*j+1]*vcell1[3*j+1] + vcell1[3*j+2]*vcell1[3*j+2];
    if (tmp > vmax) vmax = tmp;
  }
  vmax = sqrt(vmax);
  MPI_Allreduce(MPI_IN_PLACE, &vmax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

  double rel_vel_err = 0.0;
  for (int j=0; j < 3*ncell; j++) {
    tmp = fabs(vcell2[j]-vcell1[j]);
    if (tmp > rel_vel_err) rel_vel_err = tmp;
  }
  rel_vel_err = rel_vel_err/vmax;
  MPI_Allreduce(MPI_IN_PLACE, &rel_vel_err, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  if (this_rank == 0) printf("rel max vel error = %13.5e\n", rel_vel_err);

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();

  if (rel_temp_err > 1.0e-4 || rel_vel_err > 2.0e-3) return 2;

  return 0;
}

int check_box_distribution(void *solver, int ncell0, double *coord0, MPI_Comm comm) {

  int ncell, nxface, nyface, nzface;
  tpbf_get_mesh_sizes(solver, &ncell, &nxface, &nyface, &nzface);

  int this_rank;
  MPI_Comm_rank(comm, &this_rank);

  int same = (ncell0 == ncell) ? 1: 0;
  MPI_Allreduce(MPI_IN_PLACE, &same, 1, MPI_INT, MPI_MIN, comm);
  if (!same) {
    if (this_rank == 0)
        fprintf(stderr,"Box distribution (size) is different!\n");
    return 1;
  }

  int ierr = 0;
  double *coord = (double*) malloc(3*sizeof(double)*ncell);
  tpbf_get_cell_centers(solver, ncell, coord);
  for (int j=0; j < 3*ncell; j++) {
    if (coord[j] != coord0[j]) {
      ierr = 1;
      break;
    }
  }
  MPI_Allreduce(MPI_IN_PLACE, &ierr, 1, MPI_INT, MPI_MAX, comm);
  if (ierr != 0) {
    if (this_rank == 0)
      fprintf(stderr,"Box distribution (coord) is different!\n");
    return 1;
  }

  return 0;
}
