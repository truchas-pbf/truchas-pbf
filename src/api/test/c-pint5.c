//
// C-PINT5
//
// This is C version of PINT5.F90 which exercises the retrieval and setting
// of ExaCA data. The problem is based on the spot weld example with flow
// disabled. The first simulation runs until time T1 when the melt pool is
// partially re-solidified and the ExaCA state data is retrieved. It then
// continues until time T2 when solidification is complete. The second
// simulation starts at T1 initialized with the temperature field and ExaCA
// data from the first simulation, and then runs until the same time T2. The
// final temperatures and ExaCA data from both simulations are then compared.
// They should be bit-for-bit identical. (Key to this is that the time T1 is
// specified as a restart time in the input file.)
//
// Neil N. Carlson <nnc@lanl.gov>
// July 2020
//

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "tpbf-pint.h"

int main(int argc, char **argv)
{
  int ierr, this_rank, nproc;
  MPI_Comm comm;

  char *infile;
  if (argc == 2) {
    infile = (char*) malloc(sizeof(char)*(strlen(argv[1])+1));
    strcpy(infile, argv[1]);
  } else {
    fprintf(stderr,"usage: %s INFILE\n", argv[0]);
    return 1;
  }

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &this_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);
  MPI_Comm_dup(MPI_COMM_WORLD, &comm);

  double temp0 = 300.0;
  char *outdir1 = "c-pint5.1-out";
  char *outdir2 = "c-pint5.2-out";

  double t0 = 0;
  double t1 = 2.0;
  double t2 = 3.0;

  void *solver;
  int ncell, nxface, nyface, nzface;

  // First simulation pair

  solver = tpbf_create_solver(comm, infile, outdir1);
  if (!solver) goto sim1_exit;

  tpbf_get_mesh_sizes(solver, &ncell, &nxface, &nyface, &nzface);
  double *temp1 = (double*) malloc(sizeof(double)*ncell);
  double *temp2 = (double*) malloc(sizeof(double)*ncell);
  for (int j = 0; j < ncell; j++) temp1[j] = temp0;
  tpbf_set_initial_state_ht(solver, t0, ncell, temp1);
  tpbf_write_solution(solver, 0);

  ierr = tpbf_integrate_to(solver, t1);
  if (ierr != 0) goto sim1_exit;
  tpbf_write_solution(solver, 1);

  tpbf_get_current_state_ht(solver, ncell, temp2);
  int nbyte, ncell2;
  tpbf_get_exaca_state_size(solver, &nbyte, &ncell2);
  int8_t *exaca_state = (int8_t*) malloc(sizeof(int8_t)*nbyte*ncell2);
  tpbf_get_exaca_state(solver, nbyte, ncell2, exaca_state);

  // Get the start and finish solidification times. Cells without valid
  // solidification data are identified by start > finish (0 and -1)
  int nsolid1 = tpbf_num_solid_cells(solver);
  double *coord1   = (double*) malloc(sizeof(double)*3*nsolid1);
  double *tstart1  = (double*) malloc(sizeof(double)*nsolid1);
  double *tfinish1 = (double*) malloc(sizeof(double)*nsolid1);
  tpbf_get_solid_times(solver, nsolid1, coord1, tstart1, tfinish1);

  ierr = tpbf_integrate_to(solver, t2);
  if (ierr != 0) goto sim1_exit;
  tpbf_write_solution(solver, 2);

  tpbf_get_current_state_ht(solver, ncell, temp1);

  // Get the start and finish solidification times. Cells without valid
  // solidification data are identified by start > finish (0 and -1)
  int nsolid0 = tpbf_num_solid_cells(solver);
  double *coord0   = (double*) malloc(sizeof(double)*3*nsolid0);
  double *tstart0  = (double*) malloc(sizeof(double)*nsolid0);
  double *tfinish0 = (double*) malloc(sizeof(double)*nsolid0);
  tpbf_get_solid_times(solver, nsolid0, coord0, tstart0, tfinish0);

  tpbf_destroy_solver(solver);

  sim1_exit: if (ierr != 0) {
    if (this_rank == 0)
        fprintf(stderr,"Simulation failed; see %s/sim.log for details\n", outdir1);
    MPI_Finalize();
    return 1;
  }

  // Second simulation pair

  solver = tpbf_create_solver(comm, infile, outdir2);
  if (!solver) goto sim2_exit;

  tpbf_set_initial_state_ht(solver, t1, ncell, temp2);
  tpbf_set_exaca_state(solver, nbyte, ncell2, exaca_state);
  tpbf_write_solution(solver, 1);

  ierr = tpbf_integrate_to(solver, t2);
  if (ierr != 0) goto sim2_exit;
  tpbf_write_solution(solver, 2);

  tpbf_get_current_state_ht(solver, ncell, temp2);

  // Get the start and finish solidification times. Cells without valid
  // solidification data are identified by start > finish (0 and -1)
  int nsolid2 = tpbf_num_solid_cells(solver);
  double *coord2   = (double*) malloc(sizeof(double)*3*nsolid2);
  double *tstart2  = (double*) malloc(sizeof(double)*nsolid2);
  double *tfinish2 = (double*) malloc(sizeof(double)*nsolid2);
  tpbf_get_solid_times(solver, nsolid2, coord2, tstart2, tfinish2);

  tpbf_write_solid_times(solver, "solid-times");

  tpbf_destroy_solver(solver);

  sim2_exit: if (ierr != 0) {
    if (this_rank == 0)
        fprintf(stderr,"Simulation failed; see %s/sim.log for details\n", outdir2);
    MPI_Finalize();
    return 2;
  }

  // Compute the error

  double relerr = 0.0, tmp;
  for (int j=0; j < ncell; j++) {
    tmp = fabs((temp2[j] - temp1[j])/temp1[j]);
    if (tmp > relerr) relerr = tmp;
  }
  MPI_Allreduce(MPI_IN_PLACE, &relerr, 1, MPI_DOUBLE, MPI_MAX, comm);
  if (this_rank == 0) printf("%s: max rel temp error = %13.5e\n", outdir2, relerr);
  if (relerr > 0) {
    MPI_Finalize();
    return 3;
  }

  int discrepancy = fabs(nsolid1 + nsolid2 - nsolid0);
  MPI_Allreduce(MPI_IN_PLACE, &discrepancy, 1, MPI_INT, MPI_MAX, comm);
  if (this_rank == 0) printf("%s: num solid cell discrepancy = %d\n", outdir2, discrepancy);
  if (discrepancy > 0) {
    MPI_Finalize();
    return 4;
  }

  MPI_Finalize();
  return 0;
}
