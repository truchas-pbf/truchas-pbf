// Why the C wrapper? The input C MPI communicator needs to be converted to a
// Fortran MPI communicator and this must be done on the C side. But while we
// are here we go ahead and handle the input string lengths too. 

#include "mpi.h"
#include <string.h>

void* tpbf_create_solver_f(MPI_Fint comm, int inlen, char *infile,
                           int loglen, char *logfile);

void* tpbf_create_solver(MPI_Comm comm, char *infile, char *outdir)
{
  return tpbf_create_solver_f(MPI_Comm_c2f(comm),
               strlen(infile), infile, strlen(outdir), outdir);
}

void tpbf_write_solid_times_f(void *solver, int outlen, const char *outptr);

void tpbf_write_solid_times(void *solver, const char *filename)
{
  tpbf_write_solid_times_f(solver, strlen(filename), filename);
}
