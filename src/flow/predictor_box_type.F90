!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module predictor_box_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use fat_box_type
  use bndry_func_class
  use bndry_vfunc_class
  use truchas_env_type
  use scalar_func_class
  implicit none
  private

  type, extends(fat_box), public :: predictor_box
    private
    type(truchas_env), pointer :: env => null()
    class(bndry_vfunc), allocatable :: bc_vel_xlo, bc_vel_ylo, bc_vel_zlo
    class(bndry_vfunc), allocatable :: bc_vel_xhi, bc_vel_yhi, bc_vel_zhi
    class(bndry_func), allocatable :: bc_prs_xlo, bc_prs_ylo, bc_prs_zlo
    class(bndry_func), allocatable :: bc_prs_xhi, bc_prs_yhi, bc_prs_zhi
    logical :: bc_free_xlo = .false., bc_free_ylo = .false., bc_free_zlo = .false.
    logical :: bc_free_xhi = .false., bc_free_yhi = .false., bc_free_zhi = .false.
    logical :: bc_sym_xlo = .false., bc_sym_ylo = .false., bc_sym_zlo = .false.
    logical :: bc_sym_xhi = .false., bc_sym_yhi = .false., bc_sym_zhi = .false.
    logical :: bc_st_zhi = .false.
    real(r8) :: dx(3), vimpl
    class(scalar_func), allocatable :: dsig_dT
  contains
    procedure :: init
    procedure :: compute_semi_implicit_contribution
    procedure, private :: compute_cell_viscosity_contribution
    procedure, private :: init_bc
  end type predictor_box

contains

  subroutine init(this, env, mesh, box, params)

    use amrex_mesh_type
    use parameter_list_type

    class(predictor_box), intent(out) :: this
    type(truchas_env), intent(in), target :: env
    type(amrex_mesh), intent(in) :: mesh
    type(fat_box), intent(in) :: box
    type(parameter_list), intent(inout) :: params

    type(parameter_list), pointer :: bc_params
    character(:), allocatable :: context, errmsg
    integer :: stat

    this%env => env
    this%fat_box = box
    this%dx = mesh%geom%dx

    context = 'processing ' // params%name() // ': '
    if (params%is_scalar('viscous-implicitness')) then
      call params%get('viscous-implicitness', this%vimpl, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
    else
      this%vimpl = 0.5_r8
    end if

    if (params%is_sublist('bc')) then
      bc_params => params%sublist('bc')
      call this%init_bc(mesh%geom, bc_params)
    else
      call this%env%log%fatal(context//'missing "bc" sublist parameter')
    end if

  end subroutine init

  subroutine init_bc(this, geom, params)

    use scalar_func_factories
    use bndry_face_func_type
    use bndry_face_vfunc_type
    use parameter_list_type
    use amrex_base_module, only: amrex_geometry, amrex_box

    class(predictor_box), intent(inout) :: this
    type(amrex_geometry), intent(in)    :: geom
    type(parameter_list), intent(inout) :: params

    integer :: i, stat
    type(parameter_list_iterator) :: piter
    type(parameter_list), pointer :: bc_params
    character(:), allocatable :: context, errmsg, type, sides(:)

    ! FIXME: needed for bndry_face_func%init -- change its interface
    type(amrex_box) :: bx
    bx = amrex_box(this%lo, this%hi)

    piter = parameter_list_iterator(params, sublists_only=.true.)
    do while (.not.piter%at_end())
      bc_params => piter%sublist()
      context = 'processing ' // bc_params%name() // ': '
      call bc_params%get('type', type, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      call bc_params%get('sides', sides, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      do i = 1, size(sides)
        if (all(sides(i) /= ['xlo','ylo','zlo','xhi','yhi','zhi'])) &
            call this%env%log%fatal(context//'unknown "sides" specifier: '//sides(i))
      end do
      select case (type)
      case ('velocity')
        call init_vel
      case ('pressure')
        call init_prs
      case('free', 'symmetry')
        call init_free_sym
      case('marangoni')
        INSIST(size(sides) == 1)
        INSIST(sides(1) == 'zhi')
        if (this%hi_is_bndry(3)) then
          this%bc_st_zhi = .true.
          this%bc_free_zhi = .true. ! surface tension boundary implies free boundary as well
          this%bc_sym_zhi = .true.
          call get_scalar_func(bc_params, 'dsigma', this%dsig_dT, stat=stat, errmsg=errmsg)
          if (stat /= 0) call this%env%log%fatal(context//errmsg)
        end if
      case default
        call this%env%log%fatal(context//'unknown "type" value: '//type)
      end select
      call piter%next
    end do

  contains

    subroutine init_free_sym

      integer :: i

      do i = 1,size(sides)
        if (.not.this%is_bndry(sides(i))) cycle
        select case (sides(i))
        case ('xlo')
          this%bc_free_xlo = .true.
          this%bc_sym_xlo = type=='symmetry'
        case ('ylo')
          this%bc_free_ylo = .true.
          this%bc_sym_ylo = type=='symmetry'
        case ('zlo')
          this%bc_free_zlo = .true.
          this%bc_sym_zlo = type=='symmetry'
        case ('xhi')
          this%bc_free_xhi = .true.
          this%bc_sym_xhi = type=='symmetry'
        case ('yhi')
          this%bc_free_yhi = .true.
          this%bc_sym_yhi = type=='symmetry'
        case ('zhi')
          this%bc_free_zhi = .true.
          this%bc_sym_zhi = type=='symmetry'
        end select
      end do

    end subroutine init_free_sym

    !! These subroutines complete the parsing of the BC_PARAMS parameter list
    !! for specific boundary condtion types and initialize the appropriate
    !! data components of the PROJECTION_BOX structure array component. These can
    !! assume a validated list of side specifiers SIDES.
    subroutine init_vel

      use scalar_func_containers, only: scalar_func_box

      integer :: i
      real(r8) :: const
      character(3) :: side
      type(parameter_list), pointer :: func_params
      type(scalar_func_box) :: f(3)
      type(bndry_face_vfunc), allocatable :: bff
      character(5), parameter :: datastrs(3) = ['xdata','ydata','zdata']

      do i = 1,3
        if (bc_params%is_sublist(datastrs(i))) then
          func_params => bc_params%sublist(datastrs(i))
          call alloc_scalar_func(f(i)%f, func_params, stat, errmsg)
          if (stat /= 0) call this%env%log%fatal(errmsg)
        else if (bc_params%is_scalar(datastrs(i))) then
          call bc_params%get(datastrs(i), const, stat=stat, errmsg=errmsg)
          if (stat /= 0) call this%env%log%fatal(context//errmsg)
          call alloc_const_scalar_func(f(i)%f, const)
        else
          call this%env%log%fatal(context//'missing valid "'//datastrs(i)//'" parameter')
        end if
      end do

      do i = 1, size(sides)
        side = sides(i)
        if (this%is_bndry(side)) then
          allocate(bff)
          call bff%init(geom, bx, side, f)
          select case (side)
          case ('xlo')
            call move_alloc(bff, this%bc_vel_xlo)
          case ('ylo')
            call move_alloc(bff, this%bc_vel_ylo)
          case ('zlo')
            call move_alloc(bff, this%bc_vel_zlo)
          case ('xhi')
            call move_alloc(bff, this%bc_vel_xhi)
          case ('yhi')
            call move_alloc(bff, this%bc_vel_yhi)
          case ('zhi')
            call move_alloc(bff, this%bc_vel_zhi)
          end select
        end if
      end do

    end subroutine init_vel

    subroutine init_prs

      integer :: i
      real(r8) :: const
      character(3) :: side
      type(parameter_list), pointer :: func_params
      class(scalar_func), allocatable :: f
      type(bndry_face_func), allocatable :: bff

      if (bc_params%is_sublist('data')) then
        func_params => bc_params%sublist('data')
        call alloc_scalar_func(f, func_params, stat, errmsg)
        if (stat /= 0) call this%env%log%fatal(errmsg)
      else if (bc_params%is_scalar('data')) then
        call bc_params%get('data', const, stat=stat, errmsg=errmsg)
        if (stat /= 0) call this%env%log%fatal(context//errmsg)
        call alloc_const_scalar_func(f, const)
      else
        call this%env%log%fatal(context//'missing valid "data" parameter')
      end if

      do i = 1, size(sides)
        side = sides(i)
        if (this%is_bndry(side)) then
          allocate(bff)
          call bff%init(geom, bx, side, f)
          select case (side)
          case ('xlo')
            call move_alloc(bff, this%bc_prs_xlo)
          case ('ylo')
            call move_alloc(bff, this%bc_prs_ylo)
          case ('zlo')
            call move_alloc(bff, this%bc_prs_zlo)
          case ('xhi')
            call move_alloc(bff, this%bc_prs_xhi)
          case ('yhi')
            call move_alloc(bff, this%bc_prs_yhi)
          case ('zhi')
            call move_alloc(bff, this%bc_prs_zhi)
          end select
        end if
      end do

    end subroutine init_prs

  end subroutine init_bc

  ! calculate lhs and rhs for the predictor system
  !
  ! note 1: Along dirichlet pressure boundaries, a 0-neumann velocity is enforced.
  !         Since the grad_vel_face_out_bndry variable is 0 by default, there is
  !         no need for a routine that does anything explicitly.
  !
  ! note 2: This is a crude way to account for solid material within the cell.
  !         In SOLVE_FOR_VELOCITY we also divide by FluidVof to account for
  !         the mass of fluid in the cell, more or less canceling out this
  !         term. Momentum advection is specifically excluded because the VOF
  !         is already accounting for the solid material.
  subroutine compute_semi_implicit_contribution(this, dt, t, temperature, &
      gradp_dyn_rho, fluid_rho, volfrac, viscosity, &
      fluid_rho_n, volfrac_n, model, vof, velocity, lhs_hypre, rhs_hypre)

    use fhypre
    use flow_model_class
    use vof_box_type

    class(predictor_box), intent(inout) :: this
    real(r8), intent(in) :: dt, t, &
        !temperature(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:), &
        gradp_dyn_rho(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:,:), &
        fluid_rho(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:), &
        volfrac(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:), &
        viscosity(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:), &
        fluid_rho_n(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:), &
        volfrac_n(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:)
    class(flow_model), intent(in) :: model
    type(vof_box), intent(in) :: vof
    real(r8), intent(inout) :: velocity(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:,:), &
        temperature(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:)
    type(hypre_obj), intent(in) :: lhs_hypre, rhs_hypre(:)

    integer :: ix,iy,iz, n, ierr
    real(r8) :: vol, cell_rhs(3), &
        mom(this%lo(1)-1:this%hi(1)+1,this%lo(2)-1:this%hi(2)+1,this%lo(3)-1:this%hi(3)+1), &
        mom_delta(this%lo(1)-1:this%hi(1)+1,this%lo(2)-1:this%hi(2)+1,this%lo(3)-1:this%hi(3)+1,3),&
        lhs(7,this%lo(1):this%hi(1),this%lo(2):this%hi(2),this%lo(3):this%hi(3)), &
        rhs(this%lo(1):this%hi(1),this%lo(2):this%hi(2),this%lo(3):this%hi(3),3), &
        viscosity_fx(this%lo(1):this%hi(1)+1,this%lo(2):this%hi(2),this%lo(3):this%hi(3)), &
        viscosity_fy(this%lo(1):this%hi(1),this%lo(2):this%hi(2)+1,this%lo(3):this%hi(3)), &
        viscosity_fz(this%lo(1):this%hi(1),this%lo(2):this%hi(2),this%lo(3):this%hi(3)+1)

    rhs = 0
    vol = product(this%dx)
    call bcs
    if (.not.model%inviscid) &
        call compute_face_viscosity(viscosity, viscosity_fx, viscosity_fy, viscosity_fz)

    ! get momentum delta
    do n = 1,3
      mom = model%density * velocity(:,:,:,n)
      call vof%advect_delta(mom, mom_delta(:,:,:,n))
    end do

    do iz = this%lo(3),this%hi(3)
      do iy = this%lo(2),this%hi(2)
        do ix = this%lo(1),this%hi(1)
          lhs(:,ix,iy,iz) = 0

          if (volfrac(ix,iy,iz) > 0) then
            ! LHS momentum & solidification
            lhs(1,ix,iy,iz) = vol * (fluid_rho(ix,iy,iz) &
                + model%density*max(volfrac_n(ix,iy,iz) - volfrac(ix,iy,iz), 0.0_r8) &
                / volfrac(ix,iy,iz))

            ! pressure
            cell_rhs = - dt*fluid_rho(ix,iy,iz)*gradp_dyn_rho(ix,iy,iz,:) * vol

            ! viscosity
            if (.not.model%inviscid) &
                call this%compute_cell_viscosity_contribution(ix,iy,iz, dt, &
                viscosity_fx, viscosity_fy, viscosity_fz, &
                volfrac, velocity, lhs(:,ix,iy,iz), cell_rhs)

            ! account for solid material (see note 2)
            cell_rhs = cell_rhs * volfrac(ix,iy,iz)

            ! advect momentum
            cell_rhs = cell_rhs + mom_delta(ix,iy,iz,:) * vol

            ! previous momentum value
            cell_rhs = cell_rhs + fluid_rho_n(ix,iy,iz)*volfrac_n(ix,iy,iz)*velocity(ix,iy,iz,:)*vol

            ! the volfrac on the LHS is divided through
            ! to the RHS so the LHS will be symmetric
            rhs(ix,iy,iz,:) = (rhs(ix,iy,iz,:) + cell_rhs) / volfrac(ix,iy,iz)
          else
            lhs(1,ix,iy,iz) = 1
            rhs(ix,iy,iz,:) = 0
          end if
        end do
      end do
    end do

    ! If inviscid, solve here. We won't solve via hypre.
    if (model%inviscid .or. this%vimpl == 0) then
      do iz = this%lo(3),this%hi(3)
        do iy = this%lo(2),this%hi(2)
          do ix = this%lo(1),this%hi(1)
            velocity(ix,iy,iz,:) = rhs(ix,iy,iz,:) / lhs(1,ix,iy,iz)
          end do
        end do
      end do
    end if

    if (.not.model%inviscid .and. this%vimpl > 0) then
      call fHYPRE_StructVectorSetBoxValues(rhs_hypre(1), this%lo, this%hi, rhs(:,:,:,1), ierr)
      call fHYPRE_StructVectorSetBoxValues(rhs_hypre(2), this%lo, this%hi, rhs(:,:,:,2), ierr)
      call fHYPRE_StructVectorSetBoxValues(rhs_hypre(3), this%lo, this%hi, rhs(:,:,:,3), ierr)
      call fHYPRE_StructMatrixSetBoxValues(lhs_hypre, this%lo, this%hi, [0,1,2,3,4,5,6], lhs, ierr)
      INSIST(ierr == 0)
    end if

  contains

    subroutine st_bc()

      real(r8) :: dTdx, st, dsig_dT

      ! TODO: Here we should work with face temperatures instead of cell-centered ones.

      ! Extrapolate temperature to ghost cells using two neighbors.
      ! This results in a one-sided difference along boundaries below.
      ! If some direction is flat, we don't want a gradient there, and
      ! symmetry is used instead.
      if (this%is_bndry('xlo') .and. this%is_bndry('xhi') .and. this%hi(1) == this%lo(1)) then
        temperature(this%lo(1)-1,:,:) = temperature(this%lo(1),:,:)
        temperature(this%hi(1)+1,:,:) = temperature(this%lo(1),:,:)
      else
        if (this%is_bndry('xlo')) temperature(this%lo(1)-1,:,:) = 2*temperature(this%lo(1),:,:) - temperature(this%lo(1)+1,:,:)
        if (this%is_bndry('xhi')) temperature(this%hi(1)+1,:,:) = 2*temperature(this%hi(1),:,:) - temperature(this%hi(1)-1,:,:)
      end if

      if (this%is_bndry('ylo') .and. this%is_bndry('yhi') .and. this%hi(2) == this%lo(2)) then
        temperature(:,this%lo(2)-1,:) = temperature(:,this%lo(2),:)
        temperature(:,this%lo(2)+1,:) = temperature(:,this%lo(2),:)
      else
        if (this%is_bndry('ylo')) temperature(:,this%lo(2)-1,:) = 2*temperature(:,this%lo(2),:) - temperature(:,this%lo(2)+1,:)
        if (this%is_bndry('yhi')) temperature(:,this%hi(2)+1,:) = 2*temperature(:,this%hi(2),:) - temperature(:,this%hi(2)-1,:)
      end if

      if (this%is_bndry('zlo') .and. this%is_bndry('zhi') .and. this%hi(3) == this%lo(3)) then
        temperature(:,:,this%lo(3)-1) = temperature(:,:,this%lo(3))
        temperature(:,:,this%lo(3)+1) = temperature(:,:,this%lo(3))
      else
        if (this%is_bndry('zlo')) temperature(:,:,this%lo(3)-1) = 2*temperature(:,:,this%lo(3)) - temperature(:,:,this%lo(3)+1)
        if (this%is_bndry('zhi')) temperature(:,:,this%hi(3)+1) = 2*temperature(:,:,this%hi(3)) - temperature(:,:,this%hi(3)-1)
      end if

      ! tangential surface tension boundary condition
      ! currently only allowed on the z+ boundary
      if (this%bc_st_zhi) then
        iz = this%hi(3)
        do iy = this%lo(2),this%hi(2)
          do ix = this%lo(1),this%hi(1)
            dsig_dT = this%dsig_dT%eval([temperature(ix,iy,iz)])

            dTdx = (temperature(ix+1,iy,iz) - temperature(ix-1,iy,iz)) / (2*this%dx(1))
            st = (volfrac(ix,iy,iz) * dsig_dT / this%dx(3)) * dTdx
            rhs(ix,iy,iz,1) = rhs(ix,iy,iz,1) + dt * st * vol

            dTdx = (temperature(ix,iy+1,iz) - temperature(ix,iy-1,iz)) / (2*this%dx(2))
            st = (volfrac(ix,iy,iz) * dsig_dT / this%dx(3)) * dTdx
            rhs(ix,iy,iz,2) = rhs(ix,iy,iz,2) + dt * st * vol
          end do
        end do
      end if

    end subroutine st_bc

    subroutine bcs()

      call st_bc

      ! apply velocity boundary conditions
      if (allocated(this%bc_vel_xlo)) then
        call this%bc_vel_xlo%compute(t)
        velocity(this%lo(1)-1,this%lo(2):this%hi(2),this%lo(3):this%hi(3),:) = &
            2*this%bc_vel_xlo%value &
            - velocity(this%lo(1),this%lo(2):this%hi(2),this%lo(3):this%hi(3),:)
      end if
      if (allocated(this%bc_vel_xhi)) then
        call this%bc_vel_xhi%compute(t)
        velocity(this%hi(1)+1,this%lo(2):this%hi(2),this%lo(3):this%hi(3),:) = &
            2*this%bc_vel_xhi%value &
            - velocity(this%hi(1),this%lo(2):this%hi(2),this%lo(3):this%hi(3),:)
      end if
      if (allocated(this%bc_vel_ylo)) then
        call this%bc_vel_ylo%compute(t)
        velocity(this%lo(1):this%hi(1),this%lo(2)-1,this%lo(3):this%hi(3),:) = &
            2*this%bc_vel_ylo%value &
            - velocity(this%lo(1):this%hi(1),this%lo(2),this%lo(3):this%hi(3),:)
      end if
      if (allocated(this%bc_vel_yhi)) then
        call this%bc_vel_yhi%compute(t)
        velocity(this%lo(1):this%hi(1),this%hi(2)+1,this%lo(3):this%hi(3),:) = &
            2*this%bc_vel_yhi%value &
            - velocity(this%lo(1):this%hi(1),this%hi(2),this%lo(3):this%hi(3),:)
      end if
      if (allocated(this%bc_vel_zlo)) then
        call this%bc_vel_zlo%compute(t)
        velocity(this%lo(1):this%hi(1),this%lo(2):this%hi(2),this%lo(3)-1,:) = &
            2*this%bc_vel_zlo%value &
            - velocity(this%lo(1):this%hi(1),this%lo(2):this%hi(2),this%lo(3),:)
      end if
      if (allocated(this%bc_vel_zhi)) then
        call this%bc_vel_zhi%compute(t)
        velocity(this%lo(1):this%hi(1),this%lo(2):this%hi(2),this%hi(3)+1,:) = &
            2*this%bc_vel_zhi%value &
            - velocity(this%lo(1):this%hi(1),this%lo(2):this%hi(2),this%hi(3),:)
      end if

      ! at pressure boundary conditions, set velocity gradient to zero
      ! (velocity in ghosts same as adjacent cell)
      ! need to apply pressure boundary conditions or ensure pressure and TODO? velocity
      ! bcs are applied after initialization
      if (allocated(this%bc_prs_xlo)) velocity(this%lo(1)-1,:,:,:) = velocity(this%lo(1),:,:,:)
      if (allocated(this%bc_prs_xhi)) velocity(this%hi(1)+1,:,:,:) = velocity(this%hi(1),:,:,:)
      if (allocated(this%bc_prs_ylo)) velocity(:,this%lo(2)-1,:,:) = velocity(:,this%lo(2),:,:)
      if (allocated(this%bc_prs_yhi)) velocity(:,this%hi(2)+1,:,:) = velocity(:,this%hi(2),:,:)
      if (allocated(this%bc_prs_zlo)) velocity(:,:,this%lo(3)-1,:) = velocity(:,:,this%lo(3),:)
      if (allocated(this%bc_prs_zhi)) velocity(:,:,this%hi(3)+1,:) = velocity(:,:,this%hi(3),:)

      if (this%bc_free_xlo) then
        velocity(this%lo(1)-1,:,:,:) = velocity(this%lo(1),:,:,:)
        if (this%bc_sym_xlo) velocity(this%lo(1)-1,:,:,1) = -velocity(this%lo(1)-1,:,:,1)
      end if
      if (this%bc_free_xhi) then
        velocity(this%hi(1)+1,:,:,:) = velocity(this%hi(1),:,:,:)
        if (this%bc_sym_xhi) velocity(this%hi(1)+1,:,:,1) = -velocity(this%hi(1)+1,:,:,1)
      end if
      if (this%bc_free_ylo) then
        velocity(:,this%lo(2)-1,:,:) = velocity(:,this%lo(2),:,:)
        if (this%bc_sym_ylo) velocity(:,this%lo(2)-1,:,2) = -velocity(:,this%lo(2)-1,:,2)
      end if
      if (this%bc_free_yhi) then
        velocity(:,this%hi(2)+1,:,:) = velocity(:,this%hi(2),:,:)
        if (this%bc_sym_yhi) velocity(:,this%hi(2)+1,:,2) = -velocity(:,this%hi(2)+1,:,2)
      end if
      if (this%bc_free_zlo) then
        velocity(:,:,this%lo(3)-1,:) = velocity(:,:,this%lo(3),:)
        if (this%bc_sym_zlo) velocity(:,:,this%lo(3)-1,3) = -velocity(:,:,this%lo(3)-1,3)
      end if
      if (this%bc_free_zhi) then
        velocity(:,:,this%hi(3)+1,:) = velocity(:,:,this%hi(3),:)
        if (this%bc_sym_zhi) velocity(:,:,this%hi(3)+1,3) = -velocity(:,:,this%hi(3)+1,3)
      end if

    end subroutine bcs

    subroutine compute_face_viscosity(viscosity, viscosity_fx, viscosity_fy, viscosity_fz)

      real(r8), intent(in) :: viscosity(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:)
      real(r8), intent(out), dimension(this%lo(1):,this%lo(2):,this%lo(3):) :: &
          viscosity_fx, viscosity_fy, viscosity_fz

      ! x faces
      do iz = this%lo(3),this%hi(3)
        do iy = this%lo(2),this%hi(2)
          do ix = this%lo(1),this%hi(1)+1
            if (any(viscosity(ix-1:ix,iy,iz) == 0)) then
              viscosity_fx(ix,iy,iz) = maxval(viscosity(ix-1:ix,iy,iz))
            else
              viscosity_fx(ix,iy,iz) = 2 * product(viscosity(ix-1:ix,iy,iz)) &
                  / sum(viscosity(ix-1:ix,iy,iz))
            end if
          end do
        end do
      end do

      ! y faces
      do iz = this%lo(3),this%hi(3)
        do iy = this%lo(2),this%hi(2)+1
          do ix = this%lo(1),this%hi(1)
            if (any(viscosity(ix,iy-1:iy,iz) == 0)) then
              viscosity_fy(ix,iy,iz) = maxval(viscosity(ix,iy-1:iy,iz))
            else
              viscosity_fy(ix,iy,iz) = 2 * product(viscosity(ix,iy-1:iy,iz)) &
                  / sum(viscosity(ix,iy-1:iy,iz))
            end if
          end do
        end do
      end do

      ! z faces
      do iz = this%lo(3),this%hi(3)+1
        do iy = this%lo(2),this%hi(2)
          do ix = this%lo(1),this%hi(1)
            if (any(viscosity(ix,iy,iz-1:iz) == 0)) then
              viscosity_fz(ix,iy,iz) = maxval(viscosity(ix,iy,iz-1:iz))
            else
              viscosity_fz(ix,iy,iz) = 2 * product(viscosity(ix,iy,iz-1:iz)) &
                  / sum(viscosity(ix,iy,iz-1:iz))
            end if
          end do
        end do
      end do

    end subroutine compute_face_viscosity

  end subroutine compute_semi_implicit_contribution

  ! note: Here we are assuming the viscosity is constant in the liquid phase, so that
  !       the face viscosity is equal to the viscosity property for all non-solid faces.
  !       In the future, this will change if we add more materials or make viscosity
  !       dependent on temperature.
  subroutine compute_cell_viscosity_contribution(this, ix, iy, iz, dt, &
      viscosity_fx, viscosity_fy, viscosity_fz, vof, velocity, lhs, rhs)

    class(predictor_box), intent(in) :: this
    integer, intent(in) :: ix,iy,iz
    real(r8), intent(in) :: dt, &
        viscosity_fx(this%lo(1):,this%lo(2):,this%lo(3):), &
        viscosity_fy(this%lo(1):,this%lo(2):,this%lo(3):), &
        viscosity_fz(this%lo(1):,this%lo(2):,this%lo(3):), &
        vof(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:), &
        velocity(this%lo(1)-1:,this%lo(2)-1:,this%lo(3)-1:,:)
    real(r8), intent(inout) :: lhs(:), rhs(:)

    real(r8) :: tmp, vrhs(3)

    vrhs = 0

    ! x- face
    tmp = dt * viscosity_fx(ix,iy,iz) * this%dx(2)*this%dx(3) / this%dx(1)

    if (ix==this%lo(1) .and. this%lo_is_bndry(1)) then
      if (allocated(this%bc_vel_xlo)) then
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
        vrhs = vrhs + this%vimpl * 2 * tmp * this%bc_vel_xlo%value(iy,iz,:)
      end if
    else
      if (vof(ix-1,iy,iz) > 0) then
        lhs(2) = lhs(2) - this%vimpl * tmp
        lhs(1) = lhs(1) + this%vimpl * tmp
      else
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
      end if
    end if
    if (vof(ix-1,iy,iz) > 0) then
      vrhs = vrhs - (1 - this%vimpl) * tmp * (velocity(ix,iy,iz,:) - velocity(ix-1,iy,iz,:))
    else
      vrhs = vrhs - 2 * (1 - this%vimpl) * tmp * velocity(ix,iy,iz,:)
    end if

    ! x+ face
    tmp = dt * viscosity_fx(ix+1,iy,iz) * this%dx(2)*this%dx(3) / this%dx(1)

    if (ix==this%hi(1) .and. this%hi_is_bndry(1)) then
      if (allocated(this%bc_vel_xhi)) then
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
        vrhs = vrhs + this%vimpl * 2 * tmp * this%bc_vel_xhi%value(iy,iz,:)
      end if
    else
      if (vof(ix+1,iy,iz) > 0) then
        lhs(3) = lhs(3) - this%vimpl * tmp
        lhs(1) = lhs(1) + this%vimpl * tmp
      else
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
      end if
    end if
    if (vof(ix+1,iy,iz) > 0) then
      vrhs = vrhs + (1 - this%vimpl) * tmp * (velocity(ix+1,iy,iz,:) - velocity(ix,iy,iz,:))
    else
      vrhs = vrhs - 2 * (1 - this%vimpl) * tmp * velocity(ix,iy,iz,:)
    end if

    ! y- face
    tmp = dt * viscosity_fy(ix,iy,iz) * this%dx(1)*this%dx(3) / this%dx(2)

    if (iy==this%lo(2) .and. this%lo_is_bndry(2)) then
      if (allocated(this%bc_vel_ylo)) then
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
        vrhs = vrhs + this%vimpl * 2 * tmp * this%bc_vel_ylo%value(ix,iz,:)
      end if
    else
      if (vof(ix,iy-1,iz) > 0) then
        lhs(4) = lhs(4) - this%vimpl * tmp
        lhs(1) = lhs(1) + this%vimpl * tmp
      else
        lhs(1) = lhs(1) + 2 * this%vimpl * tmp
      end if
    end if
    if (vof(ix,iy-1,iz) > 0) then
      vrhs = vrhs - (1 - this%vimpl) * tmp * (velocity(ix,iy,iz,:) - velocity(ix,iy-1,iz,:))
    else
      vrhs = vrhs - 2 * (1 - this%vimpl) * tmp * velocity(ix,iy,iz,:)
    end if

    ! y+ face
    tmp = dt * viscosity_fy(ix,iy+1,iz) * this%dx(1)*this%dx(3) / this%dx(2)

    if (iy==this%hi(2) .and. this%hi_is_bndry(2)) then
      if (allocated(this%bc_vel_yhi)) then
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
        vrhs = vrhs + this%vimpl * 2 * tmp * this%bc_vel_yhi%value(ix,iz,:)
      end if
    else
      if (vof(ix,iy+1,iz) > 0) then
        lhs(5) = lhs(5) - this%vimpl * tmp
        lhs(1) = lhs(1) + this%vimpl * tmp
      else
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
      end if
    end if
    if (vof(ix,iy+1,iz) > 0) then
      vrhs = vrhs + (1 - this%vimpl) * tmp * (velocity(ix,iy+1,iz,:) - velocity(ix,iy,iz,:))
    else
      vrhs = vrhs - 2 * (1 - this%vimpl) * tmp * velocity(ix,iy,iz,:)
    end if

    ! z- face
    tmp = dt * viscosity_fz(ix,iy,iz) * this%dx(1)*this%dx(2) / this%dx(3)

    if (iz==this%lo(3) .and. this%lo_is_bndry(3)) then
      if (allocated(this%bc_vel_zlo)) then
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
        vrhs = vrhs + this%vimpl * 2 * tmp * this%bc_vel_zlo%value(ix,iy,:)
      end if
    else
      if (vof(ix,iy,iz-1) > 0) then
        lhs(6) = lhs(6) - this%vimpl * tmp
        lhs(1) = lhs(1) + this%vimpl * tmp
      else
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
      end if
    end if
    if (vof(ix,iy,iz-1) > 0) then
      vrhs = vrhs - (1 - this%vimpl) * tmp * (velocity(ix,iy,iz,:) - velocity(ix,iy,iz-1,:))
    else
      vrhs = vrhs - 2 * (1 - this%vimpl) * tmp * velocity(ix,iy,iz,:)
    end if

    ! z+ face
    tmp = dt * viscosity_fz(ix,iy,iz+1) * this%dx(1)*this%dx(2) / this%dx(3)

    if (iz==this%hi(3) .and. this%hi_is_bndry(3)) then
      if (allocated(this%bc_vel_zhi)) then
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
        vrhs = vrhs + this%vimpl * 2 * tmp * this%bc_vel_zhi%value(ix,iy,:)
      end if
    else
      if (vof(ix,iy,iz+1) > 0) then
        lhs(7) = lhs(7) - this%vimpl * tmp
        lhs(1) = lhs(1) + this%vimpl * tmp
      else
        lhs(1) = lhs(1) + this%vimpl * 2 * tmp
      end if
    end if
    if (vof(ix,iy,iz+1) > 0) then
      vrhs = vrhs + (1 - this%vimpl) * tmp * (velocity(ix,iy,iz+1,:) - velocity(ix,iy,iz,:))
    else
      vrhs = vrhs - 2 * (1 - this%vimpl) * tmp * velocity(ix,iy,iz,:)
    end if

    rhs = rhs + vrhs

  end subroutine compute_cell_viscosity_contribution

  !! This auxiliary subroutine gets the scalar function specified by the value
  !! of the parameter PARAM in the parameter list PLIST. The parameter value is
  !! either a real acalar or a sublist that defines the function.

  subroutine get_scalar_func(plist, param, f, stat, errmsg)

    use scalar_func_factories
    use parameter_list_type

    type(parameter_list), intent(inout) :: plist
    character(*), intent(in) :: param
    class(scalar_func), allocatable, intent(out) :: f
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    real(r8) :: const
    type(parameter_list), pointer :: fparams

    if (plist%is_parameter(param)) then
      if (plist%is_sublist(param)) then ! function defined by parameter list
        fparams => plist%sublist(param)
        call alloc_scalar_func(f, fparams, stat, errmsg)
        if (stat /= 0) return
      else  ! it must be a constant value
        call plist%get(param, const, stat=stat, errmsg=errmsg)
        if (stat /= 0) return
        call alloc_const_scalar_func(f, const)
      end if
    else
      stat = 1
      errmsg = 'missing "' // param // '" parameter'
      return
    end if

  end subroutine get_scalar_func

end module predictor_box_type
