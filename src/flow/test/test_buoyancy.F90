!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

program test_buoyancy

#ifdef NAGFOR
  use,intrinsic :: f90_unix, only: exit
#endif
  use,intrinsic :: iso_fortran_env, only: output_unit
  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use mpi
  use amrex_base_module
  use parameter_list_type
  use parameter_list_json
  use truchas_amrex_init_proc
  use amrex_mesh_type
  use simlog_type
  use truchas_env_type
  implicit none

  integer :: j, nproc, this_rank, ierr, status = 0
  type(parameter_list) :: params
  type(truchas_env), target :: env
  type(amrex_mesh) :: mesh
#if defined(__GFORTRAN__)
  procedure(amrex_finalize), pointer :: pp
#endif
  character(:), allocatable :: paramstr

  call truchas_amrex_init(params)

#if defined(__GFORTRAN__)
  pp => amrex_finalize
  pp => amrex_finalize
  call env%log%init(MPI_COMM_WORLD, [output_unit], verbosity=VERB_NOISY, finalize=pp)
#else
  call env%log%init(MPI_COMM_WORLD, [output_unit], verbosity=VERB_NOISY, finalize=amrex_finalize)
#endif

  call MPI_Comm_rank(MPI_COMM_WORLD, this_rank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)
  env%comm = MPI_COMM_WORLD
  env%rank = this_rank

  call params%set('lo', [0,0,0])
  call params%set('hi', [31,31,31])
  call params%set('box-size', 16)
  call params%set('prob-lo', [0.0_r8, 0.0_r8, 0.0_r8])
  call params%set('prob-hi', [1.0_r8, 1.0_r8, 1.0_r8])
  call mesh%init(params)

  do j = 0, nproc-1
    if (j == this_rank) then
      write(*,'("process ",i0," has ",i0," boxes")') j, mesh%nbox
    endif
    call MPI_Barrier(MPI_COMM_WORLD, ierr)
  end do

  paramstr = '{&
      &"model":{ "density": 1.0, "body force": [1.0, 1.0, 1.0], &
      &              "density variation":{ "type": "polynomial", &
      &                                    "poly-coef": [1.0], "poly-powers": [1]}}, &
      &"temperature init":{ &
      &  "type": "polynomial", "poly-coef": [1.0, 1.0, 1.0], &
      &  "poly-powers": [[1,0,0], [0,1,0], [0,0,1]]}, &
      &"bc":{ &
      &  "vel side":{ "type": "velocity", "sides": ["xlo","xhi","ylo","yhi","zlo","zhi"], &
      &               "xdata": 0.0, "ydata": 0.0, "zdata": 0.0}},&
      &"pressure-solver":{ "tol": 1e-12}}'
  call test(paramstr, 1)

  call amrex_finalize()
  call exit(status)

contains

  subroutine test(paramstr, dir)

    use flow_sim_type

    character(*), intent(in) :: paramstr
    integer, intent(in) :: dir

    type(flow_sim) :: flow
    type(parameter_list), pointer :: params => null()
    character(:), allocatable :: errmsg
    real(r8) :: prs_ex, linf, linf_global, xcen(3)
    integer :: ix, iy, iz
    real(r8), pointer :: prs(:,:,:,:) => null()
    type(amrex_mfiter) :: mfi
    type(amrex_box) :: bx

    call parameter_list_from_json_string(paramstr, params, errmsg)
    INSIST(associated(params))

    call flow%init(env, mesh, params)

    ! renormalize pressure
    call flow%flow%pressure%plus(-flow%flow%pressure%min(1,0), 1, 1, 1)

    ! because the min sets a cell center to 0
    prs_ex = sum(mesh%geom%dx/2) + sum(mesh%geom%dx/2)**2 / 2
    call flow%flow%pressure%plus(prs_ex, 1, 1, 1)

    ! check error
    linf = 0
    call mesh%mfiter_build(mfi)
    do while (mfi%next())
      bx = mfi%tilebox()
      prs => flow%flow%pressure%dataptr(mfi)

      do iz = bx%lo(3),bx%hi(3)
        do iy = bx%lo(2),bx%hi(2)
          do ix = bx%lo(1),bx%hi(1)
            xcen = mesh%geom%get_physical_location([ix,iy,iz]) + mesh%geom%dx / 2
            prs_ex = sum(xcen) + sum(xcen)**2 / 2
            linf = max(linf, abs(prs(ix,iy,iz,1) - prs_ex))
          end do
        end do
      end do
    end do

    call MPI_Reduce(linf, linf_global, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD, ierr)
    if (this_rank==0) then
      print '(a,3es13.3)', 'linf: ', linf_global
      if (linf_global > 1e-11_r8) status = 1
    end if
    call MPI_Bcast(status, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)

  end subroutine test

end program test_buoyancy
