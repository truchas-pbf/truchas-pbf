!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

program test_marangoni_flow

#ifdef NAGFOR
  use,intrinsic :: f90_unix, only: exit
#endif
  use,intrinsic :: iso_fortran_env, only: output_unit
  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use mpi
  use amrex_base_module
  use parameter_list_type
  use parameter_list_json
  use truchas_amrex_init_proc
  use timer_tree_type
  use amrex_mesh_type
  use simlog_type
  use truchas_env_type
  implicit none

  integer :: j, nproc, this_rank, ierr, status = 0
  type(parameter_list) :: params
  type(truchas_env), target :: env
  type(amrex_mesh) :: mesh
#if defined(__GFORTRAN__)
  procedure(amrex_finalize), pointer :: pp
#endif
  character(:), allocatable :: paramstr

  call truchas_amrex_init(params)

#if defined(__GFORTRAN__)
  pp => amrex_finalize
  call env%log%init(MPI_COMM_WORLD, [output_unit], verbosity=VERB_NOISY, finalize=pp)
#else
  call env%log%init(MPI_COMM_WORLD, [output_unit], verbosity=VERB_NOISY, finalize=amrex_finalize)
#endif

  call MPI_Comm_rank(MPI_COMM_WORLD, this_rank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)
  env%comm = MPI_COMM_WORLD
  env%rank = this_rank

  call params%set('lo', [0,0,0])
  call params%set('hi', [31,31,31])
  call params%set('box-size', 16)
  call params%set('prob-lo', [-0.5_r8, -0.5_r8, -0.5_r8])
  call params%set('prob-hi', [ 0.5_r8,  0.5_r8,  0.5_r8])
  call mesh%init(params)

  do j = 0, nproc-1
    if (j == this_rank) then
      write(*,'("process ",i0," has ",i0," boxes")') j, mesh%nbox
    endif
    call MPI_Barrier(MPI_COMM_WORLD, ierr)
  end do

  paramstr = '{"max dt": 1e-2, "viscous-implicitness": 0.5, &
      &"model":{"body force": [0.0, 0.0, -1.0], "density": 1.0, "viscosity": 1e-2, &
      &              "density variation":{ "type": "polynomial", &
      &                                    "poly-coef": [10.0], "poly-powers": [1]}}, &
      &"temperature init":{ &
      &  "type": "polynomial", "poly-coef": [1.0, 1.0], &
      &  "poly-powers": [[2,0,0], [0,2,0]]}, &
      &"geometry init":{ &
      &  "liquid":{ &
      &    "material-id": 1, &
      &    "region":{ "type": "sphere", "center": [0.0, 0.0, 0.3], "radius": 0.3 }}, &
      &  "solid":{ &
      &    "material-id": 2, &
      &    "region":{ "type": "fill" }}}, &
      &"bc":{ &
      &  "vel side":{ "type": "velocity", "sides": ["xlo","xhi","ylo","yhi","zlo"], &
      &               "xdata": 0.0, "ydata": 0.0, "zdata": 0.0}, &
      &  "st side":{  "type": "marangoni", "sides": ["zhi"], "dsigma": 1e-1}}}'
  call test(paramstr, 1)

  !! Write some timing info.
  call env%log%info('')
  call env%log%info('Timing Summary:')
  call env%log%info('')
  if (this_rank == 0) call write_timer_tree(output_unit, indent=3)

  call amrex_finalize()
  call exit(status)

contains

  subroutine test(paramstr, dir)

    use flow_sim_type

    character(*), intent(in) :: paramstr
    integer, intent(in) :: dir

    type(flow_sim) :: flow
    type(parameter_list), pointer :: params => null()
    character(:), allocatable :: errmsg
    real(r8) :: tmax
    integer :: i, ndumps

    call parameter_list_from_json_string(paramstr, params, errmsg)
    INSIST(associated(params))

    call flow%init(env, mesh, params)
    ndumps = 1
    tmax = 1e-2_r8
    ndumps = 10
    tmax = 1
    flow%tout = [(i * tmax / ndumps, i=1,ndumps)]

    call flow%write_solution()
    call flow%run()

  end subroutine test

end program test_marangoni_flow
