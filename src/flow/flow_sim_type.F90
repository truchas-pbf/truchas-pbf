!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module flow_sim_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use amrex_mesh_type
  use amrex_base_module, only: amrex_multifab
  use flow_model1_type
  use flow_solver_type
  use parameter_list_type
  use truchas_env_type
  use timer_tree_type
  implicit none
  private

  type, public :: flow_sim
    type(truchas_env), pointer :: env => null()
    type(amrex_mesh), pointer :: mesh => null()
    type(parameter_list), pointer :: params => null()

    type(flow_model1) :: model
    type(flow_solver) :: flow

    integer :: ndump = 0, nstep = 0, print_freq = 10
    real(r8) :: t, max_dt, min_dt = 1e-10_r8
    real(r8), allocatable :: tout(:)

    type(amrex_multifab) :: volfrac, temperature
  contains
    procedure :: init
    procedure :: run
    procedure :: step
    procedure :: solve_to_time
    procedure :: write_solution
    procedure, private :: timestep_size
  end type flow_sim

contains

  subroutine init(this, env, mesh, params)

    use scalar_func_class
    use scalar_func_factories, only: alloc_scalar_func
    use material_geometry_type
    use vof_init

    class(flow_sim), intent(out) :: this
    type(truchas_env), intent(in), target :: env
    type(amrex_mesh),  intent(in), target :: mesh
    type(parameter_list), intent(inout) :: params

    integer :: stat
    character(:), allocatable :: context, errmsg
    type(parameter_list), pointer :: params_tmp
    integer :: n
    real(r8), allocatable :: vconst(:)
    class(scalar_func), allocatable :: f
    type(material_geometry) :: matl_geom
    type(amrex_multifab) :: face_vel(3), cell_vel
    type(parameter_list), pointer :: plist

    call start_timer('flow init')

    this%env => env
    this%mesh => mesh
    this%t = 0

    ! parse input parameter list
    context = 'processing ' // params%name() // ': '

    if (params%is_sublist('model')) then
      plist => params%sublist('model')
      call this%model%init(this%mesh, this%env, plist)
    else
      call this%env%log%fatal(context//'missing "model" sublist parameter')
    end if
    call this%flow%init(env, this%model, params)

    if (params%is_scalar('max dt')) then
      call params%get('max dt', this%max_dt, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
    else
      this%max_dt = 1e-1_r8
    end if

    ! initial temperature field
    call mesh%multifab_build(this%temperature, nc=1, ng=1)
    if (params%is_sublist('temperature init')) then
      params_tmp => params%sublist('temperature init')
      call alloc_scalar_func(f, params_tmp, stat, errmsg)
      if (stat /= 0) call this%env%log%fatal(errmsg)
      call this%mesh%compute_mesh_func(f, this%temperature)
    else
      call this%temperature%setval(0.0_r8)
    end if

    ! initial vof field
    call mesh%multifab_build(this%volfrac, nc=1, ng=1)
    if (params%is_sublist('geometry init')) then
      params_tmp => params%sublist('geometry init')
      call matl_geom%init(env, params_tmp, [1,2])
      call vof_initialize(mesh, matl_geom, 6, 2, this%volfrac)
    else
      call this%volfrac%setval(1.0_r8)
    end if

    ! initial velocity field (cell and face)
    call mesh%multifab_build(cell_vel, nc=3, ng=0)
    call mesh%multifab_build(face_vel(1), nc=1, ng=0, nodal=XFACE_NODAL)
    call mesh%multifab_build(face_vel(2), nc=1, ng=0, nodal=YFACE_NODAL)
    call mesh%multifab_build(face_vel(3), nc=1, ng=0, nodal=ZFACE_NODAL)
    if (params%is_sublist('velocity init')) then
      params_tmp => params%sublist('velocity init')
      if (params_tmp%is_vector('velocity')) then
        call params_tmp%get('velocity', vconst, stat=stat, errmsg=errmsg)
        if (stat /= 0) call this%env%log%fatal(context//errmsg)
        do n = 1, 3
          call face_vel(n)%setval(vconst(n))
          call cell_vel%setval(vconst(n), n, 1)
        end do
      else
        ! TODO: add a function option here
        INSIST(.false.)
      end if
    else
      call face_vel(1)%setval(0.0_r8)
      call face_vel(2)%setval(0.0_r8)
      call face_vel(3)%setval(0.0_r8)
      call cell_vel%setval(0.0_r8)
    end if

    call this%flow%set_initial_state(this%temperature, this%volfrac, cell_vel, face_vel)

    call stop_timer('flow init')

  end subroutine init

  subroutine run(this)

    class(flow_sim), intent(inout) :: this

    integer :: n

    call this%write_solution()
    do n = 1,size(this%tout)
      call this%solve_to_time(this%tout(n))
      call this%write_solution()
    end do

  end subroutine run

  subroutine solve_to_time(this, tout)

    class(flow_sim), intent(inout) :: this
    real(r8), intent(in) :: tout

    real(r8) :: dt
    integer :: ierr
    character(128) :: diag

    do while (this%t < tout)
      dt = this%timestep_size(tout)

      ! if the timestep is almost enough to finish this cycle, set it to the remaining time
      ! this avoids extremely small timesteps in the next iteration
      if (abs(dt - (tout - this%t)) < 1e4_r8*epsilon(1.0_r8)) dt = tout - this%t

      call this%step(dt, ierr)
      if (ierr /= 0) cycle
      this%t = this%t + dt
      this%nstep = this%nstep + 1

      if (modulo(this%nstep,this%print_freq)==0) then
        write(diag, '(a,i5,2es15.5,a,3es15.5)') 'step ', this%nstep, this%t, dt, '    ', &
            this%flow%face_velocity(1)%norm0(1), this%flow%face_velocity(2)%norm0(1), &
            this%flow%face_velocity(3)%norm0(1)
        call this%env%log%info(trim(diag))
      end if
    end do

  end subroutine solve_to_time

  function timestep_size(this, tout) result(dt)

    class(flow_sim), intent(in) :: this
    real(r8), intent(in) :: tout
    real(r8) :: dt

    dt = this%flow%timestep_limit()
    dt = min(dt, tout - this%t, this%max_dt)
    if (dt < this%min_dt) call this%env%log%fatal("flow: too small timestep")

  end function timestep_size

  subroutine step(this, dt, ierr)

    class(flow_sim), intent(inout) :: this
    real(r8), intent(in) :: dt
    integer, intent(out) :: ierr

    type(amrex_multifab) :: fluid_rho_n, volfrac_n

    ierr = 0

    call this%mesh%multifab_build(fluid_rho_n, nc=1, ng=1)
    call this%mesh%multifab_build(volfrac_n, nc=1, ng=1)
    call fluid_rho_n%copy(this%flow%fluid_rho, 1, 1, 1, 1)
    call volfrac_n%copy(this%volfrac, 1, 1, 1, 1)

    call this%flow%vol_advect_step(dt, this%volfrac)
    call this%flow%step(dt, this%t, this%temperature, volfrac_n, this%volfrac, fluid_rho_n, ierr)

    if (ierr == 0) call this%flow%commit_pending_state

  end subroutine step

  subroutine write_solution(this)

    use amrex_base_module, only: amrex_string, amrex_string_build, amrex_write_plotfile

    class(flow_sim), intent(inout) :: this
    type(amrex_multifab) :: plotmf
    type(amrex_string) :: label(4)
    character(16) :: filename

    write(filename,'("plt",i4.4)') this%ndump
    ! amrex dump setup
    call amrex_string_build(label(1), 'velocity-x')
    call amrex_string_build(label(2), 'velocity-y')
    call amrex_string_build(label(3), 'velocity-z')
    call amrex_string_build(label(4), 'pressure')

    call this%mesh%multifab_build(plotmf, nc=4, ng=0)

    call plotmf%copy(this%flow%velocity,1,1,3,0)
    call plotmf%copy(this%flow%pressure,1,4,1,0)

    call amrex_write_plotfile(trim(filename),1,[plotmf], &
        label,[this%mesh%geom],this%t,[1],[1])

    this%ndump = this%ndump + 1

  end subroutine write_solution

end module flow_sim_type
