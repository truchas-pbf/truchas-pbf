!!
!! Neil N. Carlson <nnc@lanl.gov>
!! December 2016
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program test_scan_path_type

  use scan_path_type
  use,intrinsic :: iso_fortran_env, only: r8 => real64
#ifdef NAGFOR
  use,intrinsic :: f90_unix, only: exit
#endif
  implicit none

  integer :: status = 0

  call test_start_seg
  call test_final_seg
  call test_minimal
  call test_path
  call test_partition

  call exit(status)

contains

  !! Test appending a start segment.  This does not produce a valid scan path
  !! but we can still position the scan path to the segment and query data
  !! from the segment.

  subroutine test_start_seg

    type(scan_path) :: sp
    real(r8) :: t1 = 1, r1(2) = [1,2], r(2)
    integer :: flags = 2

    call sp%append_path_segment(new_start_segment(t1, r1, flags))
    call sp%set_segment(-1.0_r8)

    call sp%get_position(-1.0_r8, r)
    if (any(r /= r1)) call write_fail('test_start_seg: wrong position')

    if (sp%is_flag_set(0)) call write_fail('test_start_seg: flag 0 wrong')
    if (.not.sp%is_flag_set(1)) call write_fail('test_start_seg: flag 1 wrong')

    if (sp%is_valid()) call write_fail('test_start_seg: validity check failed')

  end subroutine test_start_seg

  !! Test appending a final segment.  This does not produce a valid scan path
  !! but we can still position the scan path to the segment and query data
  !! from the segment.

  subroutine test_final_seg

    type(scan_path) :: sp
    real(r8) :: t0 = 1, r0(2) = [1,2], r(2)
    integer :: flags = 2

    call sp%append_path_segment(new_final_segment(t0, r0, flags))
    call sp%set_segment(2.0_r8)

    call sp%get_position(2.0_r8, r)
    if (any(r /= r0)) call write_fail('test_final_seg: wrong position')

    if (sp%is_flag_set(0)) call write_fail('test_final_seg: flag 0 wrong')
    if (.not.sp%is_flag_set(1)) call write_fail('test_final_seg: flag 1 wrong')

    if (sp%is_valid()) call write_fail('test_final_seg: validity check failed')

  end subroutine test_final_seg

  !! Test a minimally valid scan path (start and final segments)

  subroutine test_minimal

    type(scan_path) :: sp
    real(r8) :: t1 = 1, r1(2) = [1,2], r0(2) = [3,2], r(2)

    !! A discontinuous path for checking correct scan path positioning
    call sp%append_path_segment(new_start_segment(t1, r1, flags=2))
    call sp%append_path_segment(new_final_segment(t1, r0, flags=1))

    if (.not.sp%is_valid()) call write_fail('test_minimal: validity check failed')

    !! Check final segment
    call sp%set_segment(2.0_r8)
    call sp%get_position(t1, r)
    if (any(r /= r0)) call write_fail('test_minimal: final wrong position')
    if (.not.sp%is_flag_set(0)) call write_fail('test_minimal: final flag 0 wrong')
    if (sp%is_flag_set(1)) call write_fail('test_minimal: final flag 1 wrong')

    !! Check start segment
    call sp%set_segment(0.0_r8)
    call sp%get_position(t1, r)
    if (any(r /= r1)) call write_fail('test_minimal: start wrong position')
    if (sp%is_flag_set(0)) call write_fail('test_minimal: start flag 0 wrong')
    if (.not.sp%is_flag_set(1)) call write_fail('test_minimal: start flag 1 wrong')

    !! Move to final segment and check again
    call sp%next_segment
    call sp%get_position(t1, r)
    if (any(r /= r0)) call write_fail('test_minimal: next wrong position')
    if (.not.sp%is_flag_set(0)) call write_fail('test_minimal: next flag 0 wrong')
    if (sp%is_flag_set(1)) call write_fail('test_minimal: next flag 1 wrong')

  end subroutine test_minimal

  !! Check a hand-built scan path.  More checks of scan path segment positioning
  !! and adds check of new_path_segment and get_segment starts.

  subroutine test_path

    use dwell_xy_motion_type
    use linear_xy_motion_type
    use xy_motion_class

    type(scan_path) :: sp
    class(xy_motion), allocatable :: move

    real(r8) :: t = 1, r(2) = [1,2]
    real(r8), allocatable :: times(:)
    logical, allocatable :: discont(:)

    call sp%append_path_segment(new_start_segment(t, r, flags=0))
#ifdef NO_2008_LHS_POLY_REALLOC
    allocate(move, source=dwell_xy_motion(r, t0=t, dt=1.0_r8))
#else
    move = dwell_xy_motion(r, t0=t, dt=1.0_r8)
#endif
    t = move%final_time(); r = move%final_coord()
    call sp%append_path_segment(new_path_segment(move, flags=1))
#ifdef NO_2008_LHS_POLY_REALLOC
    allocate(move, source=linear_xy_motion(t, r, [1.0_r8, 0.0_r8], 1.0_r8))
#else
    move = linear_xy_motion(t, r, [1.0_r8, 0.0_r8], 1.0_r8)
#endif
    t = move%final_time(); r = move%final_coord()
    call sp%append_path_segment(new_path_segment(move, flags=1))
#ifdef NO_2008_LHS_POLY_REALLOC
    allocate(move, source=dwell_xy_motion(r, t0=t, dt=1.0_r8))
#else
    move = dwell_xy_motion(r, t0=t, dt=1.0_r8)
#endif
    t = move%final_time(); r = move%final_coord()
    call sp%append_path_segment(new_path_segment(move, flags=0))
    call sp%append_path_segment(new_final_segment(t, r, flags=0))

    if (.not.sp%is_valid()) call write_fail('test_path: validity check failed')

    call sp%set_segment(1.0_r8) ! the dwell, not start segment
    if (.not.sp%is_flag_set(0)) call write_fail('test_path: A: flag is wrong')
    call sp%get_position(1.0_r8,r)
    if (any(r /= [1,2])) call write_fail('test_path: A: start position is wrong')
    call sp%get_position(2.0_r8,r)
    if (any(r /= [1,2])) call write_fail('test_path: A: final position is wrong')

    call sp%next_segment  ! the linear move
    if (.not.sp%is_flag_set(0)) call write_fail('test_path: B: flag is wrong')
    call sp%get_position(2.0_r8,r)
    if (any(r /= [1,2])) call write_fail('test_path: B: start position is wrong')
    call sp%get_position(3.0_r8,r)
    if (any(r /= [2,2])) call write_fail('test_path: B: final position is wrong')

    call sp%next_segment  ! the dwell
    if (sp%is_flag_set(0)) call write_fail('test_path: C: flag is wrong')
    call sp%get_position(3.0_r8,r)
    if (any(r /= [2,2])) call write_fail('test_path: C: start position is wrong')
    call sp%get_position(4.0_r8,r)
    if (any(r /= [2,2])) call write_fail('test_path: C: final position is wrong')

    call sp%next_segment  ! the final segment
    if (sp%is_flag_set(0)) call write_fail('test_path: D: flag is wrong')
    call sp%get_position(4.0_r8,r)
    if (any(r /= [2,2])) call write_fail('test_path: D: start position is wrong')

    call sp%get_segment_starts(times, discont)
    if (size(times) /= 4) then
      call write_fail('test_path: times wrong size')
    else if (any(times /= [1,2,3,4])) then
      call write_fail('test_path: times is wrong')
    end if
    if (size(discont) /= 4) then
      call write_fail('test_path: discont wrong size')
    else if (any(discont .neqv. [.true.,.false.,.true.,.false.])) then
      call write_fail('test_path: discont is wrong')
    end if

  end subroutine test_path

  !! This checks the special partitioning feature using another hand-built scan path.

  subroutine test_partition

    use dwell_xy_motion_type
    use linear_xy_motion_type
    use xy_motion_class

    type(scan_path) :: sp
    class(xy_motion), allocatable :: move

    real(r8) :: t = 0, r(2) = [0,0]
    real(r8), allocatable :: time(:), coord(:,:)
    character(:), allocatable :: hash(:)

    call sp%append_path_segment(new_start_segment(t, r, flags=0))
#ifdef NO_2008_LHS_POLY_REALLOC
    allocate(move, source=dwell_xy_motion(r, t0=t, dt=1.0_r8))
#else
    move = dwell_xy_motion(r, t0=t, dt=1.0_r8)
#endif
    t = move%final_time(); r = move%final_coord()
    call sp%append_path_segment(new_path_segment(move, flags=0))
#ifdef NO_2008_LHS_POLY_REALLOC
    allocate(move, source=linear_xy_motion(t, r, [0.0_r8, 1.0_r8], 1.0_r8))
#else
    move = linear_xy_motion(t, r, [0.0_r8, 1.0_r8], 1.0_r8)
#endif
    t = move%final_time(); r = move%final_coord()
    call sp%append_path_segment(new_path_segment(move, flags=0))
#ifdef NO_2008_LHS_POLY_REALLOC
    allocate(move, source=linear_xy_motion(t, r, [2.0_r8, 0.0_r8], 2.0_r8))
#else
    move = linear_xy_motion(t, r, [2.0_r8, 0.0_r8], 2.0_r8)
#endif
    t = move%final_time(); r = move%final_coord()
    call sp%append_path_segment(new_path_segment(move, flags=0))
#ifdef NO_2008_LHS_POLY_REALLOC
    allocate(move, source=linear_xy_motion(t, r, [0.0_r8, -4.0_r8], 2.0_r8))
#else
    move = linear_xy_motion(t, r, [0.0_r8, -4.0_r8], 2.0_r8)
#endif
    t = move%final_time(); r = move%final_coord()
    call sp%append_path_segment(new_path_segment(move, flags=0))
    call sp%append_path_segment(new_final_segment(t, r, flags=0))

    if (.not.sp%is_valid()) call write_fail('test_partition: validity check failed')

    call sp%set_partition(2.0_r8)
    call sp%get_partition(time=time, coord=coord, hash=hash)
    if (size(time) /= 6) then
      call write_fail('test_partition: A: wrong time size')
    else if (any(time /= [0,1,2,3,4,5])) then
      call write_fail('test_partition: A: wrong time values')
    end if
    call sp%get_partition(time=time, coord=coord, hash=hash)
    if (any(shape(coord) /= [2,6])) then
      call write_fail('test_partition: A: wrong coord shape')
    else if (any(coord /= reshape([0,0, 0,0, 0,1, 2,1, 2,-1, 2,-3],[2,6]))) then
      call write_fail('test_partition: A: wrong coord values')
    end if
    if (size(hash) /= 6) then
      call write_fail('test_partition: A: wrong hash size')
    else if (any(hash /= ['e129f27', 'e129f27', 'f08571f', 'a3ddd86', '608f9b9', 'e4bc3f9'])) then
      call write_fail('test_partition: A: wrong hash values')
    end if

  end subroutine test_partition

  subroutine write_fail(errmsg)
    use,intrinsic :: iso_fortran_env, only: error_unit
    character(*), intent(in) :: errmsg
    status = 1
    write(error_unit,'(a)') errmsg
  end subroutine

end program test_scan_path_type
