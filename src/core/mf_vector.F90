module mf_vector_type

  use vector_class
  use amrex_multifab_module
  implicit none
  private

  type, extends(vector), public :: mf_vector
    type(amrex_multifab) :: mf
  contains
    procedure :: init
    procedure :: clone1, clone2
  end type

contains

  subroutine init(this, ba, dm, nc, ng, nodal)
    class(mf_vector), intent(out) :: this
    type(amrex_boxarray), intent(in) :: ba
    type(amrex_distromap), intent(in) :: dm
    integer, intent(in) :: nc, ng
    logical, intent(in), optional :: nodal(:)
    call amrex_multifab_build(this%mf, ba, dm, nc, ng, nodal)
  end subroutine init

  subroutine clone1(mold, copy)
    class(mf_vector), intent(in) :: mold
    class(vector), allocatable, intent(out) :: copy
    type(mf_vector), allocatable :: tmp
    allocate(tmp)
    call mf_clone(mold%mf, tmp%mf)
    call move_alloc(tmp, copy)
  end subroutine
  
  subroutine clone2(mold, copy, n)
    class(mf_vector), intent(in) :: mold
    class(vector), allocatable, intent(out) :: copy(:)
    integer, intent(in) :: n
    integer :: j
    type(mf_vector), allocatable :: tmp(:)
    allocate(tmp(n))
    do j = 1, n
      call mf_clone(mold%mf, tmp(j)%mf)
    end do
    call move_alloc(tmp, copy)
  end subroutine clone2

  subroutine copy(this, src)
    class(mf_vector), intent(inout) :: this
    class(vector), intent(in) :: src
    select type (src)
    type is (mf_vector)
      call
  end subroutine copy(this, src)
  
  subroutine mf_clone(mold, copy)
    type(amrex_multifab), intent(in) :: mold
    type(amrex_multifab), intent(inout) :: copy
    integer :: nc, ng
    logical :: nodal(3)
    nc = mold%ncomp()
    ng = mold%nghost()
    nodal = mold%nodal_type()
    call amrex_multifab_build(copy, mold%ba, mold%dm, nc, ng, nodal)
  end subroutine mf_clone

  subroutine scale(this, a)
    class(mf_vector), intent(inout) :: this
    real(r8), intent(in) :: a
    call this%mf%mult(a, 1, this%mf%nc, 0)
  end subroutine scale

  subroutine update1_(this, a, x)
    class(mf_vector), intent(inout) :: this
    class(vector), intent(in) :: x
    real(r8), intent(in) :: a
    select type (x)
    class is (mf_vector)
      call this%mf%saxpy(a, x%mf, 1, 1, this%mf%nc, 0)
    end select
  end subroutine

  subroutine update2_(this, a, x, b)
    class(mf_vector), intent(inout) :: this
    class(vector), intent(in) :: x
    real(r8), intent(in) :: a, b
    select type (x)
    class is (mf_vector)
      call this%mf%mult(b, 1, this%mf%nc, 0)
      call this%mf%saxpy(alpha, a, 1, 1, this%mf%nc, 0)
    end select
  end subroutine

  subroutine update3_(this, a, x, b, y)
    class(mf_vector), intent(inout) :: this
    class(vector), intent(in) :: x, y
    real(r8), intent(in) :: a, b
    select type (x)
    class is (mf_vector)
      select type (y)
      class is (mf_vector)
        call this%mf%saxpy(a, x%mf, 1, 1, this%mf%nc, 0)
        call this%mf%saxpy(b, y%mf, 1, 1, this%mf%nc, 0)
      end select
    end select
  end subroutine

  subroutine update4_(this, a, x, b, y, c)
    class(mf_vector), intent(inout) :: this
    class(vector), intent(in) :: x, y
    real(r8), intent(in) :: a, b, c
    select type (x)
    class is (mf_vector)
      select type (y)
      class is (mf_vector)
        call this%mf%mult(c, 1, this%mf%nc, 0)
        call this%mf%saxpy(a, x%mf, 1, 1, this%mf%nc, 0)
        call this%mf%saxpy(b, y%mf, 1, 1, this%mf%nc, 0)
      end select
    end select
  end subroutine

  function dot(a, b) result(dp)
    class(mf_vector), intent(in) :: a
    class(vector), intent(in) :: b
    real(r8) :: dp
    select type (b)
    class is (mf_vector)
      block
      type(amrex_mfiter) :: mfi
      type(amrex_box) :: bx
      real(r8), pointer, contiguous :: aptr(:,:,:,:), bptr(:,:,:,:)
      integer :: alo(4), ahi(4), blo(4), bhi(4)
      call amrex_mfiter_build(mfi, a%mf)
      dp = 0.0_r8
      do while (mfi%next())
        bx = mfi%tilebox()
        aptr => a%mf%dataptr(mfi)
        bptr => b%mf%dataptr(mfi)
        alo = lbound(aptr)
        ahi = ubound(aptr)
        blo = lbound(bptr)
        bhi = ubound(bptr)
        dp = dp + dot_kernel(bx%lo, bx%hi, aptr, alo, ahi, bptr, blo, bhi)
      end do
      end block
    end select
  end function

  pure function dot_kernel(lo, hi, a, alo, ahi, b, blo, bhi) result(dp)
    integer, intent(in) :: lo(3), hi(3), alo(3), ahi(3), blo(3), bhi(3)
    real(r8), intent(in) :: a(alo(1):ahi(1),alo(2):ahi(2),alo(3):ahi(3))
    real(r8), intent(in) :: b(blo(1):bhi(1),blo(2):bhi(2),blo(3):bhi(3))
    real(r8) :: dp
    integer :: i, j, k
    dp = 0.0_r8
    do k = lo(3), hi(3)
      do j = lo(2), hi(2)
        do i = lo(1), hi(1)
          dp = dp + a(i,j,k)*b(i,j,k)
        end do
      end do
    end do
  end function
        
  function norm2(this)
    class(mf_vector), intent(in) :: this
    real(r8) :: norm2
    norm2 = this%mf%norm2()
  end function
end module mf_vector_type
