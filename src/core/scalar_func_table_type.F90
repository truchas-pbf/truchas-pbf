!!
!! SCALAR_FUNC_TABLE_TYPE
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! January 2018
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! PROGRAMMING INTERFACE
!!
!! This module defines the SCALAR_FUNC_TABLE derived type that is an associative
!! array with (row,col) pair character keys and SCALAR_FUNC class values. It has
!! the following type bound procedures.
!!
!!  INSERT(ROW_KEY, COL_KEY, VALUE) adds the specified element and associated
!!    value to the table. If the element already exists, its value is replaced
!!    with the specifed one. VALUE is an allocatable SCALAR_FUNC class variable.
!!    Its allocation is moved into the table and is returned unallocated.
!!
!!  REMOVE(ROW_KEY, COL_KEY) removes the specified element from the table and
!!    deallocates the associated value. If the element does not exist, the table
!!    is unchanged.
!!
!!  MAPPED(ROW_KEY, COL_KEY) returns true if the specified table element exists;
!!    otherwise it returns false.
!!
!!  LOOKUP(ROW_KEY, COL_KEY, VALUE) returns the value for the specified element.
!!    VALUE is an allocatable SCALAR_FUNC class variable.  It is allocated and
!!    assigned a copy of the mapped value if it exists; otherwise it is
!!    is returned unallocated.
!!
!!  CLEAR() removes all elements from the table, leaving it with a size of 0.
!!
!! NB: The polymorphic SCALAR_FUNC class values in the interface are all
!! allocatable.  The functions inserted into the table are handed off to the
!! the table using the MOVE_ALLOC intrinsic subroutine; no copies are made.
!! On the other hand, the functions returned by LOOKUP are copies of the
!! stored value as created by sourced-allocation.  These are shallow copies.
!! For pointer components this means that a copy of the pointer is made
!! but not a copy of its target; the original pointer and its copy will
!! have the same target.  Currently, none of the extensions of SCALAR_FUNC
!! have pointer components (except for DL_SCALAR_FUNC which holds a C_PTR
!! component that is the handle to the library -- here a shallow copy is
!! fine), so these are deep copies.  But this may change in the future and
!! these copies may not be what is needed.
!!

module scalar_func_table_type

  use scalar_func_class
  use scalar_func_map_type
  implicit none
  private

  type, public :: scalar_func_table
    private
    type(list_row), pointer :: first => null()
  contains
    procedure :: insert
    procedure :: remove
    procedure :: lookup
    procedure :: mapped
    procedure :: clear
    final :: scalar_func_table_delete
  end type scalar_func_table

  type :: list_row
    character(:), allocatable :: key
    type(scalar_func_map) :: map
    type(list_row), pointer :: next => null(), prev => null()
  contains
    final :: list_row_delete
  end type list_row

contains

  !! Final subroutine for SCALAR_FUNC_TABLE objects.
  subroutine scalar_func_table_delete(this)
    type(scalar_func_table), intent(inout) :: this
    if (associated(this%first)) deallocate(this%first)
  end subroutine scalar_func_table_delete

  !! Final subroutine for LIST_ROW objects. This recursively follows the NEXT
  !! pointer. When deallocating a linked-list structure only the root needs to
  !! be explicitly deallocated. When the desire is to deallocate a single
  !! LIST_ROW object, first nullify the NEXT pointer to prevent the recursive
  !! finalization from deallocating more than it should.
  recursive subroutine list_row_delete(this)
    type(list_row), intent(inout) :: this
    if (associated(this%next)) deallocate(this%next)
  end subroutine list_row_delete

  !!!! SCALAR_FUNC_TABLE TYPE-BOUND PROCEDURES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !! Return the mapped value for the (ROW_KEY, COL_KEY) element of the table.
  !! VALUE is an allocatable SCALAR_FUNC class variable. It is allocated and
  !! assigned a (shallow) copy of the mapped value if it exists; otherwise it is
  !! returned unallocated.
  subroutine lookup(this, row_key, col_key, value)
    class(scalar_func_table), intent(in) :: this
    character(*), intent(in) :: row_key, col_key
    class(scalar_func), allocatable, intent(out) :: value
    type(list_row), pointer :: row
    row => find_list_row(this, row_key)
    if (associated(row)) call row%map%lookup(col_key, value)
  end subroutine lookup

  !! Add the specified (ROW_KEY, COL_KEY) element and associated value to the
  !! table.  If the element already exists, its value is replaced with the
  !! specified one. VALUE is an allocatable SCALAR_FUNC class variable. Its
  !! allocation is moved into the table and is returned unallocated.
  subroutine insert(this, row_key, col_key, value)
    class(scalar_func_table), intent(inout) :: this
    character(*), intent(in) :: row_key, col_key
    class(scalar_func), allocatable, intent(inout) :: value
    type(list_row), pointer :: row
    row => find_list_row(this, row_key)
    if (.not.associated(row)) then
      allocate(row)
      row%key = row_key
      call append_list_row(this, row)
    end if
    call row%map%insert(col_key, value)
  end subroutine insert

  !! Remove the (ROW_KEY, COL_KEY) element from the table.
  subroutine remove(this, row_key, col_key)
    class(scalar_func_table), intent(inout) :: this
    character(*), intent(in) :: row_key, col_key
    type(list_row), pointer :: row
    row => find_list_row(this, row_key)
    if (associated(row)) then
      call row%map%remove(col_key)
      if (row%map%is_empty()) then
        call remove_list_row(this, row)
        deallocate(row)
      end if
    end if
  end subroutine remove

  !! Remove all elements from the table.
  subroutine clear(this)
    class(scalar_func_table), intent(inout) :: this
    if (associated(this%first)) deallocate(this%first)
  end subroutine clear

  logical function mapped(this, row_key, col_key)
    class(scalar_func_table), intent(in) :: this
    character(*), intent(in) :: row_key, col_key
    type(list_row), pointer :: row
    row => find_list_row(this, row_key)
    mapped = associated(row)
    if (mapped) mapped = row%map%mapped(col_key)
  end function mapped

  !!!! AUXILLARY ROUTINES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !! Returns a pointer to the LIST_ROW having the specified key,
  !! or a null pointer of none was found.
  function find_list_row(this, key) result(item)
    class(scalar_func_table), intent(in) :: this
    character(*), intent(in) :: key
    type(list_row), pointer :: item
    item => this%first
    do while (associated(item))
      if (item%key == key) exit
      item => item%next
    end do
  end function find_list_row

  !! Blindly links the given LIST_ROW pointer to the end of the list;
  !! it does not check that the key is unique (someone else must).
  subroutine append_list_row(this, row)
    class(scalar_func_table), intent(inout) :: this
    type(list_row), pointer, intent(in) :: row
    type(list_row), pointer :: tail
    if (associated(this%first)) then
      tail => this%first%prev
      tail%next => row
      row%prev => tail
      this%first%prev => row
    else
      row%prev => row
      this%first => row
    end if
  end subroutine append_list_row

  !! Unlink the given LIST_ROW pointer from the list.
  subroutine remove_list_row(this, row)
    class(scalar_func_table), intent(inout) :: this
    type(list_row), pointer, intent(in) :: row
    if (associated(row%prev, row)) then ! single row list
      this%first => null()
    else if (associated(this%first, row)) then ! first row of multiple
      this%first => row%next
      row%next%prev => row%prev
    else if (.not.associated(row%next)) then ! last row of multiple
      row%prev%next => row%next
      this%first%prev => row%prev
    else  ! interior row of multiple
      row%prev%next => row%next
      row%next%prev => row%prev
    end if
    row%next => null() ! stop recursive finalization when row is deallocated
  end subroutine remove_list_row

end module scalar_func_table_type
