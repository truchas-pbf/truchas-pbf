!!
!! PHASE_PROPERTY_TABLE_FACTORY
!!
!! This module provides a subroutine for initializing a SCALAR_FUNC_TABLE object
!! with phase property functions specified by a parameter list.
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! January 2018
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!!  CALL INIT_PHASE_PROPERTY_TABLE(PARAMS, PPT) initializes the SCALAR_FUNC_TABLE
!!    object PPT with phase property functions specified in the parameter list
!!    PARAMS.  PPT is INTENT(OUT).  The parameter list has the following format:
!!
!!    { phase-name : {
!!            prop-name : real-const OR function-plist,
!!            ...},
!!      ...
!!    }
!!
!!    Here function-plist is any parameter list accepted by the ALLOC_SCALR_FUNC
!!    subroutine from the SCALAR_FUNC_FACTORIES module.
!!
!!    The phase names are the keys for the rows, and the property names the keys
!!    for the columns of the resulting table.  Use the LOOKUP table procedure
!!    retrieving a copy of the the function.  See scalar_func_table_type.F90 for
!!    details.
!!

module phase_property_table_factory

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use scalar_func_table_type
  use scalar_func_factories
  use parameter_list_type
  use simlog_type
  implicit none
  private

  public :: init_phase_property_table

contains

  subroutine init_phase_property_table(params, log, ppt)

    use parameter_entry_class

    type(parameter_list), intent(inout) :: params
    type(simlog), intent(in) :: log
    type(scalar_func_table), intent(out) :: ppt

    character(:), allocatable :: phase, prop, context, errmsg
    type(parameter_list), pointer :: plist, fparam
    type(parameter_list_iterator) :: phase_iter, prop_iter
    class(parameter_entry), pointer :: p
    class(scalar_func), allocatable :: f
    real(r8) :: const
    logical :: errc
    integer :: stat

    context = 'processing ' // params%name() // ': '
    phase_iter = parameter_list_iterator(params, sublists_only=.true.)
    do while (.not.phase_iter%at_end())
      phase = phase_iter%name()
      plist => phase_iter%sublist()
      context = 'processing ' // plist%name() // ': '
      prop_iter = parameter_list_iterator(plist)
      do while (.not.prop_iter%at_end())
        prop = prop_iter%name()
        call log%info('creating "'//prop//'" property for phase "'//phase//'"')
        p => prop_iter%entry()
        select type (p)
        type is (any_scalar)
          call p%get_value(const, errc)
          if (errc) call log%fatal(context//'real value expected for scalar "'//prop//'"')
          call alloc_const_scalar_func(f, const)
        type is (parameter_list)
          fparam => p
          call alloc_scalar_func(f, fparam, stat, errmsg)
          if (stat /= 0) call log%fatal(errmsg)
        class default
          call log%fatal(context//'invalid value for "'//prop//'" property')
        end select
        call ppt%insert(phase, prop, f)
        call prop_iter%next
      end do
      call phase_iter%next
    end do

  end subroutine init_phase_property_table

end module phase_property_table_factory
