!!
!! TRUCHAS_SCALAR_FUNC_TABLE
!!
!! This module manages the global table of SCALAR_FUNC objects.
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! June 2016, with additions February 2018
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module truchas_scalar_func_table

  use scalar_func_class
  use scalar_func_map_type
  implicit none
  private

  public :: insert_func, lookup_func, known_func
  public :: insert_func_from_plist

  type(scalar_func_map) :: table

contains

  subroutine insert_func(name, f)
    character(*), intent(in) :: name
    class(scalar_func), allocatable, intent(inout) :: f
    call table%insert(name, f)
  end subroutine insert_func

  subroutine lookup_func(name, f)
    character(*), intent(in) :: name
    class(scalar_func), allocatable, intent(out) :: f
    call table%lookup(name, f)
  end subroutine lookup_func

  logical function known_func(name)
    character(*), intent(in) :: name
    known_func = table%mapped(name)
  end function known_func


  subroutine insert_func_from_plist(plist, stat, errmsg)

    use,intrinsic :: iso_fortran_env, only: r8 => real64
    use parameter_list_type
    use parameter_entry_class
    use scalar_func_factories

    type(parameter_list), intent(inout) :: plist

    character(:), allocatable :: context, name, errmsg
    type(parameter_list_iterator) :: piter
    type(parameter_list), pointer :: fparams
    class(parameter_entry), pointer :: p
    class(scalar_func), allocatable :: f
    real(r8) :: const
    logical :: errc
    integer :: stat

    context = 'processing ' // plist%name() // ': '

    piter = parameter_list_iterator(plist)
    do while (.not.piter%at_end())
      name = piter%name()
      if (table%mapped(name)) then
        stat = 1
        errmsg = context//'function "'//name//'" already defined'
        return
      end if
      p => piter%entry()
      select type (p)
      type is (any_scalar)
        call p%get_value(const, errc)
        if (errc) then
          stat = 1
          errmsg = context//'scalar value for function "'//name//'" must be real'
          return
        end if
        call alloc_const_scalar_func(f, const)
      type is (parameter_list)
        fparams => p
        call alloc_scalar_func(f, fparams, stat, errmsg)
        if (stat /= 0) return
      class default
        stat = 1
        errmsg = context//'invalid value for function "'//name//'"'
        return
      end select
      call table%insert(name, f)
      call piter%next
    end do

  end subroutine insert_func_from_plist

end module truchas_scalar_func_table
