!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module hypre_c_binding

  use,intrinsic :: iso_c_binding, only: c_ptr, c_int, c_double, c_char
  implicit none
  private

  integer(c_int), parameter, public :: HYPRE_ERROR_GENERIC = 1
  integer(c_int), parameter, public :: HYPRE_ERROR_MEMORY  = 2
  integer(c_int), parameter, public :: HYPRE_ERROR_ARG     = 4
  integer(c_int), parameter, public :: HYPRE_ERROR_CONV    = 256

  public :: HYPRE_ClearAllErrors
  public :: HYPRE_Initialize
  public :: HYPRE_Finalize

  !! Functions from the Struct interface
  public :: HYPRE_StructGridCreate
  public :: HYPRE_StructGridDestroy
  public :: HYPRE_StructGridSetExtents
  public :: HYPRE_StructGridAssemble
  public :: HYPRE_StructGridSetNumGhost

  public :: HYPRE_StructStencilCreate
  public :: HYPRE_StructStencilDestroy
  public :: HYPRE_StructStencilSetElement

  public :: HYPRE_StructMatrixCreate
  public :: HYPRE_StructMatrixDestroy
  public :: HYPRE_StructMatrixInitialize
  public :: HYPRE_StructMatrixSetValues
  public :: HYPRE_StructMatrixAddToValues
  public :: HYPRE_StructMatrixSetBoxValues
  public :: HYPRE_StructMatrixAddToBoxValues
  public :: HYPRE_StructMatrixAssemble
  public :: HYPRE_StructMatrixGetValues
  public :: HYPRE_StructMatrixGetBoxValues
  public :: HYPRE_StructMatrixSetSymmetric
  public :: HYPRE_StructMatrixSetNumGhost
  public :: HYPRE_StructMatrixPrint
  public :: HYPRE_StructMatrixMatvec

  public :: HYPRE_StructVectorCreate
  public :: HYPRE_StructVectorDestroy
  public :: HYPRE_StructVectorInitialize
  public :: HYPRE_StructVectorSetValues
  public :: HYPRE_StructVectorAddToValues
  public :: HYPRE_StructVectorSetBoxValues
  public :: HYPRE_StructVectorAddToBoxValues
  public :: HYPRE_StructVectorAssemble
  public :: HYPRE_StructVectorGetValues
  public :: HYPRE_StructVectorGetBoxValues
  public :: HYPRE_StructVectorPrint

  !! Functions from the SStruct interface
  public :: HYPRE_SStructGridCreate
  public :: HYPRE_SStructGridDestroy
  public :: HYPRE_SStructGridSetExtents
  public :: HYPRE_SStructGridSetVariables
  public :: HYPRE_SStructGridSetFEMOrdering
  public :: HYPRE_SStructGridAssemble

  public :: HYPRE_SStructStencilCreate
  public :: HYPRE_SStructStencilDestroy
  public :: HYPRE_SStructStencilSetEntry

  public :: HYPRE_SStructGraphCreate
  public :: HYPRE_SStructGraphDestroy
  public :: HYPRE_SStructGraphSetObjectType
  public :: HYPRE_SStructGraphSetStencil
  public :: HYPRE_SStructGraphSetFEM
  public :: HYPRE_SStructGraphAssemble

  public :: HYPRE_SStructMatrixCreate
  public :: HYPRE_SStructMatrixDestroy
  public :: HYPRE_SStructMatrixSetObjectType
  public :: HYPRE_SStructMatrixGetObject
  public :: HYPRE_SStructMatrixInitialize
  public :: HYPRE_SStructMatrixSetBoxValues
  public :: HYPRE_SStructMatrixGetBoxValues
  public :: HYPRE_SStructMatrixAddFEMValues
  public :: HYPRE_SStructMatrixAssemble
  public :: HYPRE_SStructMatrixPrint

  public :: HYPRE_SStructVectorCreate
  public :: HYPRE_SStructVectorDestroy
  public :: HYPRE_SStructVectorInitialize
  public :: HYPRE_SStructVectorGetValues
  public :: HYPRE_SStructVectorAddFEMValues
  public :: HYPRE_SStructVectorGetFEMValues
  public :: HYPRE_SStructVectorSetBoxValues
  public :: HYPRE_SStructVectorGetBoxValues
  public :: HYPRE_SStructVectorAssemble
  public :: HYPRE_SStructVectorGather
  public :: HYPRE_SStructVectorSetObjectType
  public :: HYPRE_SStructVectorGetObject
  public :: HYPRE_SStructVectorPrint

  !! Functions from the BoomerAMG interface.
  public :: HYPRE_BoomerAMGCreate
  public :: HYPRE_BoomerAMGDestroy
  public :: HYPRE_BoomerAMGSetup
  public :: HYPRE_BoomerAMGSolve
  public :: HYPRE_BoomerAMGGetNumIterations
  public :: HYPRE_BoomerAMGGetFinalRelativeResidualNorm
  public :: HYPRE_BoomerAMGSetStrongThreshold
  public :: HYPRE_BoomerAMGSetMaxIter
  public :: HYPRE_BoomerAMGSetCoarsenType
  public :: HYPRE_BoomerAMGSetInterpType
  public :: HYPRE_BoomerAMGSetTol
  public :: HYPRE_BoomerAMGSetNumSweeps
  public :: HYPRE_BoomerAMGSetRelaxType
  public :: HYPRE_BoomerAMGSetPrintLevel
  public :: HYPRE_BoomerAMGSetMaxLevels
  public :: HYPRE_BoomerAMGSetCycleNumSweeps
  public :: HYPRE_BoomerAMGSetCycleRelaxType
  public :: HYPRE_BoomerAMGSetCycleType
  public :: HYPRE_BoomerAMGSetDebugFlag
  public :: HYPRE_BoomerAMGSetLogging

  !! Functions from the Struct Hybrid Solver
  public :: HYPRE_StructHybridCreate
  public :: HYPRE_StructHybridDestroy
  public :: HYPRE_StructHybridSetup
  public :: HYPRE_StructHybridSolve
  public :: HYPRE_StructHybridSetTol
  public :: HYPRE_StructHybridSetConvergenceTol
  public :: HYPRE_StructHybridSetDSCGMaxIter
  public :: HYPRE_StructHybridSetPCGMaxIter
  public :: HYPRE_StructHybridSetTwoNorm
  public :: HYPRE_StructHybridSetRelChange
  public :: HYPRE_StructHybridSetSolverType
  public :: HYPRE_StructHybridSetKDim
  public :: HYPRE_StructHybridSetLogging
  public :: HYPRE_StructHybridSetPrintLevel
  public :: HYPRE_StructHybridGetNumIterations
  public :: HYPRE_StructHybridGetDSCGNumIterations
  public :: HYPRE_StructHybridGetPCGNumIterations
  public :: HYPRE_StructHybridGetFinalRelativeResidualNorm

  !! Struct PFMG solver procedures
  public :: HYPRE_StructPFMGCreate
  public :: HYPRE_StructPFMGDestroy
  public :: HYPRE_StructPFMGSetup
  public :: HYPRE_StructPFMGSolve
  public :: HYPRE_StructPFMGSetTol
  public :: HYPRE_StructPFMGSetMaxIter

 !! HYPRE UTILITIES
  interface
    function HYPRE_ClearAllErrors() &
        result(ierr) bind(c,name="HYPRE_ClearAllErrors")
      import c_int
      integer(c_int) :: ierr
    end function
    function HYPRE_Initialize() &
        result(ierr) bind(c,name="HYPRE_Initialize")
      import c_int
      integer(c_int) :: ierr
    end function
    function HYPRE_Finalize() &
        result(ierr) bind(c,name="HYPRE_Finalize")
      import c_int
      integer(c_int) :: ierr
    end function
  end interface

  !! Struct GRID INTERFACES
  interface
    function HYPRE_StructGridCreate(comm, ndim, grid) &
        result(ierr) bind(c,name="fHYPRE_StructGridCreate")
      import c_int, c_ptr
      integer, value :: comm
      integer(c_int), value :: ndim
      type(c_ptr) :: grid
      integer(c_int) :: ierr
    end function
    function HYPRE_StructGridDestroy(grid) &
        result(ierr) bind(c,name="HYPRE_StructGridDestroy")
      import c_int, c_ptr
      type(c_ptr), value :: grid
      integer(c_int) :: ierr
    end function
    function HYPRE_StructGridSetExtents(grid, ilower, iupper) &
        result(ierr) bind(c,name="HYPRE_StructGridSetExtents")
      import c_int, c_ptr
      type(c_ptr), value :: grid
      integer(c_int), intent(in) :: ilower(*), iupper(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructGridAssemble(grid) &
        result(ierr) bind(c,name="HYPRE_StructGridAssemble")
      import c_int, c_ptr
      type(c_ptr), value :: grid
      integer(c_int) :: ierr
    end function
    function HYPRE_StructGridSetNumGhost(grid, num_ghost) &
        result(ierr) bind(c,name="HYPRE_StructGridSetNumGhost")
      import c_int, c_ptr
      type(c_ptr), value :: grid
      integer(c_int), intent(in) :: num_ghost(*)
      integer(c_int) :: ierr
    end function
  end interface

  !! Struct STENCIL INTERFACES
  interface
    function HYPRE_StructStencilCreate(ndim, size, stencil) &
        result(ierr) bind(c,name="HYPRE_StructStencilCreate")
      import c_int, c_ptr
      integer(c_int), value :: ndim, size
      type(c_ptr) :: stencil
      integer(c_int) :: ierr
    end function
    function HYPRE_StructStencilDestroy(stencil) &
        result(ierr) bind(c,name="HYPRE_StructStencilDestroy")
      import c_int, c_ptr
      type(c_ptr), value :: stencil
      integer(c_int) :: ierr
    end function
    function HYPRE_StructStencilSetElement(stencil, entry, offset) &
        result(ierr) bind(c,name="HYPRE_StructStencilSetElement")
      import c_int, c_ptr
      type(c_ptr), value :: stencil
      integer(c_int), value :: entry
      integer(c_int), intent(in) :: offset(*)
      integer(c_int) :: ierr
    end function
  end interface

  !! Struct MATRIX INTERFACES
  interface
    function HYPRE_StructMatrixCreate(comm, grid, stencil, matrix) &
        result(ierr) bind(c,name="fHYPRE_StructMatrixCreate")
      import c_int, c_ptr
      integer, value :: comm
      type(c_ptr), value :: grid, stencil
      type(c_ptr) :: matrix
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixDestroy(matrix) &
        result(ierr) bind(c,name="HYPRE_StructMatrixDestroy")
      import c_int, c_ptr
      type(c_ptr), value :: matrix
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixInitialize(matrix) &
        result(ierr) bind(c,name="HYPRE_StructMatrixInitialize")
      import c_int, c_ptr
      type(c_ptr), value :: matrix
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixSetValues(matrix, index, nentries, entries, values) &
        result(ierr) bind(c,name="HYPRE_StructMatrixSetValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: matrix
      integer(c_int), value :: nentries
      integer(c_int), intent(in) :: index(*), entries(*)
      real(c_double), intent(in) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixAddToValues(matrix, index, nentries, entries, values) &
        result(ierr) bind(c,name="HYPRE_StructMatrixAddToValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: matrix
      integer(c_int), value :: nentries
      integer(c_int), intent(in) :: index(*), entries(*)
      real(c_double), intent(in) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixSetBoxValues(matrix, ilower, iupper, nentries, &
        entries, values) result(ierr) bind(c,name="HYPRE_StructMatrixSetBoxValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: matrix
      integer(c_int), value :: nentries
      integer(c_int), intent(in) :: ilower(*), iupper(*), entries(*)
      real(c_double), intent(in) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixAddToBoxValues(matrix, ilower, iupper, nentries, &
        entries, values) result(ierr) bind(c,name="HYPRE_StructMatrixAddToBoxValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: matrix
      integer(c_int), value :: nentries
      integer(c_int), intent(in) :: ilower(*), iupper(*), entries(*)
      real(c_double), intent(in) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixAssemble(matrix) &
        result(ierr) bind(c,name="HYPRE_StructMatrixAssemble")
      import c_int, c_ptr
      type(c_ptr), value :: matrix
      integer :: ierr
    end function
    function HYPRE_StructMatrixGetValues(matrix, index, nentries, entries, values) &
        result(ierr) bind(c,name="HYPRE_StructMatrixGetValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: matrix
      integer(c_int), value :: nentries
      integer(c_int), intent(in) :: index(*), entries(*)
      real(c_double), intent(out) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixGetBoxValues(matrix, ilower, iupper, nentries, &
        entries, values) result(ierr) bind(c,name="HYPRE_StructMatrixGetBoxValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: matrix
      integer(c_int), value :: nentries
      integer(c_int), intent(in) :: ilower(*), iupper(*), entries(*)
      real(c_double), intent(out) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixSetSymmetric(matrix, symmetric) &
        result(ierr) bind(c,name="HYPRE_StructMatrixSetSymmetric")
      import c_int, c_ptr
      type(c_ptr), value :: matrix
      integer(c_int), value :: symmetric
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixSetNumGhost(matrix, num_ghost) &
        result(ierr) bind(c,name="HYPRE_StructMatrixSetNumGhost")
      import c_int, c_ptr
      type(c_ptr), value :: matrix
      integer(c_int), intent(in) :: num_ghost(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixPrint(filename, matrix, all) &
        result(ierr) bind(c,name="HYPRE_StructMatrixPrint")
      import c_int, c_ptr, c_char
      character(kind=c_char), intent(in) :: filename(*)
      type(c_ptr), value :: matrix
      integer(c_int), value :: all
      integer(c_int) :: ierr
    end function
    function HYPRE_StructMatrixMatvec(alpha, A, x, beta, y) &
        result(ierr) bind(c,name="HYPRE_StructMatrixMatvec")
      import c_int, c_ptr, c_double
      real(c_double), value :: alpha, beta
      type(c_ptr), value :: A, x, y
      integer(c_int) :: ierr
    end function
  end interface

  !! Struct VECTOR INTERFACES
  interface
    function HYPRE_StructVectorCreate(comm, grid, vector) &
        result(ierr) bind(c,name="fHYPRE_StructVectorCreate")
      import c_int, c_ptr
      integer, value :: comm
      type(c_ptr), value :: grid
      type(c_ptr) :: vector
      integer(c_int) :: ierr
    end function
    function HYPRE_StructVectorDestroy(vector) &
        result(ierr) bind(c,name="HYPRE_StructVectorDestroy")
      import c_int, c_ptr
      type(c_ptr), value :: vector
      integer(c_int) :: ierr
    end function
    function HYPRE_StructVectorInitialize(vector) &
        result(ierr) bind(c,name="HYPRE_StructVectorInitialize")
      import c_int, c_ptr
      type(c_ptr), value :: vector
      integer(c_int) :: ierr
    end function
    function HYPRE_StructVectorSetValues(vector, index, value) &
        result(ierr) bind(c,name="HYPRE_StructVectorSetValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), intent(in) :: index(*)
      real(c_double), value :: value
      integer(c_int) :: ierr
    end function
    function HYPRE_StructVectorAddToValues(vector, index, value) &
        result(ierr) bind(c,name="HYPRE_StructVectorAddToValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), intent(in) :: index(*)
      real(c_double), value :: value
      integer(c_int) :: ierr
    end function
    function HYPRE_StructVectorSetBoxValues(vector, ilower, iupper, values) &
        result(ierr) bind(c,name="HYPRE_StructVectorSetBoxValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), intent(in) :: ilower(*), iupper(*)
      real(c_double), intent(in) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructVectorAddToBoxValues(vector, ilower, iupper, values) &
        result(ierr) bind(c,name="HYPRE_StructVectorAddToBoxValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), intent(in) :: ilower(*), iupper(*)
      real(c_double), intent(in) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructVectorAssemble(vector) &
        result(ierr) bind(c,name="HYPRE_StructVectorAssemble")
      import c_int, c_ptr
      type(c_ptr), value :: vector
      integer(c_int) :: ierr
    end function
    function HYPRE_StructVectorGetValues(vector, index, value) &
        result(ierr) bind(c,name="HYPRE_StructVectorGetValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), intent(in) :: index(*)
      real(c_double) :: value
      integer(c_int) :: ierr
    end function
    function HYPRE_StructVectorGetBoxValues(vector, ilower, iupper, values) &
        result(ierr) bind(c,name="HYPRE_StructVectorGetBoxValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), intent(in) :: ilower(*), iupper(*)
      real(c_double), intent(out) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_StructVectorPrint(filename, vector, all) &
        result(ierr) bind(c,name="HYPRE_StructVectorPrint")
      import c_int, c_ptr, c_char
      character(kind=c_char), intent(in) :: filename(*)
      type(c_ptr), value :: vector
      integer(c_int), value :: all
      integer(c_int) :: ierr
    end function
  end interface

  !! SStruct GRID INTERFACES
  interface
    function HYPRE_SStructGridCreate(comm, ndim, nparts, grid) &
        result(ierr) bind(c,name="fHYPRE_SStructGridCreate")
      import c_int, c_ptr
      integer, value :: comm
      integer(c_int), value :: ndim, nparts
      type(c_ptr) :: grid
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructGridDestroy(grid) &
        result(ierr) bind(c,name="HYPRE_SStructGridDestroy")
      import c_int, c_ptr
      type(c_ptr), value :: grid
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructGridSetExtents(grid, part, ilower, iupper) &
        result(ierr) bind(c,name="HYPRE_SStructGridSetExtents")
      import c_int, c_ptr
      type(c_ptr), value :: grid
      integer(c_int), value :: part
      integer(c_int), intent(in) :: ilower(*), iupper(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructGridSetVariables(grid, part, nvars, vartypes) &
        result(ierr) bind(c,name="HYPRE_SStructGridSetVariables")
      import c_int, c_ptr
      type(c_ptr), value :: grid
      integer(c_int), value :: part, nvars
      integer(c_int), intent(in) :: vartypes(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructGridSetFEMOrdering(grid, part, ordering) &
        result(ierr) bind(c,name="HYPRE_SStructGridSetFEMOrdering")
      import c_int, c_ptr
      type(c_ptr), value :: grid
      integer(c_int), value :: part
      integer(c_int), intent(in) :: ordering(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructGridAssemble(grid) &
        result(ierr) bind(c,name="HYPRE_SStructGridAssemble")
      import c_int, c_ptr
      type(c_ptr), value :: grid
      integer(c_int) :: ierr
    end function
  end interface

  !! SStruct STENCIL INTERFACES
  interface
    function HYPRE_SStructStencilCreate(ndim, size, stencil) &
        result(ierr) bind(c,name="HYPRE_SStructStencilCreate")
      import c_int, c_ptr
      integer(c_int), value :: ndim, size
      type(c_ptr) :: stencil
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructStencilDestroy(stencil) &
        result(ierr) bind(c,name="HYPRE_SStructStencilDestroy")
      import c_int, c_ptr
      type(c_ptr), value :: stencil
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructStencilSetEntry(stencil, entry, offset, var) &
        result(ierr) bind(c,name="HYPRE_SStructStencilSetEntry")
      import c_int, c_ptr
      type(c_ptr), value :: stencil
      integer(c_int), value :: entry, var
      integer(c_int), intent(in) :: offset(*)
      integer(c_int) :: ierr
    end function
  end interface

  !! SStruct GRAPH INTERFACES
  interface
    function HYPRE_SStructGraphCreate(comm, grid, graph) &
        result(ierr) bind(c,name="fHYPRE_SStructGraphCreate")
      import c_int, c_ptr
      integer, value :: comm
      type(c_ptr), value :: grid
      type(c_ptr) :: graph
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructGraphDestroy(graph) &
        result(ierr) bind(c,name="HYPRE_SStructGraphDestroy")
      import c_int, c_ptr
      type(c_ptr), value :: graph
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructGraphSetObjectType(graph, type) &
        result(ierr) bind(c,name="HYPRE_SStructGraphSetObjectType")
      import c_int, c_ptr
      type(c_ptr), value :: graph
      integer(c_int), value :: type
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructGraphSetStencil(graph, part, var, stencil) &
        result(ierr) bind(c,name="HYPRE_SStructGraphSetStencil")
      import c_int, c_ptr
      type(c_ptr), value :: graph, stencil
      integer(c_int), value :: part, var
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructGraphSetFEM(graph, part) &
        result(ierr) bind(c,name="HYPRE_SStructGraphSetFEM")
      import c_int, c_ptr
      type(c_ptr), value :: graph
      integer(c_int), value :: part
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructGraphAssemble(graph) &
        result(ierr) bind(c,name="HYPRE_SStructGraphAssemble")
      import c_int, c_ptr
      type(c_ptr), value :: graph
      integer(c_int) :: ierr
    end function
  end interface

  !! SStruct MATRIX INTERFACES
  interface
    function HYPRE_SStructMatrixCreate(comm, graph, matrix) &
        result(ierr) bind(c,name="fHYPRE_SStructMatrixCreate")
      import c_int, c_ptr
      integer, value :: comm
      type(c_ptr), value :: graph
      type(c_ptr) :: matrix
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructMatrixDestroy(matrix) &
        result(ierr) bind(c,name="HYPRE_SStructMatrixDestroy")
      import c_int, c_ptr
      type(c_ptr), value :: matrix
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructMatrixSetObjectType(matrix, type) &
        result(ierr) bind(c,name="HYPRE_SStructMatrixSetObjectType")
      import c_int, c_ptr
      type(c_ptr), value :: matrix
      integer(c_int), value :: type
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructMatrixGetObject(matrix, object) &
        result(ierr) bind(c,name="HYPRE_SStructMatrixGetObject")
      import c_int, c_ptr
      type(c_ptr), value :: matrix
      type(c_ptr) :: object
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructMatrixInitialize(matrix) &
        result(ierr) bind(c,name="HYPRE_SStructMatrixInitialize")
      import c_int, c_ptr
      type(c_ptr), value :: matrix
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructMatrixSetBoxValues(matrix, part, ilower, iupper, var, nentries, &
        entries, values) result(ierr) bind(c,name="HYPRE_SStructMatrixSetBoxValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: matrix
      integer(c_int), value :: part, var, nentries
      integer(c_int), intent(in) :: ilower(*), iupper(*), entries(*)
      real(c_double), intent(in) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructMatrixGetBoxValues(matrix, part, ilower, iupper, var, nentries, &
        entries, values) result(ierr) bind(c,name="HYPRE_SStructMatrixGetBoxValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: matrix
      integer(c_int), value :: part, var, nentries
      integer(c_int), intent(in) :: ilower(*), iupper(*), entries(*)
      real(c_double), intent(out) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructMatrixAddFEMValues(matrix, part, index, values) &
        result(ierr) bind(c,name="HYPRE_SStructMatrixAddFEMValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: matrix
      integer(c_int), value :: part
      integer(c_int), intent(in) :: index(*)
      real(c_double), intent(in) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructMatrixAssemble(matrix) &
        result(ierr) bind(c,name="HYPRE_SStructMatrixAssemble")
      import c_int, c_ptr
      type(c_ptr), value :: matrix
      integer :: ierr
    end function
    function HYPRE_SStructMatrixPrint(filename, matrix, all) &
        result(ierr) bind(c,name="HYPRE_SStructMatrixPrint")
      import c_int, c_ptr, c_char
      character(kind=c_char), intent(in) :: filename(*)
      type(c_ptr), value :: matrix
      integer(c_int), value :: all
      integer(c_int) :: ierr
    end function
  end interface

  !! SStruct VECTOR INTERFACES
  interface
    function HYPRE_SStructVectorCreate(comm, grid, vector) &
        result(ierr) bind(c,name="fHYPRE_SStructVectorCreate")
      import c_int, c_ptr
      integer, value :: comm
      type(c_ptr), value :: grid
      type(c_ptr) :: vector
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorDestroy(vector) &
        result(ierr) bind(c,name="HYPRE_SStructVectorDestroy")
      import c_int, c_ptr
      type(c_ptr), value :: vector
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorInitialize(vector) &
        result(ierr) bind(c,name="HYPRE_SStructVectorInitialize")
      import c_int, c_ptr
      type(c_ptr), value :: vector
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorGetValues(vector, part, index, var, value) &
        result(ierr) bind(c,name="HYPRE_SStructVectorGetValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), value :: part, var
      integer(c_int) :: index(*)
      real(c_double) :: value
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorAddFEMValues(vector, part, index, values) &
        result(ierr) bind(c,name="HYPRE_SStructVectorAddFEMValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), value :: part
      integer(c_int), intent(in) :: index(*)
      real(c_double), intent(in) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorGetFEMValues(vector, part, index, values) &
        result(ierr) bind(c,name="HYPRE_SStructVectorGetFEMValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), value :: part
      integer(c_int), intent(in) :: index(*)
      real(c_double), intent(out) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorSetBoxValues(vector, part, ilower, iupper, var, values) &
        result(ierr) bind(c,name="HYPRE_SStructVectorSetBoxValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), value :: part, var
      integer(c_int), intent(in) :: ilower(*), iupper(*)
      real(c_double), intent(in) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorGetBoxValues(vector, part, ilower, iupper, var, values) &
        result(ierr) bind(c,name="HYPRE_SStructVectorGetBoxValues")
      import c_int, c_ptr, c_double
      type(c_ptr), value :: vector
      integer(c_int), value :: part, var
      integer(c_int), intent(in) :: ilower(*), iupper(*)
      real(c_double), intent(out) :: values(*)
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorAssemble(vector) &
        result(ierr) bind(c,name="HYPRE_SStructVectorAssemble")
      import c_int, c_ptr
      type(c_ptr), value :: vector
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorGather(vector) &
        result(ierr) bind(c,name="HYPRE_SStructVectorGather")
      import c_int, c_ptr
      type(c_ptr), value :: vector
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorSetObjectType(vector, type) &
        result(ierr) bind(c,name="HYPRE_SStructVectorSetObjectType")
      import c_int, c_ptr
      type(c_ptr), value :: vector
      integer(c_int), value :: type
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorGetObject(vector, object) &
        result(ierr) bind(c,name="HYPRE_SStructVectorGetObject")
      import c_int, c_ptr
      type(c_ptr), value :: vector
      type(c_ptr) :: object
      integer(c_int) :: ierr
    end function
    function HYPRE_SStructVectorPrint(filename, vector, all) &
        result(ierr) bind(c,name="HYPRE_SStructVectorPrint")
      import c_int, c_ptr, c_char
      character(kind=c_char), intent(in) :: filename(*)
      type(c_ptr), value :: vector
      integer(c_int), value :: all
      integer(c_int) :: ierr
    end function
  end interface

 !!
 !! BOOMERAMG INTERFACES
 !!

  interface
    function HYPRE_BoomerAMGCreate(solver) &
        result(ierr) bind(c, name="HYPRE_BoomerAMGCreate")
      import c_ptr, c_int
      type(c_ptr) :: solver
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGDestroy(solver) &
        result(ierr) bind(c, name="HYPRE_BoomerAMGDestroy")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetup(solver, A, b, x) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetup")
      import c_int, c_ptr
      type(c_ptr), value :: solver, A, b, x
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSolve(solver, A, b, x) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSolve")
      import c_ptr, c_int
      type(c_ptr), value :: solver, A, b, x
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGGetNumIterations(solver, num_iterations) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGGetNumIterations")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), intent(out) :: num_iterations
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGGetFinalRelativeResidualNorm(solver, rel_resid_norm) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGGetFinalRelativeResidualNorm")
      import c_ptr, c_double, c_int
      type(c_ptr), value :: solver
      real(c_double), intent(out) :: rel_resid_norm
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetStrongThreshold(solver, strong_threshold) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetStrongThreshold")
      import c_ptr, c_int, c_double
      type(c_ptr), value :: solver
      real(c_double), value :: strong_threshold
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetMaxIter(solver, max_iter) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetMaxIter")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: max_iter
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetCoarsenType(solver, coarsen_type) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetCoarsenType")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: coarsen_type
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetInterpType(solver, interp_type) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetInterpType")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: interp_type
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetTol(solver, tol) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetTol")
      import c_ptr, c_int, c_double
      type(c_ptr), value :: solver
      real(c_double), value :: tol
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetNumSweeps(solver, num_sweeps) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetNumSweeps")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int) , value :: num_sweeps
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetRelaxType(solver, relax_type) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetRelaxType")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: relax_type
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetPrintLevel(solver, print_level) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetPrintLevel")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: print_level
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetMaxLevels(solver, max_levels) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetMaxLevels")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: max_levels
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetCycleNumSweeps(solver, num_sweeps, k) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetCycleNumSweeps")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: num_sweeps, k
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetCycleRelaxType (solver, relax_type, k) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetCycleRelaxType")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: relax_type, k
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetCycleType(solver, cycle_type) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetCycleType")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: cycle_type
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetDebugFlag(solver, debug) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetDebugFlag")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: debug
      integer(c_int) :: ierr
    end function
    function HYPRE_BoomerAMGSetLogging(solver, logging) &
        result(ierr) bind(c,name="HYPRE_BoomerAMGSetLogging")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: logging
      integer(c_int) :: ierr
    end function
  end interface

 !!
 !! STRUCT HYBRID SOLVER INTERFACES
 !!

  interface
    function HYPRE_StructHybridCreate(comm, solver) &
        result(ierr) bind(c,name="fHYPRE_StructHybridCreate")
      import c_ptr, c_int
      integer, value :: comm
      type(c_ptr) :: solver
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridDestroy(solver) &
        result(ierr) bind(c,name="HYPRE_StructHybridDestroy")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetup(solver, A, b, x) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetup")
      import c_ptr, c_int
      type(c_ptr), value :: solver, A, b, x
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSolve(solver, A, b, x) &
        result(ierr) bind(c,name="HYPRE_StructHybridSolve")
      import c_ptr, c_int
      type(c_ptr), value :: solver, A, b, x
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetTol(solver, tol) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetTol")
      import c_ptr, c_int, c_double
      type(c_ptr), value :: solver
      real(c_double), value :: tol
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetConvergenceTol(solver, cf_tol) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetConvergenceTol")
      import c_ptr, c_int, c_double
      type(c_ptr), value :: solver
      real(c_double), value :: cf_tol
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetDSCGMaxIter(solver, ds_max_its) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetDSCGMaxIter")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: ds_max_its
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetPCGMaxIter(solver, pre_max_its) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetPCGMaxIter")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: pre_max_its
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetTwoNorm(solver, two_norm) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetTwoNorm")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: two_norm
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetRelChange(solver, rel_change) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetRelChange")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: rel_change
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetSolverType(solver, solver_type) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetSolverType")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: solver_type
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetKDim(solver, k_dim) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetKDim")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: k_dim
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetLogging(solver, logging) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetLogging")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: logging
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridSetPrintLevel(solver, print_level) &
        result(ierr) bind(c,name="HYPRE_StructHybridSetPrintLevel")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), value :: print_level
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridGetNumIterations(solver, num_its) &
        result(ierr) bind(c,name="HYPRE_StructHybridGetNumIterations")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), intent(out) :: num_its
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridGetDSCGNumIterations(solver, ds_num_its) &
        result(ierr) bind(c,name="HYPRE_StructHybridGetDSCGNumIterations")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), intent(out) :: ds_num_its
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridGetPCGNumIterations(solver, pre_num_its) &
        result(ierr) bind(c,name="HYPRE_StructHybridGetPCGNumIterations")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int), intent(out) :: pre_num_its
      integer(c_int) :: ierr
    end function
    function HYPRE_StructHybridGetFinalRelativeResidualNorm(solver, norm) &
         result(ierr) bind(c,name="HYPRE_StructHybridGetFinalRelativeResidualNorm")
      import c_ptr, c_int, c_double
      type(c_ptr), value :: solver
      real(c_double), intent(out) :: norm
      integer(c_int) :: ierr
    end function
  end interface

 !!
 !! STRUCT HYBRID SOLVER INTERFACES
 !!

  interface
    function HYPRE_StructPFMGCreate(comm, solver) &
        result(ierr) bind(c,name="fHYPRE_StructPFMGCreate")
      import c_ptr, c_int
      integer, value :: comm
      type(c_ptr) :: solver
      integer(c_int) :: ierr
    end function
    function HYPRE_StructPFMGDestroy(solver) &
        result(ierr) bind(c,name="HYPRE_StructPFMGDestroy")
      import c_ptr, c_int
      type(c_ptr), value :: solver
      integer(c_int) :: ierr
    end function
    function HYPRE_StructPFMGSetup(solver, A, b, x) &
        result(ierr) bind(c,name="HYPRE_StructPFMGSetup")
      import c_ptr, c_int
      type(c_ptr), value :: solver, A, b, x
      integer(c_int) :: ierr
    end function
    function HYPRE_StructPFMGSolve(solver, A, b, x) &
        result(ierr) bind(c,name="HYPRE_StructPFMGSolve")
      import c_ptr, c_int
      type(c_ptr), value :: solver, A, b, x
      integer(c_int) :: ierr
    end function
    function HYPRE_StructPFMGSetTol(solver, tol) &
        result(ierr) bind(c,name="HYPRE_StructPFMGSetTol")
      import c_ptr, c_int, c_double
      type(c_ptr), value :: solver
      real(c_double), value :: tol
      integer(c_int) :: ierr
    end function
    function HYPRE_StructPFMGSetMaxIter(solver, maxiter) &
        result(ierr) bind(c,name="HYPRE_StructPFMGSetMaxIter")
      import c_ptr, c_int, c_double
      type(c_ptr), value :: solver
      integer(c_int), value :: maxiter
      integer(c_int) :: ierr
    end function
  end interface

end module hypre_c_binding
