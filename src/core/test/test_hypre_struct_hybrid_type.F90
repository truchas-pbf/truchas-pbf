!! Solving $-\Delta u = 3\sin x\sin y\sin z$ on $[0,\pi/2]^3$ with 0 Dirichlet
!! BC on the low sides and 0 Neumann BC on the high sides.
!! Solution is $u = \sin x\sin y\sin z$.  Use cell-centered finite difference
!! method on regular Cartesian grid with N zones in each coordinate direction.
!!
!! This is a serial example, but is MPI-enabled (for comparison with test3d-amrex).

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program test_hypre_struct_hybrid_type

  use,intrinsic :: iso_fortran_env, only: output_unit
  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use mpi
  use fhypre
  use amrex_mesh_type
  use truchas_amrex_init_proc
  use parameter_list_type
  use amrex_base_module, only: amrex_finalize, amrex_multifab, amrex_mfiter, amrex_box
  use hypre_struct_hybrid_type
  implicit none

  integer,  parameter :: N = 48
  real(r8), parameter :: PI = 3.1415926535897932_r8
  real(r8), parameter :: H = (PI/2)/N
  integer,  parameter :: BSIZE = 7

  integer :: status = 0
  integer :: j, ierr, nproc, this_rank
  type(parameter_list) :: params
  type(amrex_mesh), target :: mesh

  call truchas_amrex_init(params)

  call MPI_Comm_rank(MPI_COMM_WORLD, this_rank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)

  !! Create the AMReX mesh
  call params%set('lo', [1,1,1])
  call params%set('hi', [N,N,N])
  call params%set('box-size', BSIZE)
  call params%set('prob-lo', [0.0_r8, 0.0_r8, 0.0_r8])
  call params%set('prob-hi', [PI/2, PI/2, PI/2])
  call mesh%init(params)

  do j = 0, nproc-1
    if (j == this_rank) then
      write(*,'("process ",i0," has ",i0," boxes")') j, mesh%nbox
    endif
    call MPI_Barrier(MPI_COMM_WORLD,ierr)
  end do

  call test_vector_copy
  call test_solver

  call amrex_finalize
  if (status /= 0) stop 1

contains

  subroutine test_vector_copy

    type(hypre_struct_hybrid) :: solver
    type(parameter_list) :: params
    type(amrex_multifab) :: umf, vmf
    type(amrex_mfiter) :: mfi
    type(hypre_obj) :: x
    real(r8), pointer, contiguous :: dp(:,:,:,:)
    real(r8) :: error
    integer :: stat
    character(:), allocatable :: errmsg

    call solver%init(mesh, params, stat, errmsg)
    if (stat /= 0) then
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      status = 1
      return
    end if

    call mesh%multifab_build(umf, nc=2, ng=0)
    call mesh%multifab_build(vmf, nc=2, ng=0)

    call mesh%mfiter_build(mfi)
    do while (mfi%next())
      dp => umf%dataptr(mfi)
      call random_number(dp)
    end do

    call solver%create_vector(x)
    call solver%copy_multifab_to_vector(umf, 1, x)
    call solver%copy_vector_to_multifab(x, vmf, 2)
    call solver%copy_multifab_to_vector(umf, 2, x)
    call solver%copy_vector_to_multifab(x, vmf, 1)

    call umf%subtract(vmf, 1, 2, 1, 0)
    call umf%subtract(vmf, 2, 1, 1, 0)

    error = umf%norm0()
    if (this_rank == 0) write(output_unit,'(2a)') &
        'test_vector_copy: ', merge('pass','fail',error==0)
    if (error /= 0) status = 1

  end subroutine test_vector_copy


  subroutine test_solver

    type(hypre_struct_hybrid) :: solver
    type(parameter_list) :: params
    type(hypre_obj) :: A, b, x
    type(amrex_multifab) :: u, v
    real(r8) :: max_error, tol
    integer :: ierr, i, j, k, stat
    type(amrex_box) :: bx
    type(amrex_mfiter) :: mfi
    real(r8), pointer, contiguous :: dp(:,:,:,:)
    character(:), allocatable :: errmsg

    !call params%set('max-ds-iter', 0)
    !call params%set('max-pc-iter', 1)
    !call params%set('tol', 1.0d-6)
    !call params%set('conv-rate-tol', 0.9_r8)
    call solver%init(mesh, params, stat, errmsg)
    if (stat /= 0) then
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      status = 1
      return
    end if

    call solver%create_matrix(A)
    call fill_matrix1(A, ierr)  ! Also can use fill_matrix2 for testing hypre
    if (ierr /= 0) then
      if (this_rank == 0) write(output_unit,'(a)') 'solver test: fill matrix hypre error: fail'
      status = 1
      return
    end if
    !call fHYPRE_StructMatrixPrint('A', A, 0, ierr)

    !! Create and fill the RHS vector
    call solver%create_vector(b)
    call fill_rhs(b, ierr)
    if (ierr /= 0) then
      if (this_rank == 0) write(output_unit,'(a)') 'solver test: fill rhs hypre error: fail'
      status = 1
      return
    end if
    !call fHYPRE_StructVectorPrint('b', b, 0, ierr)

    !! Create the solution vector and corresponding multifab.
    call solver%create_vector(x)
    call mesh%multifab_build(v, nc=1, ng=0)
    call v%setval(0.0_r8) ! zero initial solution guess
    call solver%copy_multifab_to_vector(v, 1, x)

    !! Setup the solver for Ax = b and solve.
    call solver%setup(A, b, x)
    call solver%solve(b, x, ierr)
    if (this_rank == 0) write(output_unit,'(3(a,i0),a,es10.3,": ",a)') &
        'solver test: ', solver%num_itr(), ' iterations (', solver%num_dscg_itr(), &
        ' DSCG, ', solver%num_pcg_itr(), ' PCG); rel res norm=', solver%rel_res_norm(), &
        merge('pass','fail',ierr==0)
    if (ierr /= 0) status = 1
    !call write_multifab(v)

    !! Generate exact solution multifab
    call mesh%multifab_build(u, nc=1, ng=0)
    call mesh%mfiter_build(mfi)
    do while (mfi%next())
      bx = mfi%tilebox()
      dp => u%dataptr(mfi)
      do k = bx%lo(3), bx%hi(3)
        do j = bx%lo(2), bx%hi(2)
          do i = bx%lo(1), bx%hi(1)
            dp(i,j,k,1) = u1(i,j,k)
          end do
        end do
      end do
    end do

    !! Compute the solution error
    call solver%copy_vector_to_multifab(x, v, 1)
    call u%subtract(v, 1, 1, 1, 0)
    max_error = u%norm0(); tol = 1.0d-4
    if (this_rank == 0) write(output_unit,'(a,es10.3,": ",a)') &
        'solver test: max solution error=', max_error, merge('pass','fail',max_error<=tol)
    if (max_error > tol) status = 1

  end subroutine test_solver


  !! Fill the matrix by stencil coefficient using the SetBoxValues and AddToBoxValues methods.
  subroutine fill_matrix1(A, ierr)
    type(hypre_obj), intent(in) :: A
    integer, intent(out) :: ierr
    integer :: n, i, lo(3), hi(3)
    real(r8), dimension(BSIZE**3) :: val1 = 6, val2 = -1
    real(r8), dimension(BSIZE**2) :: val3 = 0, val4 = 1, val5 = -1
    call fHYPRE_StructMatrixInitialize(A, ierr)
    do n = 1, mesh%nbox
      call fHYPRE_StructMatrixSetBoxValues(A, mesh%box(n)%lo, mesh%box(n)%hi, [0], val1, ierr)
      do i = 1, 6
        call fHYPRE_StructMatrixSetBoxValues(A, mesh%box(n)%lo, mesh%box(n)%hi, [i], val2, ierr)
      end do
      do i = 1, 3
        if (mesh%box(n)%lo_is_bndry(i)) then
          hi = mesh%box(n)%hi; hi(i) = mesh%box(n)%lo(i)
          call fHYPRE_StructMatrixSetBoxValues(A, mesh%box(n)%lo, hi, [2*i-1], val3, ierr)
          call fHYPRE_StructMatrixAddToBoxValues(A, mesh%box(n)%lo, hi, [0], val4, ierr)
        end if
        if (mesh%box(n)%hi_is_bndry(i)) then
          lo = mesh%box(n)%lo; lo(i) = mesh%box(n)%hi(i)
          call fHYPRE_StructMatrixSetBoxValues(A, lo, mesh%box(n)%hi, [2*i], val3, ierr)
          call fHYPRE_StructMatrixAddToBoxValues(A, lo, mesh%box(n)%hi, [0], val5, ierr)
        end if
      end do
    end do
    call fHYPRE_StructMatrixAssemble(A, ierr)
  end subroutine fill_matrix1

  !! Fill the matrix cell-by-cell using the SetValues and AddToValues methods.
  subroutine fill_matrix2(A, ierr)
    type(hypre_obj), intent(in) :: A
    integer, intent(out) :: ierr
    integer :: i, j, k, n
    real(r8), parameter :: values(*) = [6,-1,-1,-1,-1,-1,-1]
    call fHYPRE_ClearAllErrors
    call fHYPRE_StructMatrixInitialize(A, ierr)
    do n = 1, mesh%nbox
      do k = mesh%box(n)%lo(3), mesh%box(n)%hi(3)
        do j = mesh%box(n)%lo(2), mesh%box(n)%hi(2)
          do i = mesh%box(n)%lo(1), mesh%box(n)%hi(1)
            call fHYPRE_StructMatrixSetValues(A, [i,j,k], [0,1,2,3,4,5,6], values, ierr)
          end do
        end do
      end do
      if (mesh%box(n)%lo_is_bndry(1)) then
        i = mesh%box(n)%lo(1)
        do k = mesh%box(n)%lo(3), mesh%box(n)%hi(3)
          do j = mesh%box(n)%lo(2), mesh%box(n)%hi(2)
            call fHYPRE_StructMatrixSetValues(A, [i,j,k], [1], [0.0_r8], ierr)
            call fHYPRE_StructMatrixAddToValues(A, [i,j,k], [0], [1.0_r8], ierr)
          end do
        end do
      end if
      if (mesh%box(n)%lo_is_bndry(2)) then
        j = mesh%box(n)%lo(2)
        do k = mesh%box(n)%lo(3), mesh%box(n)%hi(3)
          do i = mesh%box(n)%lo(1), mesh%box(n)%hi(1)
            call fHYPRE_StructMatrixSetValues(A, [i,j,k], [3], [0.0_r8], ierr)
            call fHYPRE_StructMatrixAddToValues(A, [i,j,k], [0], [1.0_r8], ierr)
          end do
        end do
      end if
      if (mesh%box(n)%lo_is_bndry(3)) then
        k = mesh%box(n)%lo(3)
        do j = mesh%box(n)%lo(2), mesh%box(n)%hi(2)
          do i = mesh%box(n)%lo(1), mesh%box(n)%hi(1)
            call fHYPRE_StructMatrixSetValues(A, [i,j,k], [5], [0.0_r8], ierr)
            call fHYPRE_StructMatrixAddToValues(A, [i,j,k], [0], [1.0_r8], ierr)
          end do
        end do
      end if
      if (mesh%box(n)%hi_is_bndry(1)) then
        i = mesh%box(n)%hi(1)
        do k = mesh%box(n)%lo(3), mesh%box(n)%hi(3)
          do j = mesh%box(n)%lo(2), mesh%box(n)%hi(2)
            call fHYPRE_StructMatrixSetValues(A, [i,j,k], [2], [0.0_r8], ierr)
            call fHYPRE_StructMatrixAddToValues(A, [i,j,k], [0], [-1.0_r8], ierr)
          end do
        end do
      end if
      if (mesh%box(n)%hi_is_bndry(2)) then
        j = mesh%box(n)%hi(2)
        do k = mesh%box(n)%lo(3), mesh%box(n)%hi(3)
          do i = mesh%box(n)%lo(1), mesh%box(n)%hi(1)
            call fHYPRE_StructMatrixSetValues(A, [i,j,k], [4], [0.0_r8], ierr)
            call fHYPRE_StructMatrixAddToValues(A, [i,j,k], [0], [-1.0_r8], ierr)
          end do
        end do
      end if
      if (mesh%box(n)%hi_is_bndry(3)) then
        k = mesh%box(n)%hi(3)
        do j = mesh%box(n)%lo(2), mesh%box(n)%hi(2)
          do i = mesh%box(n)%lo(1), mesh%box(n)%hi(1)
            call fHYPRE_StructMatrixSetValues(A, [i,j,k], [6], [0.0_r8], ierr)
            call fHYPRE_StructMatrixAddToValues(A, [i,j,k], [0], [-1.0_r8], ierr)
          end do
        end do
      end if
    end do
    call fHYPRE_StructMatrixAssemble(A, ierr)
  end subroutine fill_matrix2

  subroutine fill_rhs(b, ierr)
    type(hypre_obj), intent(in) :: b
    integer, intent(out) :: ierr
    integer :: n, i, j, k
    call fHYPRE_ClearAllErrors
    call fHYPRE_StructVectorInitialize(b, ierr)
    do n = 1, mesh%nbox
      do k = mesh%box(n)%lo(3), mesh%box(n)%hi(3)
        do j = mesh%box(n)%lo(2), mesh%box(n)%hi(2)
          do i = mesh%box(n)%lo(1), mesh%box(n)%hi(1)
            call fHYPRE_StructVectorSetValues(b, [i,j,k], q1(i,j,k)*H**2, ierr)
          end do
        end do
      end do
    end do
    call fHYPRE_StructVectorAssemble(b, ierr)
  end subroutine fill_rhs

  !! Exact solution
  function u1(i,j,k) result(u)
    integer, intent(in) :: i, j, k
    real(r8) :: u, x, y, z
    x = (i-(mesh%domain%lo(1)-0.5_r8))*H
    y = (j-(mesh%domain%lo(2)-0.5_r8))*H
    z = (k-(mesh%domain%lo(3)-0.5_r8))*H
    u = sin(x)*sin(y)*sin(z)
  end function

  !! Source term for exact solution U1
  function q1(i,j,k) result(q)
    integer, intent(in) :: i, j, k
    real(r8) :: q
    q = 3*u1(i,j,k)
  end function

  subroutine write_multifab(mf)
    use amrex_base_module, only: amrex_string, amrex_string_build, amrex_write_plotfile
    type(amrex_multifab), intent(in) :: mf
    type(amrex_string) :: label(1)
    call amrex_string_build(label(1), 'u')
    call amrex_write_plotfile('plt0', 1, [mf], label, [mesh%geom], 0.0_r8, [1], [1])
  end subroutine

end program test_hypre_struct_hybrid_type
