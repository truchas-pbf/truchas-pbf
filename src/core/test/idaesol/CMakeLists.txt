add_executable(test_idaesol test_idaesol.F90 box_vector_type.F90)
target_link_libraries(test_idaesol truchas-pbf)
#add_test(idaesol_type test_idaesol)

add_executable(nka_example nka_example.F90 box_vector_type.F90)
target_link_libraries(nka_example truchas-pbf)
#add_test(nka_example nka_example)

