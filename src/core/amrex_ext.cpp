#define AMREX_USE_HYPRE
#include "AMReX_iMultiFab.H"
#include "AMReX_MultiFab.H"
#include "AMReX_MultiFabUtil.H"
#include "AMReX_MLMG.H"
#include "AMReX_ParallelReduce.H"

using namespace amrex;

extern "C"
{
  void amrex_ext_set_bottom_maxiter(MLMG& mlmg, int n)
  {
    mlmg.setBottomMaxIter(n);
  }


  void amrex_ext_set_bottom_tolerance(MLMG& mlmg, double t)
  {
    mlmg.setBottomTolerance(t);
  }


  void amrex_ext_set_bottom_tolerance_abs(MLMG& mlmg, double t)
  {
    mlmg.setBottomToleranceAbs(t);
  }


  // This sets Hypre defaults, which aren't necessarily AMReX's default configuration for Hypre.
  void amrex_ext_set_hypre_interface_ij(MLMG& mlmg)
  {
    mlmg.setHypreInterface(Hypre::Interface::ij);
    mlmg.setHypreOldDefault(false);
    //mlmg.setHypreCoarsenType(10); // hypre default 10
    mlmg.setHypreRelaxType(18); // hypre default 13?
    mlmg.setHypreRelaxOrder(0); // hypre default 0
    mlmg.setHypreNumSweeps(1); // hypre default 1
    mlmg.setHypreStrongThreshold(0.5); // hypre default 0.25, should be 0.5 for 3D problems
  }


  iMultiFab* amrex_ext_makeFineMask(const MultiFab& cmf, const MultiFab& fmf,
                                    int nghost, int ratio)
  {
    return new iMultiFab(makeFineMask(cmf, fmf,
                                      IntVect(nghost), IntVect(ratio),
                                      Periodicity::NonPeriodic(), 1, 0));
  }


  Real amrex_ext_dot(const iMultiFab& mask,
                     const MultiFab& x, int xcomp,
                     const MultiFab& y, int ycomp,
                     int num_comp, int nghost)
  {
    // Need to convert from 1-based to 0-based indexing.
    return MultiFab::Dot(mask, x, xcomp-1, y, ycomp-1, num_comp, nghost);
  }


  void amrex_ext_imf_copy(iMultiFab& dst, const iMultiFab& src,
                          int srccomp, int dstcomp, int numcomp, int nghost)
  {
    // Need to convert from 1-based to 0-based indexing.
    iMultiFab::Copy(dst, src, srccomp-1, dstcomp-1, numcomp, nghost);
  }


  void amrex_ext_mlmg_apply(MLMG& mlmg, MultiFab* out[], MultiFab* in[])
  {
    const int nlev = mlmg.numAMRLevels();
    amrex::Vector<MultiFab*> vout{out, out+nlev}, vin{in, in+nlev};
    mlmg.apply(vout, vin);
  }
}
