/*==============================================================================

  Copyright (c) Los Alamos National Security, LLC.  This file is part of the
  Truchas code (LA-CC-15-097) and is subject to the revised BSD license terms
  in the LICENSE file found in the top-level directory of this distribution.

==============================================================================*/

#include "HYPRE_struct_ls.h"
#include "HYPRE_sstruct_ls.h"

#ifdef HYPRE_SEQUENTIAL
#define MPI_Fint int
#endif

int fHYPRE_StructGridCreate(MPI_Fint comm, int ndim, HYPRE_StructGrid *grid)
{
  return HYPRE_StructGridCreate(MPI_Comm_f2c(comm), ndim, grid);
}

int fHYPRE_StructMatrixCreate(MPI_Fint comm, HYPRE_StructGrid grid, HYPRE_StructStencil stencil, HYPRE_StructMatrix *matrix)
{
  return HYPRE_StructMatrixCreate(MPI_Comm_f2c(comm), grid, stencil, matrix);
}

int fHYPRE_StructVectorCreate(MPI_Fint comm, HYPRE_StructGrid grid, HYPRE_StructVector *vector)
{
  return HYPRE_StructVectorCreate(MPI_Comm_f2c(comm), grid, vector);
}

int fHYPRE_SStructGridCreate(MPI_Fint comm, int ndim, int nparts, HYPRE_SStructGrid *grid)
{
  return HYPRE_SStructGridCreate(MPI_Comm_f2c(comm), ndim, nparts, grid);
}

int fHYPRE_SStructGraphCreate(MPI_Fint comm, HYPRE_SStructGrid grid, HYPRE_SStructGraph *graph)
{
  return HYPRE_SStructGraphCreate(MPI_Comm_f2c(comm), grid, graph);
}

int fHYPRE_SStructMatrixCreate(MPI_Fint comm, HYPRE_SStructGraph graph, HYPRE_SStructMatrix *matrix)
{
  return HYPRE_SStructMatrixCreate(MPI_Comm_f2c(comm), graph, matrix);
}

int fHYPRE_SStructVectorCreate(MPI_Fint comm, HYPRE_SStructGrid grid, HYPRE_SStructVector *vector)
{
  return HYPRE_SStructVectorCreate(MPI_Comm_f2c(comm), grid, vector);
}

int fHYPRE_StructHybridCreate(MPI_Fint comm, HYPRE_StructSolver *solver)
{
  return HYPRE_StructHybridCreate(MPI_Comm_f2c(comm), solver);
}

int fHYPRE_StructPFMGCreate(MPI_Fint comm, HYPRE_StructSolver *solver)
{
  return HYPRE_StructPFMGCreate(MPI_Comm_f2c(comm), solver);
}
