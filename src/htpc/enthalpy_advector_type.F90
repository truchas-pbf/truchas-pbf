!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module enthalpy_advector_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use ht_matl_class
  use vof_solver_type
  use amrex_base_module, only: amrex_multifab
  use amrex_mesh_type
  use timer_tree_type
  implicit none
  private

  type, public :: enthalpy_advector
    private
    class(ht_matl), pointer :: mat => null() ! reference only -- not owned
    type(vof_solver), pointer :: solver => null() ! reference only -- not owned
    type(amrex_mesh), pointer :: mesh => null()   ! reference only -- not owned
  contains
    procedure :: init
    procedure :: get_source_density
  end type enthalpy_advector

contains

  subroutine init(this, mat, solver)

    class(enthalpy_advector), intent(out) :: this
    class(ht_matl), intent(in), target  :: mat
    type(vof_solver), intent(in), target  :: solver

    this%mat => mat
    this%solver => solver
    this%mesh => solver%mesh

  end subroutine init


  !NB: ghost values of q_mf are not defined.
  subroutine get_source_density(this, temp_mf, q_mf)

    use ml_operators, only: mirror_boundary_ghosts

    class(enthalpy_advector), intent(in) :: this
    type(amrex_multifab), intent(in) :: temp_mf ! 1 ghost layer
    type(amrex_multifab), intent(inout) :: q_mf ! 1 ghost layer (TODO: should need none)

    type(amrex_multifab) :: h_mf ! enthalpy density

    ! Both TEMP and Q must be consistent with this iterator

    call start_timer('heat-advection')
    call this%mesh%multifab_build(h_mf, nc=1, ng=1)
    call h_mf%setval(0.0_r8) ! put something on the domain ghosts
    call this%mat%compute_enthalpy_density(temp_mf, h_mf)
    call mirror_boundary_ghosts(h_mf, this%mesh)
    call this%solver%advect_delta(h_mf, q_mf)
    call q_mf%mult(1.0_r8/this%solver%dt, 1, 1, 0)
    call stop_timer('heat-advection')

  end subroutine get_source_density

end module enthalpy_advector_type
