!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module pbf_matl_model_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use scalar_func_class
  use scalar_func_containers, only: scalar_func_box
  use inverse_func_class
  use amrex_mesh_type
  use truchas_env_type
  use ht_matl_class
  implicit none
  private

  type, extends(inverse_func) :: invf
    class(pbf_matl_model), pointer :: mat
  contains
    procedure :: g => H_of_T
  end type invf

  !TODO: This inheritance is a temporary stop gap
  !type, public :: pbf_matl_model
  type, extends(ht_matl), public :: pbf_matl_model
    type(amrex_mesh), pointer :: mesh => null() ! reference only -- not owned
    real(r8) :: rho
    class(scalar_func), allocatable :: visc, drho
    type(scalar_func_box), allocatable :: mat_kappa(:)
    type(scalar_func_box), allocatable :: mat_enth_dens(:)
    real(r8) :: L           ! latent heat of fusion [energy/mass]
    real(r8) :: Tsol, Tliq  ! solidus, liquidus temperatures
    real(r8) :: Hsol, Hliq
    integer  :: sfunc
    type(invf) :: T_of_H
  contains
    procedure :: init
    procedure :: compute_kappa
    procedure :: compute_enthalpy_density
    procedure :: compute_enthalpy_density_deriv
    procedure :: liquid_frac
    procedure :: compute_temp
    procedure :: dump_stats
    procedure, private :: kappa
    procedure, private :: enthalpy_density
    procedure, private :: enthalpy_density_deriv
    procedure, private :: temp
  end type pbf_matl_model

contains

  subroutine init(this, env, mesh, params, stat, errmsg)

    use truchas_env_type
    use parameter_list_type
    use scalar_func_factories
    use scalar_func_tools

    class(pbf_matl_model), intent(out), target :: this
    type(truchas_env), intent(in) :: env
    type(amrex_mesh), intent(in), target :: mesh
    type(parameter_list), intent(inout) :: params
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    character(:), allocatable :: context, string
    type(parameter_list), pointer :: plist
    class(scalar_func), allocatable :: cp, h, rho
    real(r8) :: t0, h0

    this%mesh => mesh

    context = 'processing ' // params%name() // ': '

    call params%get('density', this%rho, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if

    call params%get('solidus-temp', this%Tsol, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    call params%get('liquidus-temp', this%Tliq, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    if (this%Tsol >= this%Tliq) then
      stat = 1
      errmsg = context // 'must have "solidus-temp" < "liquidus_temp"'
      return
    end if

    call params%get('sigma-func-type', string, default='smoother-step', stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    select case (string)
    case ('linear-step')
      this%sfunc = 0
    case ('smooth-step')
      this%sfunc = 1
    case ('smoother-step')
      this%sfunc = 2
    case default
      stat = 1
      errmsg = context // 'invalid "sigma-func-type" value: ' // string
      return
    end select

    call params%get('latent-heat', this%L, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    if (this%L < 0.0_r8) then
      stat = 1
      errmsg = '"latent-heat" must be > 0.0'
      return
    end if

    allocate(this%mat_kappa(2), this%mat_enth_dens(2))

    if (params%is_sublist('solid')) then
      plist => params%sublist('solid')
      !! Thermal conductivity
      call get_scalar_func(plist, 'conductivity', this%mat_kappa(1)%f, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if
      !! Specific heat capacity
      call get_scalar_func(plist, 'specific-heat', cp, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if
      !! Enthalpy density
      t0 = 0.0_r8; h0 = 0.0_r8
      call alloc_scalar_func_antideriv(cp, t0, h0, h, stat, errmsg)
      if (stat /= 0) then
        errmsg = context // 'unable to integrate the "specific-heat" property'
        return
      end if
      call alloc_const_scalar_func(rho, this%rho)
      call alloc_scalar_func_product(rho, h, this%mat_enth_dens(1)%f, stat, errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if
    else
      stat = 1
      errmsg = context // 'missing "solid" sublist parameter'
      return
    end if

    if (params%is_sublist('liquid')) then
      plist => params%sublist('liquid')
      !! Thermal conductivity
      call get_scalar_func(plist, 'conductivity', this%mat_kappa(2)%f, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if
      !! Specific heat capacity
      call get_scalar_func(plist, 'specific-heat', cp, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if
      !! Enthalpy density
      t0 = this%Tliq; h0 = this%L + h%eval([t0])
      call alloc_scalar_func_antideriv(cp, t0, h0, h, stat, errmsg)
      if (stat /= 0) then
        errmsg = context // 'unable to integrate the "specific-heat" property'
        return
      end if
      call alloc_scalar_func_product(rho, h, this%mat_enth_dens(2)%f, stat, errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if
      !! Viscosity (optional)
      if (plist%is_parameter('viscosity')) then
        call get_scalar_func(plist, 'viscosity', this%visc, stat=stat, errmsg=errmsg)
        if (stat /= 0) then
          errmsg = context // errmsg
          return
        end if
      end if
      !! Relative density deviation (optional)
      if (plist%is_parameter('density-deviation')) then
        call get_scalar_func(plist, 'density-deviation', this%drho, stat=stat, errmsg=errmsg)
        if (stat /= 0) then
          errmsg = context // errmsg
          return
        end if
      end if
    else
      stat = 1
      errmsg = context // 'missing "liquid" sublist parameter'
      return
    end if

    this%Hsol = this%enthalpy_density(this%Tsol)
    this%Hliq = this%enthalpy_density(this%Tliq)

    call this%T_of_H%init(eps=0.0_r8,maxadj=12)
    this%T_of_H%mat => this

  end subroutine init

  !! This auxiliary subroutine gets the scalar function specified by the value
  !! of the parameter PARAM in the parameter list PLIST. The parameter value is
  !! either a real acalar or a sublist that defines the function.

  subroutine get_scalar_func(plist, param, f, stat, errmsg)

    use scalar_func_factories
    use parameter_list_type

    type(parameter_list), intent(inout) :: plist
    character(*), intent(in) :: param
    class(scalar_func), allocatable, intent(out) :: f
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    real(r8) :: const
    type(parameter_list), pointer :: fparams

    if (plist%is_parameter(param)) then
      if (plist%is_sublist(param)) then ! function defined by parameter list
        fparams => plist%sublist(param)
        call alloc_scalar_func(f, fparams, stat, errmsg)
        if (stat /= 0) return
      else  ! it must be a constant value
        call plist%get(param, const, stat=stat, errmsg=errmsg)
        if (stat /= 0) return
        call alloc_const_scalar_func(f, const)
      end if
    else
      stat = 1
      errmsg = 'missing "' // param // '" parameter'
      return
    end if

  end subroutine get_scalar_func


  function kappa(this, temp)
    class(pbf_matl_model), intent(in) :: this
    real(r8), intent(in) :: temp
    real(r8) :: kappa, args(1), vfrac(2)
    integer :: m
    kappa = 0.0_r8
    args(1) = temp
    vfrac(2) = liquid_frac(this, temp)
    vfrac(1) = 1 - vfrac(2)
    do m = 1, size(this%mat_kappa)
      if (vfrac(m) > 0.0_r8) kappa = kappa + vfrac(m)*this%mat_kappa(m)%eval(args)
    end do
  end function kappa

  !! This computes the cell-based thermal conductivities that correspond to the
  !! given cell-based temperatures. The input and outputs must be compatible
  !! multifabs having the same number of ghosts. If the temperature multifab
  !! includes ghosts, its values on the ghosts should be synced so that the
  !! computed conductivies on the ghosts will be consistent; this procedure
  !! does not explicitly sync ghost values. Temperature values on domain ghost
  !! cells, if any, are never referenced. Likewise, conductivity values on
  !! domain ghost cells are never modified.

  subroutine compute_kappa(this, temp, cond)

    use amrex_base_module

    class(pbf_matl_model), intent(in) :: this
    type(amrex_multifab), intent(in) :: temp
    type(amrex_multifab), intent(inout) :: cond

    integer :: ix, iy, iz, lo(3), hi(3)
    type(amrex_mfiter) :: mfi
    real(r8), pointer, contiguous :: dp1(:,:,:), dp2(:,:,:)

    call this%mesh%mfiter_build(mfi)
    do while (mfi%next())
      call get_component_ptr(temp%dataptr(mfi), 1, dp1)
      call get_component_ptr(cond%dataptr(mfi), 1, dp2)
      lo = max(lbound(dp1), this%mesh%domain%lo)
      hi = min(ubound(dp1), this%mesh%domain%hi)
      do iz = lo(3), hi(3)
        do iy = lo(2), hi(2)
          do ix = lo(1), hi(1)
            dp2(ix,iy,iz) = this%kappa(dp1(ix,iy,iz))
          end do
        end do
      end do
    end do

  contains

    subroutine get_component_ptr(dp, n, cdp)
      real(r8), pointer, contiguous, intent(in) :: dp(:,:,:,:)
      integer, intent(in) :: n
      real(r8), pointer, contiguous, intent(out) :: cdp(:,:,:)
      cdp(lbound(dp,1):,lbound(dp,2):,lbound(dp,3):) => dp(:,:,:,n)
    end subroutine get_component_ptr

  end subroutine compute_kappa

  function enthalpy_density(this, temp) result(H)
    class(pbf_matl_model), intent(in) :: this
    real(r8), intent(in) :: temp
    real(r8) :: H, args(1), vfrac(2)
    integer :: m
    H = 0.0_r8
    args(1) = temp
    vfrac(2) = liquid_frac(this, temp)
    vfrac(1) = 1 - vfrac(2)
    do m = 1, size(this%mat_enth_dens)
      if (vfrac(m) > 0) H = H + vfrac(m)*this%mat_enth_dens(m)%eval(args)
    end do
  end function enthalpy_density

  !! This computes the cell-based enthalpy densities that correspond to the
  !! given cell-based temperatures. The input and outputs must be compatible
  !! multifabs with the input having at least as many ghosts as the output.
  !! If the output multifab does include ghosts, the temperature values on
  !! the corresponding input multifab ghosts should be synced so that the
  !! output is consistent; this procedure does not explicitly sync ghost
  !! values. Temperature values on domain ghost cells, if any, are never
  !! referenced. Likewise, enthalpy values on domain ghost cells are never
  !! modified.

  subroutine compute_enthalpy_density(this, temp, h)

    use amrex_base_module

    class(pbf_matl_model), intent(in) :: this
    type(amrex_multifab), intent(in) :: temp
    type(amrex_multifab), intent(inout) :: h

    integer :: ix, iy, iz, lo(3), hi(3)
    type(amrex_mfiter) :: mfi
    real(r8), pointer, contiguous :: dp1(:,:,:), dp2(:,:,:)

    ASSERT(temp%nghost() >= h%nghost())

    call this%mesh%mfiter_build(mfi)
    do while (mfi%next())
      call get_component_ptr(temp%dataptr(mfi), 1, dp1)
      call get_component_ptr(h%dataptr(mfi), 1, dp2)
      lo = max(lbound(dp2), this%mesh%domain%lo)
      hi = min(ubound(dp2), this%mesh%domain%hi)
      do iz = lo(3), hi(3)
        do iy = lo(2), hi(2)
          do ix = lo(1), hi(1)
            dp2(ix,iy,iz) = this%enthalpy_density(dp1(ix,iy,iz))
          end do
        end do
      end do
    end do

  contains

    subroutine get_component_ptr(dp, n, cdp)
      real(r8), pointer, contiguous, intent(in) :: dp(:,:,:,:)
      integer, intent(in) :: n
      real(r8), pointer, contiguous, intent(out) :: cdp(:,:,:)
      cdp(lbound(dp,1):,lbound(dp,2):,lbound(dp,3):) => dp(:,:,:,n)
    end subroutine get_component_ptr

  end subroutine compute_enthalpy_density

  function enthalpy_density_deriv(this, T) result(dHdT)
    class(pbf_matl_model), intent(in) :: this
    real(r8), intent(in) :: T
    real(r8) :: dHdT, delta
    !real(r8), parameter :: sreps = scale(1.0_r8,(exponent(epsilon(sreps))-1)/2) ! Intel has issues
    real(r8) :: sreps
    sreps = scale(1.0_r8,(exponent(epsilon(sreps))-1)/2)
    delta = scale(sreps,exponent(T)) ! largest power of 2 no greater than |T|*sqrt(epsilon)
    dHdT = (enthalpy_density(this,T+delta)-enthalpy_density(this,T-delta))/(2*delta)
  end function enthalpy_density_deriv

  !! This computes the temperature derivative of the cell enthalpy densitites
  !! given the cell temperatures.  The input and outputs must be compatible
  !! multifabs. Values on ghost cells, if any, are never referenced and never
  !! set.

  subroutine compute_enthalpy_density_deriv(this, T, dHdT)

    use amrex_base_module

    class(pbf_matl_model), intent(in) :: this
    type(amrex_multifab), intent(in) :: T
    type(amrex_multifab), intent(inout) :: dHdT

    integer :: n, ix, iy, iz
    type(amrex_mfiter) :: mfi
    real(r8), pointer, contiguous :: dp1(:,:,:), dp2(:,:,:)

    n = 0
    call this%mesh%mfiter_build(mfi)
    do while (mfi%next())
      n = n + 1
      call get_component_ptr(T%dataptr(mfi), 1, dp1)
      call get_component_ptr(dHdT%dataptr(mfi), 1, dp2)
      associate (lo => this%mesh%box(n)%lo, hi => this%mesh%box(n)%hi)
        do iz = lo(3), hi(3)
          do iy = lo(2), hi(2)
            do ix = lo(1), hi(1)
              dp2(ix,iy,iz) = this%enthalpy_density_deriv(dp1(ix,iy,iz))
            end do
          end do
        end do
      end associate
    end do

  contains

    subroutine get_component_ptr(dp, n, cdp)
      real(r8), pointer, contiguous, intent(in) :: dp(:,:,:,:)
      integer, intent(in) :: n
      real(r8), pointer, contiguous, intent(out) :: cdp(:,:,:)
      cdp(lbound(dp,1):,lbound(dp,2):,lbound(dp,3):) => dp(:,:,:,n)
    end subroutine get_component_ptr

  end subroutine compute_enthalpy_density_deriv

  !! NB: The C1 step seems in practice to produce a more easily solved problem
  !! than the C2 step, despite its additional smoothness. This is probably due
  !! to the C2 step having a larger gradient.
  elemental function liquid_frac(this, T) result(f)
    class(pbf_matl_model), intent(in) :: this
    real(r8), intent(in) :: T
    real(r8) :: f
    if (T <= this%Tsol) then
      f = 0.0_r8
    else if (T >= this%Tliq) then
      f = 1.0_r8
    else
      f = (T - this%Tsol)/(this%Tliq - this%Tsol)
      select case (this%sfunc)
      case (1)
        f = f*f*(3 - 2*f)
      case (2)
        f = f*f*f*(10 - f*(15 - 6*f))
      end select
    end if
!    select case (this%sfunc)
!    case (1)
!      f = C1_step(this%Tsol, this%Tliq, T)
!    case (2)
!      f = C2_step(this%Tsol, this%Tliq, T)
!    end select
  end function liquid_frac

  !! C1-smooth step function: 0 for X < X1, 1 for X > X2, monotone on [X1,X2]
  pure function C1_step(x1, x2, x) result(fx)
    real(r8), intent(in) :: x1, x2, x
    real(r8) :: fx, z
    if (x <= x1) then
      fx = 0.0_r8
    else if (x >= x2) then
      fx = 1.0_r8
    else
      z = (x - x1)/(x2 - x1)
      fx = z*z*(3 - 2*z)
    end if
  end function C1_step

  !! C2-smooth step function: 0 for X < X1, 1 for X > X2, monotone on [X1,X2]
  pure function C2_step(x1, x2, x) result(fx)
    real(r8), intent(in) :: x1, x2, x
    real(r8) :: fx, z
    if (x <= x1) then
      fx = 0.0_r8
    else if (x >= x2) then
      fx = 1.0_r8
    else
      z = (x - x1)/(x2 - x1)
      fx = z*z*z*(10 - z*(15 - 6*z))
    end if
  end function C2_step

  !FIXME: probably need to rework this to pass in a guess for the temperature
  !       interval, as we no longer have any decent guess for solid and liquid.
  function temp(this, H) result(T)
    class(pbf_matl_model), intent(inout) :: this
    real(r8), intent(in) :: H
    real(r8) :: T
    if (H <= this%Hsol) then
      call this%T_of_H%compute(H, this%Tsol-1, this%Tsol, T)
    else if (H < this%Hliq) then
      call this%T_of_H%compute(H, this%Tsol, this%Tliq, T)
    else
      call this%T_of_H%compute(H, this%Tliq, this%Tliq+1, T)
    end if
  end function temp

  function H_of_T(this, x) result(H)
    class(invf), intent(in) :: this
    real(r8), intent(in) :: x
    real(r8) :: H
    H = this%mat%enthalpy_density(x)
  end function H_of_T

  subroutine dump_stats(this)
    class(pbf_matl_model), intent(in) :: this
    print *, this%T_of_H%num_itr/this%T_of_H%num_call, this%T_of_H%max_itr
  end subroutine dump_stats

  !! This computes the cell temperatures that correspond to the given cell
  !! enthalpy densities. The input and outputs must be compatible multifabs.
  !! Values on ghost cells, are referenced and set only if they exist in both
  !! multifabs.

  subroutine compute_temp(this, H, T)

    use amrex_base_module

    class(pbf_matl_model), intent(inout) :: this
    type(amrex_multifab), intent(in) :: H
    type(amrex_multifab), intent(inout) :: T

    integer :: n, ix, iy, iz, lo(3), hi(3)
    type(amrex_mfiter) :: mfi
    real(r8), pointer, contiguous :: dp1(:,:,:), dp2(:,:,:)

    n = 0
    call this%mesh%mfiter_build(mfi)
    do while (mfi%next())
      n = n + 1
      call get_component_ptr(H%dataptr(mfi), 1, dp1)
      call get_component_ptr(T%dataptr(mfi), 1, dp2)
      lo = max(lbound(dp1), lbound(dp2), this%mesh%domain%lo)
      hi = min(ubound(dp1), ubound(dp2), this%mesh%domain%hi)
      do iz = lo(3), hi(3)
        do iy = lo(2), hi(2)
          do ix = lo(1), hi(1)
            dp2(ix,iy,iz) = this%temp(dp1(ix,iy,iz))
          end do
        end do
      end do
    end do

  contains

    subroutine get_component_ptr(dp, n, cdp)
      real(r8), pointer, contiguous, intent(in) :: dp(:,:,:,:)
      integer, intent(in) :: n
      real(r8), pointer, contiguous, intent(out) :: cdp(:,:,:)
      cdp(lbound(dp,1):,lbound(dp,2):,lbound(dp,3):) => dp(:,:,:,n)
    end subroutine get_component_ptr

  end subroutine compute_temp

end module pbf_matl_model_type
