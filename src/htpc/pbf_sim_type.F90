!!
!! PBF_SIM_TYPE
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! January 2018
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module pbf_sim_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use ml_mesh_type
  use amrex_base_module, only: amrex_multifab
  use pbf_matl_model_type
  use pbf_model_type
  use pbf_solver_type
  use laser_scan_type
  use sim_event_queue_type
  use time_step_sync_type
  use timer_tree_type
  use simlog_type
  use truchas_env_type
  implicit none
  private

  type, public :: pbf_sim
    type(truchas_env), pointer :: env => null() ! reference only
    type(ml_mesh) :: mesh
    type(pbf_matl_model) :: mat
    type(pbf_model) :: model
    type(pbf_solver) :: solver
    type(laser_scan), allocatable :: lscan
    type(sim_event_queue) :: eventq
    !! Integration control
    real(r8) :: t_init
    real(r8) :: tlast, hlast
    real(r8) :: dt_init, dt_min, dt_max, dt_explicit
    integer  :: max_try, ts_strategy
    integer  :: step = 1
    logical  :: explicit_ht
    real(r8), allocatable :: tout(:)
    type(time_step_sync) :: ts_sync
    type(amrex_multifab), allocatable :: temp(:)
    integer :: ndump = 0
    real(r8) :: exaca_csf, exaca_tsf
  contains
    procedure :: init
    procedure :: run
  end type pbf_sim

  type, extends(event_action) :: output_event
  end type

  type, extends(event_action) :: stop_event
  end type

  integer, parameter, public :: DT_POLICY_NONE   = 0
  integer, parameter, public :: DT_POLICY_NEXT   = 1
  integer, parameter, public :: DT_POLICY_FACTOR = 2
  integer, parameter, public :: DT_POLICY_VALUE  = 3

  type, extends(event_action) :: path_event
    private
    integer  :: dt_policy = DT_POLICY_NONE
    real(r8) :: c = 0.0_r8
  contains
    procedure :: init_dt => path_event_init_dt
  end type path_event

contains

  subroutine init(this, env, params)

    use parameter_list_type
    use scalar_func_factories
    use truchas_scalar_func_table
    use physical_constants, only: set_physical_constants
    use signal_handler, only: init_signal_handler, SIGURG

    class(pbf_sim), intent(out), target :: this
    type(truchas_env), intent(in),  target :: env
    type(parameter_list), intent(inout) :: params

    integer :: j, stat
    real(r8) :: const, dt_factor
    type(parameter_list), pointer :: plist
    character(:), allocatable :: errmsg, context
    class(scalar_func), allocatable :: f

    this%env => env

    call start_timer('initialization')
    call this%env%log%info('initializing the simulation', VERB_NOISY)

    !! Catch SIGURG signals.
    call init_signal_handler(SIGURG)

    !! Simulation control parameters
    if (params%is_sublist('sim-control')) then
      plist => params%sublist('sim-control')
      context = 'processing ' // plist%name() // ': '
      call plist%get('initial-time', this%t_init, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      call plist%get('initial-time-step', this%dt_init, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      if (this%dt_init <= 0.0_r8) call this%env%log%fatal(context//'"initial-time-step" must be > 0.0')
      call plist%get('min-time-step', this%dt_min, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      call plist%get('max-time-step', this%dt_max, default=huge(1.0_r8), stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      call plist%get('max-try-at-step', this%max_try, default=10, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      if (this%dt_min > this%dt_init) call this%env%log%fatal(context//'require "min-time-step" <= "initial-time-step"')
      call plist%get('output-times', this%tout, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      call plist%get('restart-time-step-factor', dt_factor, default=1.0e-3_r8, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      call plist%get('time-stepping-strategy', this%ts_strategy, default=0, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      select case (this%ts_strategy)
      case (0) ! default implicit
      case (1:2) ! explicit/implicit heat transfer
        call plist%get('explicit-time-step', this%dt_explicit, default=this%dt_max, stat=stat, errmsg=errmsg)
        if (stat /= 0) call this%env%log%fatal(context//errmsg)
      case default
        call this%env%log%fatal(context//'invalid value for "time-stepping-strategy"')
      end select
      !TODO: check for strictly increasing values in TOUT, TOUT > t_init, or sort
      !and cull those < t_init.
      call plist%get('exaca-time-scale-factor', this%exaca_tsf, default=1.0_r8, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      if (this%exaca_tsf <= 0.0_r8) call this%env%log%fatal(context//'"exaca-time-scale-factor" must be > 0.0')
      call plist%get('exaca-coord-scale-factor', this%exaca_csf, default=1.0_r8, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(context//errmsg)
      if (this%exaca_csf <= 0.0_r8) call this%env%log%fatal(context//'"exaca-coord-scale-factor" must be > 0.0')
    else
      call this%env%log%fatal('missing "sim-control" sublist parameter')
    end if

    !! Redefine the physical constants if specified.
    if (params%is_sublist('physical-constants')) then
      plist => params%sublist('physical-constants')
      call set_physical_constants(plist, stat, errmsg)
      if (stat /= 0) call this%env%log%fatal(errmsg)
    end if

    !! Create the mesh object.
    call start_timer('mesh')
    if (params%is_sublist('mesh')) then
      plist => params%sublist('mesh')
      call this%mesh%init(plist, stat, errmsg)
      if (stat /= 0) call this%env%log%fatal(errmsg)
    else
      call this%env%log%fatal('missing "mesh" sublist parameter')
    end if
    call stop_timer('mesh')

    !! Create the laser scan path object. [optional]
    if (params%is_sublist('laser-scan-path')) then
      call start_timer('laser-scan')
      allocate(this%lscan)
      plist => params%sublist('laser-scan-path')
      call this%lscan%init(this%env%comm, plist, stat, errmsg)
      if (stat /= 0) call this%env%log%fatal('processing '//plist%name()//': '//errmsg)
      call this%lscan%set_initial_state(this%t_init)
      call this%lscan%alloc_laser_scalar_func(f)
      if (plist%is_sublist('laser'))then
        call insert_func('laser', f)
      else if (plist%is_sublist('vol-laser')) then
        call insert_func('vol-laser',f)
      endif
      call stop_timer('laser-scan')
    else
      if (this%ts_strategy == 1) &
          call this%env%log%fatal('time-stepping-strategy=1 requires use of laser-scan-path')
    end if

    !! Create the model.
    if (params%is_sublist('model')) then
      plist => params%sublist('model')
      call this%model%init(env, this%mesh, plist, stat, errmsg)
      if (stat /= 0) call this%env%log%fatal(errmsg)
    else
      call this%env%log%fatal('missing "model" sublist parameter')
    end if

    !! Create the solver.
    if (params%is_sublist('solver')) then
      plist => params%sublist('solver')
      call this%solver%init(this%env, this%model, plist)
      !if (stat /= 0) call this%env%log%fatal(errmsg)
    else
      call this%env%log%fatal('missing "solver" sublist parameter')
    end if


    !! Add laser scan path segment starts to the simulation event queue.
    !! Do this first to avoid times being adjusted to avoid small spacings.
    if (allocated(this%lscan)) then
      block
        real(r8), allocatable :: times(:)
        logical,  allocatable :: discont(:)
        call this%lscan%sp%get_segment_starts(times, discont)
        do j = 1, size(times)
          if (discont(j)) then
            call this%eventq%add_event(times(j), path_event(DT_POLICY_FACTOR, dt_factor), rank=99)
          else
            call this%eventq%add_event(times(j), path_event(DT_POLICY_NEXT), rank=99)
          end if
        end do
      end block
    end if

    !! Add the requested output times to the simulation event queue.
    call this%eventq%set_time_resolution(this%dt_min)
    do j = 1, size(this%tout)
      call this%eventq%add_event(this%tout(j), output_event())
    end do
    call this%eventq%add_event(this%tout(size(this%tout)), stop_event(), rank=98)

    this%ts_sync = time_step_sync(4)

    allocate(this%temp(0:this%mesh%max_level))
    call this%mesh%ml_multifab_build(this%temp, nc=1, ng=1) ! flow wants a ghost

    !! Generate the initial temperature field
    call start_timer('initial-state')
    if (params%is_sublist('initial-temperature')) then
      plist => params%sublist('initial-temperature')
      call alloc_scalar_func(f, plist, stat, errmsg)
      if (stat /= 0) call this%env%log%fatal(errmsg)
    else if (params%is_scalar('initial-temperature')) then
      call params%get('initial-temperature', const, stat=stat, errmsg=errmsg)
      if (stat /= 0) call this%env%log%fatal(errmsg)
      call alloc_const_scalar_func(f, const)
    else
      call this%env%log%fatal('missing valid "initial-temperature" parameter')
    end if
    call this%mesh%compute_mesh_func(f, this%temp)

    !! Define the initial heat conduction state
    call this%solver%set_initial_state(this%t_init, this%temp)
    if (allocated(this%lscan) .and. this%ts_strategy == 1) then
      select case (this%ts_strategy)
      case (1)
        this%explicit_ht = this%lscan%is_laser_on()
      case (2)
        this%explicit_ht = (this%solver%max_liq_frac() > 0.0_r8)
      case default
        INSIST(.false.)
      end select
    else
      this%explicit_ht = .false.
    end if
    call this%solver%start(this%dt_init, explicit_ht=this%explicit_ht)
    call stop_timer('initial-state')

    call stop_timer('initialization')

  end subroutine init


  subroutine run(this, stat, errmsg)

    class(pbf_sim), intent(inout) :: this
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    real(r8) :: t, hnext, tout, t_write
    character(80) :: string
    type(action_list), allocatable :: actions
    class(event_action), allocatable :: action
    logical :: laser_was_on, laser_now_on

    call start_timer('integration')

    t = this%solver%time()
    this%tlast = t
    hnext = merge(this%dt_explicit, this%dt_init, this%explicit_ht)
    this%hlast = hnext

    call this%env%log%info('')
    write(string,'(a,es12.5)') 'starting integration at T = ', t
    call this%env%log%info(string)

    call this%env%log%info('writing initial solution')
    call write_solution(this, t)
    t_write = t ! keep track of the last write time

    if (allocated(this%lscan)) then
      laser_was_on = this%lscan%is_laser_on()
      call this%env%log%info('laser is ' // merge('on ', 'off', laser_was_on))
    end if
    if (this%explicit_ht) then
      if (this%ts_strategy == 2) call this%env%log%info('liquid is present')
      call this%env%log%info('using explicit heat transfer integration')
    else
      if (this%ts_strategy == 2) call this%env%log%info('no liquid present')
      call this%env%log%info('using implicit heat transfer integration')
    end if

    call this%eventq%fast_forward(t)

    event_loop: do
      if (this%eventq%is_empty()) exit
      tout = this%eventq%next_time()
      call integrate(this, tout, hnext, t, stat, errmsg)

      if (stat < 0 .and. t == t_write) exit
      if (stat /= 0) then  ! write before quitting
        write(string,'(a,es12.5)') 'writing final solution at T = ', t
        call this%env%log%info('')
        call this%env%log%info(string)
        call write_solution(this, t)
        exit
      end if
      INSIST(t == tout)
      call this%eventq%pop_actions(actions)
      do
        call actions%get_next_action(action)
        if (.not.allocated(action)) exit
        select type (action)
        type is (output_event)
          call start_timer('output')
          write(string,'(a,es12.5)') 'writing solution at T = ', t
          call this%env%log%info('')
          call this%env%log%info(string)
          call write_solution(this, t)
          t_write = t ! keep track of the last write time
          call stop_timer('output')
        type is (path_event)
          call this%env%log%info('')
          call this%env%log%info('starting new laser scan path segment')
          laser_was_on = this%lscan%is_laser_on()
          call this%lscan%next_sp_segment(t)
          laser_now_on = this%lscan%is_laser_on()
          if (laser_was_on .neqv. laser_now_on) then ! switched on/off
            call this%env%log%info('laser switched ' // merge('on ', 'off', laser_now_on))
            if (this%ts_strategy == 1) then
              if (laser_now_on) then
                call this%env%log%info('switching to explicit integration')
                hnext = this%dt_explicit
              else
                call this%env%log%info('switching to implicit integration')
                hnext = this%dt_init
              end if
              call this%solver%start(hnext, explicit_ht=laser_now_on)
              this%explicit_ht = laser_now_on
            else if (.not.this%explicit_ht) then
              !! Continue implicit integration with possibly reduced step size
              hnext = action%init_dt(this%hlast, hnext)
            end if
          end if
        type is (stop_event)
          exit event_loop
        class default
          INSIST(.false.)
        end select
      end do
    end do event_loop

    if (stat > 0) then  ! caught a signal
      call this%env%log%info('')
      call this%env%log%info(errmsg // ': current solution written, and now terminating ...')
      stat = 0  ! this is a successful return
      deallocate(errmsg)
    else if (stat < 0) then
      call this%env%log%info('')
      errmsg = 'unrecoverable integration failure: ' // errmsg
      call this%env%log%info(errmsg)
    end if

    if (this%solver%has_exaca_data()) call write_exaca_data(this, tout)
    if (this%solver%has_gv_data()) call write_gv_data(this, tout)

    call this%env%log%info('')
    write(string,'(a,es12.5,a)') 'Completed integration to T = ', t
    call this%env%log%info(string)

    call stop_timer('integration')

  end subroutine run

  !! This integrates the system to the target time TOUT.  The final solution
  !! achieved is returned in T and U.  Nominally this will be at time TOUT,
  !! however it will be at some earlier time when there is a failure in the
  !! time stepping (or a user interrupt).  The input value of HNEXT is the
  !! initial step size to take, and its return value is the suggested next
  !! step size (nominally the value passed to the next call to INTEGRATE).
  !! STAT returns a negative value if a time stepping failure occurs, with
  !! an explanatory message in ERRMSG.  If the process receives the SIGURG
  !! signal, the procedure returns at the end of the next time step with
  !! a positive value of STAT, with T and U set accordingly.

  subroutine integrate(this, tout, hnext, t, stat, errmsg)

    use signal_handler, only: read_signal, SIGURG

    class(pbf_sim), intent(inout) :: this
    real(r8), intent(in) :: tout
    real(r8), intent(inout) :: hnext
    real(r8), intent(out) :: t
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    logical :: sig_rcvd, have_liq

    do

      !! Optional switching between implicit and explicit time integration
      !! depending on the presence of liquid.
      if (this%ts_strategy == 2) then
        have_liq = (this%solver%max_liq_frac() > 0.0_r8)
        if (have_liq .neqv. this%explicit_ht) then
          call this%env%log%info('')
          if (have_liq) then
            call this%env%log%info('liquid is present; switching to explicit integration')
          else
            call this%env%log%info('no liquid present; switching to implicit integration')
          end if
          this%explicit_ht = .not.this%explicit_ht
          if (this%explicit_ht) hnext = this%dt_explicit
          call this%solver%start(hnext, explicit_ht=this%explicit_ht)
        end if
      end if

      !! Time for next step; nominally TLAST+HNEXT but possibly adjusted
      t = this%ts_sync%next_time(tout, this%tlast, this%hlast, hnext)
      call step(this, t, hnext, stat, errmsg)
      if (stat /= 0) then
        t = this%tlast
        return
      end if
      call this%solver%write_metrics
      this%hlast = t - this%tlast
      this%tlast = t

      hnext = min(hnext, this%dt_max)
      if (this%explicit_ht) hnext = min(hnext, this%dt_explicit)

      call read_signal(SIGURG, sig_rcvd)  !FIXME: make parallel safe
      if (sig_rcvd) then
        stat = 1
        errmsg = 'received SIGURG signal'
        return
      end if

      if (t == tout) return
    end do

  end subroutine integrate

  !! Take a resilient step. Nominally this takes a single step from the current
  !! time to time T. However if that step fails, the procedure will re-attempt
  !! with successively smaller step sizes, until the step is successful or the
  !! number of attempts exceeds a maximum or the step size gets too small. Thus
  !! the return value of T may differ from its input value. HNEXT returns the
  !! suggested next time step. STAT returns a negative value if the step was
  !! ultimately unsuccessful, with an explanatory message in ERRMSG.

  subroutine step(this, t, hnext, stat, errmsg)

    class(pbf_sim), intent(inout) :: this
    real(r8), intent(inout) :: t
    real(r8), intent(out) :: hnext
    integer,  intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    integer :: n
    real(r8) :: tlast
    character(80) :: string

    tlast = this%solver%time()

    do n = 1, this%max_try
      write(string,'("STEP=",i0,", T=",g0.6,", H=",g0.4)') this%step, tlast, t-tlast
      call this%env%log%info('')
      call this%env%log%info(string)
      call this%solver%step(t, hnext, stat)
      if (stat == 0) then ! success
        call this%solver%commit_pending_state
        this%step = this%step + 1
        return
      end if
      call this%env%log%info('step failed; retrying with a reduced time step size')
      t = tlast + hnext
      if (t - tlast < this%dt_min) then
        stat = -1
        errmsg = 'next time step is too small'
        return
      end if
    end do

    stat = -2
    errmsg = 'unable to take a time step'

  end subroutine step

  subroutine write_solution(this, t)

    use amrex_base_module, only: amrex_string, amrex_string_build, amrex_write_plotfile

    class(pbf_sim), intent(inout) :: this
    real(r8), intent(in) :: t

    integer :: l
    type(amrex_multifab) :: plotmf(0:this%mesh%max_level)
    type(amrex_string) :: label(9)
    character(16) :: filename

    write(filename,'("plt",i4.4)') this%ndump

    call amrex_string_build(label(1), 'temp')
    call amrex_string_build(label(2), 'enthalpy')
    call amrex_string_build(label(3), 'solid')
    call amrex_string_build(label(4), 'liquid')
    call amrex_string_build(label(5), 'level-mask')

    if (this%solver%flow_enabled) then
      call amrex_string_build(label(6), 'velocity-x')
      call amrex_string_build(label(7), 'velocity-y')
      call amrex_string_build(label(8), 'velocity-z')
      call amrex_string_build(label(9), 'pressure')
      call this%mesh%ml_multifab_build(plotmf, nc=9, ng=0)
    else
      call this%mesh%ml_multifab_build(plotmf, nc=5, ng=0)
    end if

    ! Clear out the coarser multifabs, to ensure velocity & pressure only have
    ! nonzero values on the finest level.
    do l = 0, this%mesh%max_level-1
      call plotmf(l)%setval(0.0_r8)
    end do

    ! copy the variables into the plot multifab. Copy integer arguments are:
    ! source index, destination index, number of components, number of ghosts
    ! the indices populated with these copy commands need to be consistent with
    ! the number of components in the above multifab_build calls.
    call this%solver%get_temp(plotmf, 1)
    call this%solver%get_enth(plotmf, 2)
    call this%solver%get_vol_frac(plotmf, 3)
    if (this%solver%flow_enabled) then
      call this%solver%get_velocity(plotmf(this%mesh%max_level), 6)
      call this%solver%get_pressure(plotmf(this%mesh%max_level), 9)
    end if

    ! Some boundary-adjacent cells on coarse levels do not have correct values.
    ! The values in those cells aren't used in the simulation, but without
    ! reasonable values, they affect how Visit colors the output.
    call this%mesh%average_down(plotmf, 1, 4)

    call this%mesh%get_level_mask(plotmf, 5)

    call amrex_write_plotfile(trim(filename), this%mesh%max_level+1, plotmf, &
        label, this%mesh%geom, t, [1], this%mesh%ref_ratio)

    this%ndump = this%ndump + 1

  end subroutine write_solution

  subroutine write_exaca_data(this, t)

    use amrex_base_module, only: amrex_string, amrex_string_build, amrex_write_plotfile

    class(pbf_sim), intent(inout) :: this
    real(r8), intent(in) :: t

    type(amrex_multifab) :: plotmf
    type(amrex_string) :: label(3)

    call amrex_string_build(label(1), 'melting-time')
    call amrex_string_build(label(2), 'freezing-time')
    call amrex_string_build(label(3), 'cooling-rate')
    call this%mesh%level(this%mesh%max_level)%multifab_build(plotmf, nc=3, ng=0)
    call this%solver%get_exaca_data(plotmf, 1)

    call amrex_write_plotfile('plt-exaca',1,[plotmf],label,[this%mesh%geom(this%mesh%max_level)],&
        t,[1],[1])

    call write_exaca_file(this, plotmf)

  end subroutine write_exaca_data

  subroutine write_exaca_file(this, mf)

    use amrex_base_module

    class(pbf_sim), intent(in) :: this
    type(amrex_multifab), intent(in) :: mf

    integer :: lun, ix, iy, iz, pathlen, l
    character(256) :: fname
    character(128) :: path
    real(r8) :: a(3), xc(3)
    type(amrex_box) :: bx
    type(amrex_mfiter) :: mfi
    real(r8), pointer, contiguous :: dp(:,:,:,:)

    l = this%mesh%max_level

    call get_environment_variable("EXACA_FILE_DIR",path,pathlen)
    if (pathlen == 0) path = '.'
    write(fname,'(a,"/exaca.",i4.4,".dat")') trim(path), this%env%rank
    open(newunit=lun,file=fname,action='write',status='replace')

    write(lun, '(a)') '# x, y, z, tm, tl, cr'

    associate (geom => this%mesh%geom(l), dx => this%mesh%dx(:,l))
      call amrex_mfiter_build(mfi, mf)
      do while (mfi%next())
        bx = mfi%tilebox()
        a = geom%get_physical_location(bx%lo) - (bx%lo - 0.5_r8)*dx
        dp => mf%dataptr(mfi)
        do iz = bx%lo(3), bx%hi(3)
          xc(3) = a(3) + dx(3)*iz
          do iy = bx%lo(2), bx%hi(2)
            xc(2) = a(2) + dx(2)*iy
            do ix = bx%lo(1), bx%hi(1)
              xc(1) = a(1) + dx(1)*ix
              if (dp(ix,iy,iz,2) > dp(ix,iy,iz,1)) write(lun,'(*(es13.5,:,","))') &
                  this%exaca_csf*xc, this%exaca_tsf*dp(ix,iy,iz,1:2), &
                  dp(ix,iy,iz,3)/this%exaca_tsf
            end do
          end do
        end do
      end do
    end associate

    close(lun)

  end subroutine write_exaca_file

  subroutine write_gv_data(this, t)

    use amrex_base_module, only: amrex_string, amrex_string_build, amrex_write_plotfile

    class(pbf_sim), intent(inout) :: this
    real(r8), intent(in) :: t

    type(amrex_multifab) :: plotmf
    type(amrex_string) :: label(6)

    call amrex_string_build(label(1), 'G')
    call amrex_string_build(label(2), 'V')
    call amrex_string_build(label(3), 'ex')
    call amrex_string_build(label(4), 'ey')
    call amrex_string_build(label(5), 'ez')
    call amrex_string_build(label(6), 'duration')
    call this%mesh%level(this%mesh%max_level)%multifab_build(plotmf, nc=6, ng=0)
    call this%solver%get_gv_data(plotmf, 1)

    call amrex_write_plotfile('plt-gv',1,[plotmf],label,[this%mesh%geom],t,[1],[1])

  end subroutine write_gv_data

  !!!! PATH_EVENT TYPE BOUND PROCEDURES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  pure function path_event_init_dt(this, dt_last, dt_next) result(dt)
    class(path_event), intent(in) :: this
    real(r8), intent(in) :: dt_last, dt_next
    real(r8) :: dt
    select case (this%dt_policy)
    case (DT_POLICY_NONE, DT_POLICY_NEXT)
      dt = dt_next
    case (DT_POLICY_FACTOR)
      dt = this%c*dt_last
    case (DT_POLICY_VALUE)
      dt = this%c
    case default
      dt = 0.0_r8 ! should never be here
    end select
  end function path_event_init_dt

end module pbf_sim_type
