!!
!! HTPC_FV_MODEL_TYPE
!!
!! Zach Jibben <zjibben@lanl.gov>
!! January 2020
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module htpc_fv_model_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use htpc_model_class
  use amrex_mesh_type
  use amrex_base_module, only: amrex_multifab, amrex_mfiter
  use htpc_fv_box_model_type
  use htpc_fv_vector_type
  use ht_matl_class
  use scalar_func_class
  use timer_tree_type
  implicit none
  private

  !! A private type that aggregates the data needed to compute the residual.
  type :: box_var
    real(r8), pointer, contiguous :: kappa(:,:,:) => null()
  end type box_var

  type, extends(htpc_model), public :: htpc_fv_model
    private
    !! Inherits mat and qmf members from parent.
    type(amrex_mesh), pointer, public :: mesh => null() ! reference only -- not owned
    type(htpc_fv_box_model), allocatable, public :: box(:)
    type(amrex_multifab) :: hdmf, rhmf, kappamf
    type(box_var), allocatable :: bvar(:)
    class(scalar_func), allocatable :: src_func
    type(amrex_multifab) :: src
  contains
    procedure :: init
    procedure :: compute_residual
    procedure :: compute_cell_enth
    procedure, private :: compute_cell_temp
    procedure :: compute_udot
    procedure :: get_temp_grad
    procedure :: init_vector
  end type htpc_fv_model

contains

  subroutine init(this, mesh, mat, params, stat, errmsg)

    use scalar_func_table_type
    use parameter_list_type

    class(htpc_fv_model), intent(out) :: this
    type(amrex_mesh), intent(in), target :: mesh
    class(ht_matl), intent(in), target :: mat
    type(parameter_list), intent(inout)  :: params
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    integer :: n
    type(amrex_mfiter) :: mfi

    this%mesh => mesh
    this%mat  => mat

    allocate(this%box(mesh%nbox))
    do n = 1, size(this%box)
      call this%box(n)%init(mesh, mesh%box(n), params, stat, errmsg)
      if (stat /= 0) return
    end do

    !! Optional source function contribution
    if (params%is_parameter('source')) then
      call init_source(this, params, stat, errmsg)
      if (stat /= 0) return
      call mesh%multifab_build(this%src, nc=1, ng=0)
    end if

    call mesh%multifab_build(this%rhmf, nc=1, ng=0)
    call mesh%multifab_build(this%kappamf, nc=1, ng=1)
    call this%kappamf%setval(0.0_r8)  ! ensure 0 value on domain ghost cells

    !! Advected enthalpy as a source
    !TODO: fix enthalpy_advector so that it expects no ghosts
    call mesh%multifab_build(this%qmf, nc=1, ng=1)
    call this%qmf%setval(0.0_r8)

    allocate(this%bvar(mesh%nbox))
    call mesh%mfiter_build(mfi)
    n = 0
    do while (mfi%next())
      n = n + 1
      call get_component_ptr(this%kappamf%dataptr(mfi), 1, this%bvar(n)%kappa)
    end do

  contains

    subroutine get_component_ptr(dp, n, cdp)
      real(r8), pointer, contiguous, intent(in) :: dp(:,:,:,:)
      integer, intent(in) :: n
      real(r8), pointer, contiguous, intent(out) :: cdp(:,:,:)
      cdp(lbound(dp,1):,lbound(dp,2):,lbound(dp,3):) => dp(:,:,:,n)
    end subroutine get_component_ptr

  end subroutine init

  subroutine init_source(this, plist, stat, errmsg)

    use parameter_list_type
    use scalar_func_class
    use scalar_func_factories
    use truchas_scalar_func_table

    type(htpc_fv_model), intent(inout) :: this
    type(parameter_list), intent(inout) :: plist
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    type(parameter_list), pointer :: func_params
    character(:), allocatable :: fname, context
    real(r8) :: const

    context = 'processing ' // plist%name() // ': '
    if (plist%is_sublist('source')) then
      func_params => plist%sublist('source')
      call alloc_scalar_func(this%src_func, func_params, stat, errmsg)
      if (stat /= 0) then
        errmsg = 'invalid function parameter list: ' // errmsg
        return
      end if
    else if (plist%is_scalar('source')) then
#ifdef GNU_PR93762
      block
      character(:), allocatable :: dummy
      call plist%get('source', fname, stat=stat, errmsg=dummy)
      end block
#else
      call plist%get('source', fname, stat=stat)
#endif
      if (stat == 0) then ! name of a function
        call lookup_func(fname, this%src_func)
        if (.not.allocated(this%src_func)) then
          stat = 1
          errmsg = context // 'unknown function name: ' // fname
          return
        end if
      else  ! it must be a constant value
        call plist%get('source', const, stat=stat, errmsg=errmsg)
        if (stat /= 0) then
          errmsg = context // errmsg
          return
        end if
        call alloc_const_scalar_func(this%src_func, const)
      end if
    else
      stat = 1
      errmsg = context // 'missing valid "source" parameter'
      return
    end if

  end subroutine init_source

  !! differential part of the residual goes into the temperature component,
  !! algebraic part goes into the enthalpy component.

  subroutine compute_residual(this, t, u, udot, r)

    class(htpc_fv_model), intent(inout) :: this
    real(r8), intent(in) :: t
    type(htpc_fv_vector), intent(inout) :: u, udot, r

    integer :: n
    real(r8) :: vol

    call start_timer('htpc-residual')

    call this%mat%compute_kappa(u%tc, this%kappamf)

    do n = 1, size(this%box)
      associate (bv => this%bvar(n), uvar => u%box(n), rvar => r%box(n))
        ! Temperature uses ghost cells, but the residual doesn't need (or expect) them.
        call this%box(n)%compute_residual(t, bv%kappa, uvar%t, &
            rvar%t(rvar%lo(1):rvar%hi(1),rvar%lo(2):rvar%hi(2),rvar%lo(3):rvar%hi(3)))
      end associate
    end do

    !! Convective derivative contribution
    vol = product(this%mesh%geom%dx)
    call r%tc%saxpy(vol, udot%hc, 1, 1, 1, 0)
    call r%tc%saxpy(-vol, this%qmf, 1, 1, 1, 0)

    !! Optional source function contribution
    if (allocated(this%src_func)) then
      call this%mesh%compute_mesh_func(this%src_func, t, this%src)
      call r%tc%saxpy(-vol, this%src, 1, 1, 1, 0)
    end if

    !! Algebraic enthalpy-temperature relation residual
    call this%mat%compute_enthalpy_density(u%tc, this%rhmf)
    call r%hc%copy(u%hc, 1, 1, 1, 0)
    call r%hc%subtract(this%rhmf, 1, 1, 1, 0)

    call r%fill_boundary

    call stop_timer('htpc-residual')

  end subroutine compute_residual


  subroutine compute_cell_enth(this, u)
    class(htpc_fv_model), intent(inout) :: this
    type(htpc_fv_vector), intent(inout) :: u
    call this%mat%compute_enthalpy_density(u%tc, u%hc)
  end subroutine compute_cell_enth


  subroutine compute_cell_temp(this, u)
    class(htpc_fv_model), intent(inout) :: this
    type(htpc_fv_vector), intent(inout) :: u
    call this%mat%compute_temp(u%hc, u%tc)
    call u%tc%fill_boundary(this%mesh%geom)
  end subroutine compute_cell_temp


  subroutine compute_udot(this, t, dt, u, udot)

    class(htpc_fv_model), intent(inout) :: this
    real(r8), intent(in) :: t, dt
    type(htpc_fv_vector), intent(inout) :: u, udot

    integer :: n

    call start_timer('htpc-model-udot')

    !! Compute the cell enthalpy from the cell temps.
    call this%compute_cell_enth(u)
    call this%mat%compute_kappa(u%tc, this%kappamf)

    !! Compute the cell enthalpy time derivative.
    do n = 1, size(this%box)
      associate (box => this%box(n), bvar => this%bvar(n), uvar => u%box(n), udvar => udot%box(n))
        call box%compute_hdot(t, bvar%kappa, uvar%t, udvar%h)
      end associate
    end do

    !! Forward Euler time step (only cell enthalpy is advanced)
    call udot%update(1.0_r8, u, dt) ! v = u + dt*udot

    !! Compute the advanced cell temp from the advanced cell enthalpies.
    call this%compute_cell_temp(udot)

    !! Forward Euler approximation to the time derivative at T.
    !! udot = (v - u) / dt
    call udot%update(-1.0_r8, u)
    call udot%scale(1/dt)
    
    call stop_timer('htpc-model-udot')

  end subroutine compute_udot

  !! This subroutine returns a cell-based approximation to the temperature
  !! gradient in the multifab GRAD given the solution state U. This gradient
  !! does not play a role in the discretization of the heat transfer system
  !! and is intended only for analysis/diagnostic purposes. It is computed
  !! here as a simple difference of the temperatures.
  !! The motivation for implementing this function here is that we can use the
  !! The existing multifab temporaries for unpacking the face temperatures.

  subroutine get_temp_grad(this, u, grad)

    class(htpc_fv_model), intent(inout) :: this
    type(htpc_fv_vector), intent(inout) :: u
    type(amrex_multifab), intent(inout) :: grad

    integer :: n, ix, iy, iz
    real(r8), contiguous, pointer :: dataptr(:,:,:,:)
    type(amrex_mfiter) :: mfi

    call u%tc%fill_boundary(this%mesh%geom)
    call this%mesh%mfiter_build(mfi)
    n = 0
    do while (mfi%next())
      n = n + 1
      dataptr => grad%dataptr(mfi)
      associate (box => this%box(n), uvar => u%box(n))
        do iz = box%lo(3), box%hi(3)
          do iy = box%lo(2), box%hi(2)
            do ix = box%lo(1), box%hi(1)
              dataptr(ix,iy,iz,1) = (uvar%t(ix+1,iy,iz) - uvar%t(ix-1,iy,iz))/(2*box%dx(1))
              dataptr(ix,iy,iz,2) = (uvar%t(ix,iy+1,iz) - uvar%t(ix,iy-1,iz))/(2*box%dx(2))
              dataptr(ix,iy,iz,3) = (uvar%t(ix,iy,iz+1) - uvar%t(ix,iy,iz-1))/(2*box%dx(3))
            end do
          end do
        end do
      end associate
    end do

  end subroutine get_temp_grad


  subroutine init_vector(this, vec)
    class(htpc_fv_model), intent(in) :: this
    type(htpc_fv_vector), intent(out) :: vec
    call vec%init(this%mesh)
  end subroutine

end module htpc_fv_model_type
