!!
!! HTPC_FV_VECTOR_TYPE
!!
!! This is a vector type for holding the HTPC_FV model's data, for a single
!! level. Arrays of this type are formed to hold multi-level data.
!!
!! Zach Jibben <zjibben@lanl.gov>
!! April 2020
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! CLASS DATA COMPONENTS
!!
!!  The class has the following AMREX_MULTIFAB components
!!    HC -- cell centered enthalpies
!!    TC -- cell centered temperatures (1 layer of ghost cells)
!!
!! CLASS METHODS
!!
!!  See vector_class.F90 for a description of the base class methods. The
!!  additional methods for this implementation of the base class follow.
!!
!!  TYPE(HTPC_FV_VECTOR) :: V
!!
!!  CALL V%INIT(MESH) initializes the vector by creating the multifab
!!  components corresponding to the given level MESH. The multifabs are
!!  initialized to 0.
!!
!!  CALL V%INIT(MOLD) makes V a clone of the HTPC_FV_VECTOR variable MOLD.
!!  The multifabs are initialized to 0.
!!
!!  CALL V%FILL_BOUNDARY() sets the values of ghost elements to the value
!!  of the element they duplicate.
!!

#include "f90_assert.fpp"

module htpc_fv_vector_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use vector_class
  use amrex_mesh_type
  use amrex_base_module
  implicit none
  private

  public :: mf_max_bal_norm

  type :: fv_vector_box_data
    integer :: lo(3), hi(3)
    real(r8), pointer, contiguous :: t(:,:,:) => null(), h(:,:,:) => null()
  end type fv_vector_box_data

  type, extends(vector), public :: htpc_fv_vector
    type(amrex_multifab) :: tc, hc
    type(amrex_mesh), pointer :: mesh => null() ! reference only
    type(fv_vector_box_data), allocatable :: box(:)
  contains
    !! Deferred base class procedures
    procedure :: clone1
    procedure :: clone2
    procedure :: copy_
    procedure :: setval
    procedure :: scale
    procedure :: update1_
    procedure :: update2_
    procedure :: update3_
    procedure :: update4_
    procedure :: dot_
    procedure :: norm2
    procedure :: checksum
    !! Additional procedures specific to this type
    generic :: init => init_mesh, init_mold
    procedure, private :: init_mesh, init_mold
    procedure :: fill_boundary
    procedure :: link_box_data
  end type htpc_fv_vector

contains

  subroutine clone1(this, clone)
    class(htpc_fv_vector), intent(in) :: this
    class(vector), allocatable, intent(out) :: clone
    type(htpc_fv_vector), allocatable :: tmp
    allocate(tmp)
    call multifab_clone(this%tc, tmp%tc)
    call multifab_clone(this%hc, tmp%hc)
    call link_box_data(tmp)
    tmp%mesh => this%mesh
    call move_alloc(tmp, clone)
  end subroutine

  subroutine clone2(this, clone, n)
    class(htpc_fv_vector), intent(in) :: this
    class(vector), allocatable, intent(out) :: clone(:)
    integer, intent(in) :: n
    integer :: j
    type(htpc_fv_vector), allocatable :: tmp(:)
    allocate(tmp(n))
    do j = 1, n
      call multifab_clone(this%tc, tmp(j)%tc)
      call multifab_clone(this%hc, tmp(j)%hc)
      tmp(j)%mesh => this%mesh
      call link_box_data(tmp(j))
    end do
    call move_alloc(tmp, clone)
  end subroutine

  !! This auxiliary subroutine clones a multifab.
  !! It ought to be a type bound subroutine of the amrex_multifab type.

  subroutine multifab_clone(mold, copy)
    type(amrex_multifab), intent(in) :: mold
    type(amrex_multifab), intent(inout) :: copy
    call amrex_multifab_build(copy, mold%ba, mold%dm, mold%ncomp(), mold%nghost(), mold%nodal_type())
  end subroutine

  subroutine copy_(dest, src)
    class(htpc_fv_vector), intent(inout) :: dest
    class(vector), intent(in) :: src
    select type (src)
    class is (htpc_fv_vector)
      call dest%tc%copy(src%tc, 1, 1, 1, 1)
      call dest%hc%copy(src%hc, 1, 1, 1, 0)
    end select
  end subroutine

  subroutine setval(this, val)
    class(htpc_fv_vector), intent(inout) :: this
    real(r8), intent(in) :: val
    call this%hc%setval(val)
    call this%tc%setval(val)
  end subroutine

  subroutine scale(this, a)
    class(htpc_fv_vector), intent(inout) :: this
    real(r8), intent(in) :: a
    call this%tc%mult(a, 1, 1, 1)
    call this%hc%mult(a, 1, 1, 0)
  end subroutine scale

  !! Conventional SAXPY procedure: y <-- a*x + y
  subroutine update1_(this, a, x)
    class(htpc_fv_vector), intent(inout) :: this
    class(vector), intent(in) :: x
    real(r8), intent(in) :: a
    select type (x)
    class is (htpc_fv_vector)
      call this%tc%saxpy(a, x%tc, 1, 1, 1, 1)
      call this%hc%saxpy(a, x%hc, 1, 1, 1, 0)
    end select
  end subroutine update1_

  !! SAXPY-like procedure: y <-- a*x + b*y
  subroutine update2_(this, a, x, b)
    class(htpc_fv_vector), intent(inout) :: this
    class(vector), intent(in) :: x
    real(r8), intent(in) :: a, b
    select type (x)
    class is (htpc_fv_vector)
      call mf_update(this%tc, a, x%tc, b, ng=1)
      call mf_update(this%hc, a, x%hc, b, ng=0)
    end select
  contains
    subroutine mf_update(mf, a, x, b, ng)
      type(amrex_multifab), intent(inout) :: mf
      type(amrex_multifab), intent(in) :: x
      real(r8), intent(in) :: a, b
      integer, intent(in) :: ng
      call mf%mult(b, 1, 1, ng)
      call mf%saxpy(a, x, 1, 1, 1, ng)
    end subroutine mf_update
  end subroutine update2_

  !! SAXPY-like procedure: z <-- a*x + b*y + z
  subroutine update3_(this, a, x, b, y)
    class(htpc_fv_vector), intent(inout) :: this
    class(vector), intent(in) :: x, y
    real(r8), intent(in) :: a, b
    select type (x)
    class is (htpc_fv_vector)
      select type (y)
      class is (htpc_fv_vector)
        call mf_update(this%tc, a, x%tc, b, y%tc, ng=1)
        call mf_update(this%hc, a, x%hc, b, y%hc, ng=0)
      end select
    end select
  contains
    subroutine mf_update(mf, a, x, b, y, ng)
      type(amrex_multifab), intent(inout) :: mf
      type(amrex_multifab), intent(in) :: x, y
      real(r8), intent(in) :: a, b
      integer, intent(in) :: ng
      call mf%saxpy(a, x, 1, 1, 1, ng)
      call mf%saxpy(b, y, 1, 1, 1, ng)
    end subroutine mf_update
  end subroutine update3_

  !! SAXPY-like procedure: z <-- a*x + b*y + c*z
  subroutine update4_(this, a, x, b, y, c)
    class(htpc_fv_vector), intent(inout) :: this
    class(vector), intent(in) :: x, y
    real(r8), intent(in) :: a, b, c
    select type (x)
    class is (htpc_fv_vector)
      select type (y)
      class is (htpc_fv_vector)
        call mf_update(this%tc, a, x%tc, b, y%tc, c, ng=1)
        call mf_update(this%hc, a, x%hc, b, y%hc, c, ng=0)
      end select
    end select
  contains
    subroutine mf_update(mf, a, x, b, y, c, ng)
      type(amrex_multifab), intent(inout) :: mf
      type(amrex_multifab), intent(in) :: x, y
      real(r8), intent(in) :: a, b, c
      integer, intent(in) :: ng
      call mf%mult(c, 1, 1, ng)
      call mf%saxpy(a, x, 1, 1, 1, ng)
      call mf%saxpy(b, y, 1, 1, 1, ng)
    end subroutine mf_update
  end subroutine update4_

  real(r8) function dot_(x, y)
    class(htpc_fv_vector), intent(in) :: x
    class(vector), intent(in) :: y
    select type (y)
    class is (htpc_fv_vector)
      dot_ = mf_dp(x%tc, y%tc) + mf_dp(x%hc, y%hc)
    end select
  end function dot_

  !! NB: As of this writing, AMReX does not expose the multifab dot
  !! operator in the Fortran interface.

  real(r8) function mf_dp(a, b)

    type(amrex_multifab), intent(in) :: a, b

    integer :: i, j, k
    type(amrex_mfiter) :: mfi
    type(amrex_box) :: bx
    real(r8), contiguous, pointer :: aptr(:,:,:,:), bptr(:,:,:,:)

    INSIST(count(a%nodal_type()) == 0) ! assuming only cell-centered fields

    call amrex_mfiter_build(mfi, a)
    mf_dp = 0
    do while (mfi%next())
      bx = mfi%tilebox()
      aptr => a%dataptr(mfi)
      bptr => b%dataptr(mfi)
      do k = bx%lo(3), bx%hi(3)
        do j = bx%lo(2), bx%hi(2)
          do i = bx%lo(1), bx%hi(1)
            mf_dp = mf_dp + aptr(i,j,k,1)*bptr(i,j,k,1)
          end do
        end do
      end do
    end do
    call amrex_parallel_reduce_sum(mf_dp)

  end function mf_dp

  real(r8) function norm2(this)
    class(htpc_fv_vector), intent(in) :: this
    norm2 = sqrt(this%tc%norm2()**2 + this%hc%norm2()**2)
  end function norm2


  function checksum(this, full) result(string)
    use md5_hash_type
    class(htpc_fv_vector), intent(in) :: this
    logical, intent(in), optional :: full
    character(:), allocatable :: string
    type(md5_hash) :: hash
    logical :: strict
    strict = .true.
    if (present(full)) strict = .not.full
    if (strict) then
      call mf_hash_update_strict(hash, this%tc)
      call mf_hash_update_strict(hash, this%hc)
    else
      call mf_hash_update(hash, this%tc)
      call mf_hash_update(hash, this%hc)
    end if
    string = hash%hexdigest()
  end function checksum

  !! This auxiliary subroutine updates the hash with data from the
  !! passed multifab. All box values, including ghosts are included.

  subroutine mf_hash_update(hash, mf)
    use md5_hash_type
    type(md5_hash), intent(inout) :: hash
    type(amrex_multifab), intent(in) :: mf
    type(amrex_mfiter) :: mfi
    real(r8), pointer, contiguous :: dptr(:,:,:,:)
    call amrex_mfiter_build(mfi, mf)
    do while (mfi%next())
      dptr => mf%dataptr(mfi)
      call hash%update(dptr(:,:,:,1))
    end do
  end subroutine mf_hash_update

  !! This auxiliary subroutine updates the hash with data from the passed
  !! multifab. Ghost values, if any, are skipped. NB: This only works for
  !! cell-centered multifabs.

  subroutine mf_hash_update_strict(hash, mf)

    use secure_hash_class

    class(secure_hash), intent(inout) :: hash
    type(amrex_multifab), intent(in) :: mf

    integer :: i, j, k
    type(amrex_mfiter) :: mfi
    type(amrex_box) :: bx
    real(r8), pointer, contiguous :: dptr(:,:,:,:)

    call amrex_mfiter_build(mfi, mf)
    do while (mfi%next())
      bx = mfi%tilebox()
      dptr => mf%dataptr(mfi)
      do k = bx%lo(3), bx%hi(3)
        do j = bx%lo(2), bx%hi(2)
          do i = bx%lo(1), bx%hi(1)
            call hash%update(dptr(i,j,k,1))
          end do
        end do
      end do
    end do

  end subroutine mf_hash_update_strict

  !! Specific subroutine for the generic INIT. Initialize a HTPC_FV_VECTOR
  !! object for the given level MESH. The multifabs are initialized to 0.

  subroutine init_mesh(this, mesh)
    class(htpc_fv_vector), intent(out) :: this
    type(amrex_mesh), intent(in), target :: mesh
    this%mesh => mesh
    call mesh%multifab_build(this%hc, nc=1, ng=0)
    call mesh%multifab_build(this%tc, nc=1, ng=1)
    call setval(this, 0.0_r8)
    call link_box_data(this)
  end subroutine init_mesh

  !! Specific subroutine for the generic INIT. Initialize a HTPC_FV_VECTOR
  !! object to be a clone of MOLD. The multifabs are initialized to zero.

  subroutine init_mold(this, mold)
    class(htpc_fv_vector), intent(out) :: this
    class(htpc_fv_vector), intent(in)  :: mold
    call init_mesh(this, mold%mesh)
  end subroutine init_mold


  subroutine link_box_data(this)

    class(htpc_fv_vector), intent(inout) :: this

    integer :: n
    type(amrex_mfiter) :: mfi
    type(amrex_box) :: bx

    call amrex_mfiter_build(mfi, this%tc)
    n = 0
    do while (mfi%next())
      n = n + 1
    end do

    if (allocated(this%box)) deallocate(this%box)
    allocate(this%box(n))

    call amrex_mfiter_build(mfi, this%tc)
    n = 0
    do while (mfi%next())
      n = n + 1
      bx = mfi%tilebox()
      call get_component_ptr(this%tc%dataptr(mfi), 1, this%box(n)%t)
      call get_component_ptr(this%hc%dataptr(mfi), 1, this%box(n)%h)
      this%box(n)%lo = bx%lo
      this%box(n)%hi = bx%hi
    end do

  contains

    subroutine get_component_ptr(dp, n, cdp)
      real(r8), pointer, contiguous, intent(in) :: dp(:,:,:,:)
      integer, intent(in) :: n
      real(r8), pointer, contiguous, intent(out) :: cdp(:,:,:)
      cdp(lbound(dp,1):,lbound(dp,2):,lbound(dp,3):) => dp(:,:,:,n)
    end subroutine get_component_ptr

  end subroutine link_box_data

  !! Sync the multifab ghost values.
  subroutine fill_boundary(this)
    class(htpc_fv_vector), intent(inout) :: this
    call this%tc%fill_boundary(this%mesh%geom)
  end subroutine

  real(r8) function mf_max_bal_norm(u, du, atol, rtol)

    type(amrex_multifab), intent(in) :: u, du
    real(r8), intent(in) :: atol, rtol

    integer :: i, j, k
    type(amrex_mfiter) :: mfi
    type(amrex_box) :: bx
    real(r8), contiguous, pointer :: uptr(:,:,:,:), duptr(:,:,:,:)
    real(r8) :: s

    INSIST(count(du%ba%nodal_type()) == 0) ! assuming only cell-centered fields
    
    s = 0
    call amrex_mfiter_build(mfi, du)
    do while (mfi%next())
      bx = mfi%tilebox()
      uptr => u%dataptr(mfi)
      duptr => du%dataptr(mfi)
      do k = bx%lo(3), bx%hi(3)
        do j = bx%lo(2), bx%hi(2)
          do i = bx%lo(1), bx%hi(1)
            s = max(s, abs(duptr(i,j,k,1))/(atol + rtol*abs(uptr(i,j,k,1))))
          end do
        end do
      end do
    end do
    call amrex_parallel_reduce_max(s)
    mf_max_bal_norm = s

  end function mf_max_bal_norm

end module htpc_fv_vector_type
