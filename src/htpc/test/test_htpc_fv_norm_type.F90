!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program test_htpc_fv_norm_type

#ifdef NAGFOR
  use,intrinsic :: f90_unix, only: exit
#endif
  use,intrinsic :: iso_fortran_env, only: output_unit
  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use mpi
  use amrex_base_module
  use parameter_list_type
  use truchas_amrex_init_proc
  use amrex_mesh_type
  use material_model_type
  use htpc_fv_vector_type
  use htpc_fv_model_type
  use htpc_fv_norm_type
  use scalar_func_table_type
  use phase_property_table_factory
  use simlog_type
  implicit none

  integer :: j, nproc, this_rank, ierr, status = 0
  type(parameter_list) :: params, ppt_params
  type(parameter_list), pointer :: plist
  type(simlog) :: log
  type(amrex_mesh) :: mesh
  type(scalar_func_table) :: ppt
#if defined(__GFORTRAN__)
  procedure(amrex_finalize), pointer :: pp
#endif

  call truchas_amrex_init(params)

#if defined(__GFORTRAN__)
  pp => amrex_finalize
  call log%init(MPI_COMM_WORLD, [output_unit], verbosity=VERB_NOISY, finalize=pp)
#else
  call log%init(MPI_COMM_WORLD, [output_unit], verbosity=VERB_NOISY, finalize=amrex_finalize)
#endif

  call MPI_Comm_rank(MPI_COMM_WORLD, this_rank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)

  call params%set('lo', [1,1,1])
  call params%set('hi', [4,4,4])
  call params%set('box-size', 2)
  call params%set('prob-lo', [0.0_r8, 0.0_r8, 0.0_r8])
  call params%set('prob-hi', [1.0_r8, 1.0_r8, 1.0_r8])
  call mesh%init(params)

  do j = 0, nproc-1
    if (j == this_rank) then
      write(*,'("process ",i0," has ",i0," boxes")') j, mesh%nbox
    endif
    call MPI_Barrier(MPI_COMM_WORLD, ierr)
  end do

  !! A common phase property table to use when creating models in the tests
  plist => ppt_params%sublist('stuff')
  call plist%set('conductivity', 1.0_r8)
  call plist%set('density', 1.0_r8)
  call plist%set('specific-heat', 1.0_r8)
  call init_phase_property_table(ppt_params, log, ppt)

  call test_abs
  call test_rel
  call test_mixed
  call test_global

  call amrex_finalize()
  call exit(status)

contains

  subroutine test_abs

    type(material_model) :: mat
    type(htpc_fv_model) :: model
    type(htpc_fv_norm)  :: norm
    type(parameter_list) :: params
    type(parameter_list), pointer :: bc_params

    integer :: stat
    type(htpc_fv_vector) :: u, du
    real(r8) :: du_norm, error
    character(:), allocatable :: errmsg

    call set_material_params(params)
    call mat%init(mesh, ppt, params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    bc_params => params%sublist('bc') ! can be empty for now
    call model%init(mesh, mat, params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    call params%set('temp-abs-tol', 0.5_r8)
    call params%set('enth-abs-tol', 0.5_r8)
    call norm%init(params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    !! All values set to 0
    call model%init_vector(u)
    call model%init_vector(du)

    call u%setval(-10.0_r8)
    call du%setval(-0.5_r8)
    call norm%compute(u, du, du_norm)

    call MPI_Allreduce(abs(du_norm-1.0_r8), error, 1, MPI_REAL8, MPI_MAX, MPI_COMM_WORLD, ierr)
    if (this_rank == 0) &
        write(output_unit,'(2a)') merge('pass','fail',error==0.0_r8), ': testing atol'
    if (error /= 0.0_r8) status = 1

  end subroutine test_abs

  subroutine test_rel

    type(material_model) :: mat
    type(htpc_fv_model) :: model
    type(htpc_fv_norm)  :: norm
    type(parameter_list) :: params
    type(parameter_list), pointer :: bc_params

    integer :: stat
    type(htpc_fv_vector) :: u, du
    real(r8) :: du_norm, error
    character(:), allocatable :: errmsg

    call set_material_params(params)
    call mat%init(mesh, ppt, params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    bc_params => params%sublist('bc') ! can be empty for now
    call model%init(mesh, mat, params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    call params%set('temp-rel-tol', 0.25_r8)
    !call params%set('enth-rel-tol', 0.25_r8) ! default is temp-rel-tol
    call norm%init(params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    !! All values set to 0
    call model%init_vector(u)
    call model%init_vector(du)

    call u%setval(-2.0_r8)
    call du%setval(-1.0_r8)
    call norm%compute(u, du, du_norm)

    call MPI_Allreduce(abs(du_norm-2.0_r8), error, 1, MPI_REAL8, MPI_MAX, MPI_COMM_WORLD, ierr)
    if (this_rank == 0) &
        write(output_unit,'(2a)') merge('pass','fail',error==0.0_r8), ': testing rtol'
    if (error /= 0.0_r8) status = 1

  end subroutine test_rel

  subroutine test_mixed

    type(material_model) :: mat
    type(htpc_fv_model) :: model
    type(htpc_fv_norm)  :: norm
    type(parameter_list) :: params
    type(parameter_list), pointer :: bc_params

    integer :: stat
    type(htpc_fv_vector) :: u, du
    real(r8) :: du_norm, error
    character(:), allocatable :: errmsg

    call set_material_params(params)
    call mat%init(mesh, ppt, params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    bc_params => params%sublist('bc') ! can be empty for now
    call model%init(mesh, mat, params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    call params%set('temp-abs-tol', 1.0_r8)
    call params%set('temp-rel-tol', 2.0_r8)
    call params%set('enth-abs-tol', 1.0_r8)
    call params%set('enth-rel-tol', 2.0_r8)
    call norm%init(params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    !! All values set to 0
    call model%init_vector(u)
    call model%init_vector(du)

    call u%setval(-1.5_r8)
    call du%setval(-4.0_r8)
    call norm%compute(u, du, du_norm)

    call MPI_Allreduce(abs(du_norm-1.0_r8), error, 1, MPI_REAL8, MPI_MAX, MPI_COMM_WORLD, ierr)
    if (this_rank == 0) &
        write(output_unit,'(2a)') merge('pass','fail',error==0.0_r8), ': testing mixed tol'
    if (error /= 0.0_r8) status = 1

  end subroutine test_mixed

  subroutine test_global

    type(material_model) :: mat
    type(htpc_fv_model) :: model
    type(htpc_fv_norm)  :: norm
    type(parameter_list) :: params
    type(parameter_list), pointer :: bc_params

    integer :: stat
    logical :: pass
    type(htpc_fv_vector) :: u, du
    real(r8) :: du_norm, maxnorm, minnorm
    character(:), allocatable :: errmsg

    call set_material_params(params)
    call mat%init(mesh, ppt, params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    bc_params => params%sublist('bc') ! can be empty for now
    call model%init(mesh, mat, params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    call params%set('temp-abs-tol', 1.0_r8)
    call params%set('enth-abs-tol', 1.0_r8)
    call norm%init(params, stat, errmsg)
    if (stat /= 0) then
      status = 1
      if (this_rank == 0) write(output_unit,'(a)') errmsg
      return
    end if

    !! All values set to 0
    call model%init_vector(u)
    call model%init_vector(du)

    call u%setval(-1.5_r8)
    call du%setval(real(this_rank,kind=r8))
    call norm%compute(u, du, du_norm)

    call MPI_Allreduce(du_norm, maxnorm, 1, MPI_REAL8, MPI_MAX, MPI_COMM_WORLD, ierr)
    call MPI_Allreduce(du_norm, minnorm, 1, MPI_REAL8, MPI_MIN, MPI_COMM_WORLD, ierr)

    pass = (minnorm == maxnorm)
    if (this_rank == 0) write(output_unit,'(2a)') &
        merge('pass','fail',pass), ': got common norm value'
    if (pass) then
      pass = (du_norm == nproc-1)
      if (this_rank == 0) write(output_unit,'(2a,es10.3)') &
          merge('pass','fail',pass), ': du_norm =', du_norm
      if (.not.pass) status = 1
    else
      status = 1
    end if

  end subroutine test_global

  subroutine set_material_params(params)
    type(parameter_list), intent(inout) :: params
    call params%set('solid', 'stuff')
    call params%set('liquid', 'stuff')
    call params%set('solidus-temp', 100.0_r8)
    call params%set('liquidus-temp', 110.0_r8)
    call params%set('latent-heat', 0.0_r8)
    call params%set('smoothing-radius', 1.0_r8)
  end subroutine set_material_params

end program test_htpc_fv_norm_type
