!!
!! USTRUC_CORE_TYPE
!!
!! A concrete implementation the abstract base class USTRUC_CORE.  This
!! implementation defines the non-optional core microstructure analysis
!! component that is referenced by the optional analysis components.
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! August 2014
!!
!! PROGRAMMING INTERFACE
!!
!!  See the comments accompanying the source of the abstract base class
!!  USTRUC_COMP.  The design of low-level microstructure model is implemented
!!  using the decorator programming pattern, whose "concrete component" class
!!  corresponds to this derived type.  All of the optional components will
!!  contain a reference to an instance of this type through which they can
!!  access the common state variables which are stored as array components.
!!
!!  An object of this type should be instantiated using the function
!!  NEW_USTRUC_CORE(N) which returns a pointer to a new TYPE(USTRUC_CORE)
!!  object with state array components allocated for N independent points.
!!  The content of the arrays is initialized by a subsequent call to
!!  SET_STATE.
!!
!!  Objects of this type respond to the following data names in the generic
!!  GET subroutine: 'temp', 'temp-grad', 'frac', 'frac-grad', and 'invalid'.
!!  These correspond exactly to the data passed in the SET_STATE and
!!  UPDATE_STATE calls.  They also respond to 'frac-rate' which returns the
!!  time rate of change of solid fraction, which is computed using a simple
!!  backward time difference between successive states.  Before the first
!!  state update, it returns a dummy value of 0.
!!
!! NOTES
!!
!! The design of low-level microstructure model is implemented using the
!! decorator programming pattern, whose "concrete component" class corresponds
!! to this derived type.
!!
!! All of the specific procedures for the GET method are defined.  If at this
!! point (the end of the chain of analysis components) the specified data name
!! is not recognized a fatal error message is written and execution is halted.
!!
!! The internal state includes some arrays that are managed by one of the
!! optional analysis components that computes solidification velocities.
!! This is rather funky, and in retrospect perhaps not such a great idea.
!! The idea is that we want to be able to have different ways to compute
!! this velocity, and treating them as "decorators" was a really simple way
!! to do this.  But because that data is needed by other analysis components
!! it needed to be stored with the core component rather than private to the
!! decorator as would be normal.  It might be better to incorporate an
!! option-driven velocity computation directly into this core component.
!! As it stands now, we must be careful to include the velocity component at
!! the appropriate position in the chain of components (next to last) whenever
!! velocity data is required.
!!

#include "f90_assert.fpp"

module ustruc_core_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use ustruc_comp_class
  implicit none
  private

  public :: new_ustruc_core

  type, extends(ustruc_comp), public :: ustruc_core
    real(r8) :: t = 0.0_r8
    real(r8), allocatable :: temp(:)
    real(r8), allocatable :: temp_grad(:,:)
    real(r8), allocatable :: temp_rate(:)
    real(r8), allocatable :: frac(:)
  contains
    procedure, private :: init
    procedure :: set_state
    procedure :: update_state
    procedure :: get_comp_list
    procedure :: has
    procedure :: getl1
    procedure :: geti1
    procedure :: getr1
    procedure :: getr2
    procedure :: state_size
    procedure :: serialize
    procedure :: deserialize
  end type ustruc_core

contains

  function new_ustruc_core(n) result(this)
    integer, intent(in) :: n
    type(ustruc_core), pointer :: this
    allocate(this)
    call this%init(n)
  end function

  subroutine init(this, n)
    integer, intent(in) :: n
    class(ustruc_core), intent(out) :: this
    ASSERT(n >= 0)
    this%n = n
    allocate(this%temp(n), this%temp_grad(3,n), this%temp_rate(n), this%frac(n))
  end subroutine init

  subroutine set_state(this, t, temp, temp_grad, frac)

    class(ustruc_core), intent(inout) :: this
    real(r8), intent(in) :: t, temp(:), temp_grad(:,:), frac(:)

    ASSERT(size(temp) == this%n)
    ASSERT(size(temp_grad,1) == 3)
    ASSERT(size(temp_grad,2) == size(temp))
    ASSERT(size(frac) == this%n)

    this%t = t
    this%temp = temp
    this%temp_grad = temp_grad
    this%temp_rate = 0.0_r8 ! no valid data here
    this%frac = frac

  end subroutine set_state

  subroutine update_state(this, t, temp, temp_grad, frac)

    class(ustruc_core), intent(inout) :: this
    real(r8), intent(in) :: t, temp(:), temp_grad(:,:), frac(:)

    real(r8) :: dt

    ASSERT(size(temp) == this%n)
    ASSERT(size(temp_grad,1) == 3)
    ASSERT(size(temp_grad,2) == size(temp))
    ASSERT(size(frac) == this%n)

    dt = t - this%t
    this%t = t

    this%temp_rate = (temp - this%temp)/dt
    this%temp = temp
    this%temp_grad = temp_grad
    this%frac = frac

  end subroutine update_state

  subroutine get_comp_list(this, list)
    class(ustruc_core), intent(in) :: this
    integer, allocatable, intent(out) :: list(:)
    list = [0]
  end subroutine get_comp_list

  logical function has(this, name)
    class(ustruc_core), intent(in) :: this
    character(*), intent(in) :: name
    select case (name)
!TODO: everything but %frac is dummy data
!    case ('temp', 'temp-rate', 'temp-grad', 'frac')
!      has = .true.
    case default
      has = .false.
    end select
  end function has

  subroutine getl1(this, name, array)
    class(ustruc_core), intent(in) :: this
    character(*), intent(in) :: name
    logical, intent(out) :: array(:)
    select case (name)
    case default
      ASSERT(.false.)
      !call TLS_fatal ('USTRUCT_COMP%GET: unknown name: ' // name)
    end select
  end subroutine getl1

  subroutine geti1(this, name, array, invalid)
    class(ustruc_core), intent(in) :: this
    character(*), intent(in) :: name
    integer, intent(out) :: array(:)
    logical, intent(out), optional :: invalid(:)
    ASSERT(.false.)
    !call TLS_fatal ('USTRUCT_COMP%GET: unknown name: ' // name)
  end subroutine geti1

  subroutine getr1(this, name, array, invalid)
    class(ustruc_core), intent(in) :: this
    character(*), intent(in) :: name
    real(r8), intent(out) :: array(:)
    logical, intent(out), optional :: invalid(:)
    select case (name)
!    case ('temp')
!      ASSERT(size(array) == this%n)
!      array = this%temp
!      if (present(invalid)) invalid = .false.
!    case ('temp-rate')
!      ASSERT(size(array) == this%n)
!      array = this%temp_rate
!      if (present(invalid)) invalid = .false.
!    case ('frac')
!      ASSERT(size(array) == this%n)
!      array = this%frac
!      if (present(invalid)) invalid = .false.
!      end if
    case default
      ASSERT(.false.)
      !call TLS_fatal ('USTRUCT_COMP%GET: unknown name: ' // name)
    end select
  end subroutine getr1

  subroutine getr2(this, name, array, invalid)
    class(ustruc_core), intent(in) :: this
    character(*), intent(in) :: name
    real(r8), intent(out) :: array(:,:)
    logical, intent(out), optional :: invalid(:)
    integer :: j
    select case (name)
!    case ('temp-grad')
!      ASSERT(all(shape(array) == shape(this%temp_grad)))
!      array = this%temp_grad
!      if (present(invalid)) invalid = .false.
    case default
      ASSERT(.false.)
      !call TLS_fatal ('USTRUCT_COMP%GET: unknown name: ' // name)
    end select
  end subroutine getr2

  function state_size(this, cid) result(nbyte)
    class(ustruc_core), intent(in) :: this
    integer, intent(in) :: cid
    integer :: nbyte
    nbyte = 0
  end function

  subroutine serialize(this, cid, array)
    use,intrinsic :: iso_fortran_env, only: int8
    class(ustruc_core), intent(in) :: this
    integer, intent(in) :: cid
    integer(int8), intent(out) :: array(:,:)
  end subroutine

  subroutine deserialize(this, cid, array, mask)
    use,intrinsic :: iso_fortran_env, only: int8
    class(ustruc_core), intent(inout) :: this
    integer, intent(in) :: cid
    integer(int8), intent(in) :: array(:,:)
    logical, intent(in), optional :: mask(:)
  end subroutine

end module ustruc_core_type
