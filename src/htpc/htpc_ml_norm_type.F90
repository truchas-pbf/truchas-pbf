!!
!! HTPC_ML_NORM_TYPE 
!!
!! This module defines a derived type that encapsulates the norm used for
!! solution increments of the heat transfer/phase change model, finite volume
!! version.
!!
!! Zach Jibben <zjibben@lanl.gov>
!! June 2020
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module htpc_ml_norm_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use htpc_ml_vector_type
  implicit none
  private

  type, public :: htpc_ml_norm
    private
    real(r8) :: temp_atol ! absolute temperature tolerance
    real(r8) :: temp_rtol ! relative temperature tolerance
    real(r8) :: enth_atol ! absolute enthalpy density tolerance
    real(r8) :: enth_rtol ! relative enthalpy density tolerance
  contains
    procedure :: init
    procedure :: compute
  end type htpc_ml_norm

contains

  subroutine init(this, params, stat, errmsg)

    use parameter_list_type

    class(htpc_ml_norm), intent(out) :: this
    type(parameter_list), intent(inout) :: params
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    character(:), allocatable :: context

    context = 'processing ' // params%name() // ': '
    call params%get('temp-abs-tol', this%temp_atol, default=0.0_r8, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    call params%get('temp-rel-tol', this%temp_rtol, default=0.0_r8, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    if (this%temp_atol < 0.0_r8) then
      stat = 1
      errmsg = context // '"temp-abs-tol" must be >= 0.0'
      return
    end if
    if (this%temp_rtol < 0.0_r8) then
      stat = 1
      errmsg = context // '"temp-rel-tol" must be >= 0.0'
      return
    end if
    if (this%temp_atol == 0.0_r8 .and. this%temp_rtol == 0.0_r8) then
      stat = 1
      errmsg = context // '"temp-abs-tol" and "temp-rel-tol" cannot both be 0.0'
      return
    end if

    call params%get('enth-abs-tol', this%enth_atol, default=0.0_r8, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    call params%get('enth-rel-tol', this%enth_rtol, default=this%temp_rtol, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    if (this%enth_atol < 0.0_r8) then
      stat = 1
      errmsg = context // '"enth-abs-tol" must be >= 0.0'
      return
    end if
    if (this%enth_rtol < 0.0_r8) then
      stat = 1
      errmsg = context // '"enth-rel-tol" must be >= 0.0'
      return
    end if
    if (this%enth_atol == 0.0_r8 .and. this%enth_rtol == 0.0_r8) then
      stat = 1
      errmsg = context // '"enth-abs-tol" and "enth-rel-tol" cannot both be 0.0'
      return
    end if

  end subroutine init


  subroutine compute(this, u, du, du_norm)
    class(htpc_ml_norm), intent(in) :: this
    type(htpc_ml_vector), intent(in), target :: u, du
    real(r8), intent(out) :: du_norm
    du_norm = max(mf_max_bal_norm(u%tc%mfa, du%tc%mfa, u%mesh%mask, this%temp_atol, this%temp_rtol), &
        &         mf_max_bal_norm(u%hc%mfa, du%hc%mfa, u%mesh%mask, this%enth_atol, this%enth_rtol))
  end subroutine compute

end module htpc_ml_norm_type
