!!
!! HTPC_FV_BOX_MODEL_TYPE
!!
!! Zach Jibben <zjibben@lanl.gov>
!! January 2020
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module htpc_fv_box_model_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use fat_box_type
  use bndry_func_class
  use bndry_func2_class
  use parameter_list_type
  implicit none
  private

  type, extends(fat_box), public :: htpc_fv_box_model
    class(bndry_func), allocatable :: bc_dir_xlo, bc_dir_ylo, bc_dir_zlo
    class(bndry_func), allocatable :: bc_dir_xhi, bc_dir_yhi, bc_dir_zhi
    class(bndry_func), allocatable :: bc_flux_xlo, bc_flux_ylo, bc_flux_zlo
    class(bndry_func), allocatable :: bc_flux_xhi, bc_flux_yhi, bc_flux_zhi
    class(bndry_func2), allocatable :: bc_htc_xlo, bc_htc_ylo, bc_htc_zlo
    class(bndry_func2), allocatable :: bc_htc_xhi, bc_htc_yhi, bc_htc_zhi
    class(bndry_func2), allocatable :: bc_rad_zhi, bc_evap_zhi
    real(r8) :: dx(3) ! FIXME: move to FAT_BOX
    integer :: nx(3)
  contains
    procedure :: init
    procedure :: compute_residual
    procedure :: compute_hdot
    procedure, private :: compute_flux
    procedure, private :: compute_diffusion
  end type htpc_fv_box_model

contains

  subroutine init(this, mesh, box, params, stat, errmsg)

    use amrex_mesh_type

    class(htpc_fv_box_model), intent(out) :: this
    type(amrex_mesh), intent(in) :: mesh
    type(fat_box), intent(in) :: box
    type(parameter_list), intent(inout) :: params
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    character(:), allocatable :: context
    type(parameter_list), pointer :: bc_params, source_params

    this%fat_box = box
    this%dx = mesh%geom%dx
    this%nx = this%hi - this%lo + 1

    context = 'processing ' // params%name() // ': '
    if (params%is_sublist('bc')) then
      bc_params => params%sublist('bc')
      call init_bc(this, mesh%geom, bc_params, stat, errmsg)
      if (stat /= 0) return
    else
      stat = 1
      errmsg = context // 'missing "bc" sublist parameter'
      return
    end if

  end subroutine init


  subroutine init_bc(this, geom, params, stat, errmsg)

    use scalar_func_factories
    use truchas_scalar_func_table
    use amrex_base_module, only: amrex_geometry, amrex_box

    type(htpc_fv_box_model), intent(inout) :: this
    type(amrex_geometry), intent(in)    :: geom
    type(parameter_list), intent(inout) :: params
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    integer :: i
    type(parameter_list_iterator) :: piter
    type(parameter_list), pointer :: bc_params
    character(:), allocatable :: context, type, sides(:)
#ifdef GNU_PR93762
    character(:), allocatable :: dummy
#endif

    ! FIXME: needed for bndry_face_func%init -- change its interface
    type(amrex_box) :: bx
    bx = amrex_box(this%lo, this%hi)

    piter = parameter_list_iterator(params, sublists_only=.true.)
    do while (.not.piter%at_end())
      bc_params => piter%sublist()
      context = 'processing ' // bc_params%name() // ': '
      call bc_params%get('type', type, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if
      call bc_params%get('sides', sides, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if
      do i = 1, size(sides)
        if (all(sides(i) /= ['xlo','ylo','zlo','xhi','yhi','zhi'])) then
          stat = 1
          errmsg = context // 'unknown "sides" specifier: ' // sides(i)
          return
        end if
      end do
      select case (type)
      case ('dirichlet')
        call init_dirichlet
      case ('flux')
        call init_flux
      case ('htc')
        call init_htc
      case ('radiation')
        call init_rad
      case ('evaporation')
        call init_evap
      case default
        stat = 1
        errmsg = context // 'unknown "type" value: ' // type
        return
      end select
      call piter%next
    end do

  contains

    !! These subroutine complete the parsing of the BC_PARAMS parameter list
    !! for specific boundary condtion types and initialize the appropriate
    !! data components of the HTPC_BOX structure array component. These can
    !! assume a validated list of side specifiers SIDES.

    subroutine init_dirichlet

      use bndry_face_func_type

      integer :: i
      real(r8) :: const
      character(3) :: side
      type(parameter_list), pointer :: func_params
      class(scalar_func), allocatable :: f
      type(bndry_face_func), allocatable :: bff

      if (bc_params%is_sublist('data')) then
        func_params => bc_params%sublist('data')
        call alloc_scalar_func(f, func_params, stat, errmsg)
        if (stat /= 0) return
      else if (bc_params%is_scalar('data')) then
        call bc_params%get('data', const, stat=stat, errmsg=errmsg)
        if (stat /= 0) then
          errmsg = context // errmsg
          return
        end if
        call alloc_const_scalar_func(f, const)
      else
        stat = 1
        errmsg = context // 'missing valid "data" parameter'
        return
      end if

      do i = 1, size(sides)
        side = sides(i)
        if (this%is_bndry(side)) then
          allocate(bff)
          call bff%init(geom, bx, side, f)
          select case (side)
          case ('xlo')
            call move_alloc(bff, this%bc_dir_xlo)
          case ('ylo')
            call move_alloc(bff, this%bc_dir_ylo)
          case ('zlo')
            call move_alloc(bff, this%bc_dir_zlo)
          case ('xhi')
            call move_alloc(bff, this%bc_dir_xhi)
          case ('yhi')
            call move_alloc(bff, this%bc_dir_yhi)
          case ('zhi')
            call move_alloc(bff, this%bc_dir_zhi)
          end select
        end if
      end do

    end subroutine init_dirichlet

    subroutine init_flux

      use bndry_face_func_type

      integer :: i
      real(r8) :: const
      character(3) :: side
      type(parameter_list), pointer :: func_params
      class(scalar_func), allocatable :: f
      type(bndry_face_func), allocatable :: bff
      character(:), allocatable :: fname

      if (bc_params%is_sublist('data')) then
        func_params => bc_params%sublist('data')
        call alloc_scalar_func(f, func_params, stat, errmsg)
        if (stat /= 0) return
      else if (bc_params%is_scalar('data')) then
#ifdef GNU_PR93762
        call bc_params%get('data', fname, stat=stat, errmsg=dummy)
#else
        call bc_params%get('data', fname, stat=stat)
#endif
        if (stat == 0) then ! name of a function
          call lookup_func(fname, f)
          if (.not.allocated(f)) then
            stat = 1
            errmsg = context // 'unknown function name: ' // fname
            return
          end if
        else  ! it must be a constant value
          call bc_params%get('data', const, stat=stat, errmsg=errmsg)
          if (stat /= 0) then
            errmsg = context // errmsg
            return
          end if
          call alloc_const_scalar_func(f, const)
        end if
      else
        stat = 1
        errmsg = context // 'missing valid "data" parameter'
        return
      end if

      do i = 1, size(sides)
        side = sides(i)
        if (this%is_bndry(side)) then
          allocate(bff)
          call bff%init(geom, bx, side, f)
          select case (side)
          case ('xlo')
            call move_alloc(bff, this%bc_flux_xlo)
          case ('ylo')
            call move_alloc(bff, this%bc_flux_ylo)
          case ('zlo')
            call move_alloc(bff, this%bc_flux_zlo)
          case ('xhi')
            call move_alloc(bff, this%bc_flux_xhi)
          case ('yhi')
            call move_alloc(bff, this%bc_flux_yhi)
          case ('zhi')
            call move_alloc(bff, this%bc_flux_zhi)
          end select
        end if
      end do

    end subroutine init_flux

    subroutine init_htc

      use htc_bndry_func_type

      integer :: i
      real(r8) :: const
      character(3) :: side
      type(parameter_list), pointer :: func_params
      class(scalar_func), allocatable :: f, g
      type(htc_bndry_func), allocatable :: bc

      if (bc_params%is_sublist('coefficient')) then
        func_params => bc_params%sublist('coefficient')
        call alloc_scalar_func(f, func_params, stat, errmsg)
        if (stat /= 0) return
      else if (bc_params%is_scalar('coefficient')) then
        call bc_params%get('coefficient', const, stat=stat, errmsg=errmsg)
        if (stat /= 0) then
          errmsg = context // errmsg
          return
        end if
        call alloc_const_scalar_func(f, const)
      else
        stat = 1
        errmsg = context // 'missing valid "coefficient" parameter'
        return
      end if

      if (bc_params%is_sublist('ambient-temp')) then
        func_params => bc_params%sublist('ambient-temp')
        call alloc_scalar_func(g, func_params, stat, errmsg)
        if (stat /= 0) return
      else if (bc_params%is_scalar('ambient-temp')) then
        call bc_params%get('ambient-temp', const, stat=stat, errmsg=errmsg)
        if (stat /= 0) then
          errmsg = context // errmsg
          return
        end if
        call alloc_const_scalar_func(g, const)
      else
        stat = 1
        errmsg = context // 'missing valid "ambient-temp" parameter'
        return
      end if

      do i = 1, size(sides)
        side = sides(i)
        if (this%is_bndry(side)) then
          allocate(bc)
          call bc%init(geom, bx, side, f, g)
          select case (side)
          case ('xlo')
            call move_alloc(bc, this%bc_htc_xlo)
          case ('ylo')
            call move_alloc(bc, this%bc_htc_ylo)
          case ('zlo')
            call move_alloc(bc, this%bc_htc_zlo)
          case ('xhi')
            call move_alloc(bc, this%bc_htc_xhi)
          case ('yhi')
            call move_alloc(bc, this%bc_htc_yhi)
          case ('zhi')
            call move_alloc(bc, this%bc_htc_zhi)
          end select
        end if
      end do

    end subroutine init_htc

    subroutine init_rad

      use rad_bndry_func_type
      use physical_constants, only: stefan_boltzmann, absolute_zero

      integer :: i
      real(r8) :: const
      character(3) :: side
      type(parameter_list), pointer :: func_params
      class(scalar_func), allocatable :: f, g
      type(rad_bndry_func), allocatable :: bc

      if (bc_params%is_sublist('emissivity')) then
        func_params => bc_params%sublist('emissivity')
        call alloc_scalar_func(f, func_params, stat, errmsg)
        if (stat /= 0) return
      else if (bc_params%is_scalar('emissivity')) then
        call bc_params%get('emissivity', const, stat=stat, errmsg=errmsg)
        if (stat /= 0) then
          errmsg = context // errmsg
          return
        end if
        call alloc_const_scalar_func(f, const)
      else
        stat = 1
        errmsg = context // 'missing valid "emissivity" parameter'
        return
      end if

      if (bc_params%is_sublist('ambient-temp')) then
        func_params => bc_params%sublist('ambient-temp')
        call alloc_scalar_func(g, func_params, stat, errmsg)
        if (stat /= 0) return
      else if (bc_params%is_scalar('ambient-temp')) then
        call bc_params%get('ambient-temp', const, stat=stat, errmsg=errmsg)
        if (stat /= 0) then
          errmsg = context // errmsg
          return
        end if
        call alloc_const_scalar_func(g, const)
      else
        stat = 1
        errmsg = context // 'missing valid "ambient-temp" parameter'
        return
      end if

      do i = 1, size(sides)
        side = sides(i)
        if (this%is_bndry(side)) then
          allocate(bc)
          call bc%init(geom, bx, side, stefan_boltzmann, absolute_zero, f, g)
          select case (side)
          case ('zhi')
            call move_alloc(bc, this%bc_rad_zhi)
          case default
            stat = 1
            errmsg = context // 'radiation BC only implemented for side "zhi"'
            return
          end select
        end if
      end do

    end subroutine init_rad

    subroutine init_evap

      use evap_bndry_func_type

      integer :: i
      real(r8) :: prefactor, beta, E_a
      character(3) :: side
      type(evap_bndry_func), allocatable :: bc

      call bc_params%get('prefactor', prefactor, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if

      call bc_params%get('temp-exponent', beta, default=0.0_r8, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if

      call bc_params%get('activation-energy', E_a, stat=stat, errmsg=errmsg)
      if (stat /= 0) then
        errmsg = context // errmsg
        return
      end if

      do i = 1, size(sides)
        side = sides(i)
        if (this%is_bndry(side)) then
          allocate(bc)
          call bc%init(geom, bx, side, prefactor, beta, E_a)
          select case (side)
          case ('zhi')
            call move_alloc(bc, this%bc_evap_zhi)
          case default
            stat = 1
            errmsg = context // 'evaporation flux BC only implemented for side "zhi"'
            return
          end select
        end if
      end do

    end subroutine init_evap

  end subroutine init_bc


  subroutine compute_residual(this, t, kappa, tc, rc)

    class(htpc_fv_box_model), intent(inout) :: this
    real(r8), intent(in) :: t
    real(r8), contiguous, intent(in) :: kappa(0:,0:,0:)
    real(r8), contiguous, intent(in) :: tc(0:,0:,0:)
    real(r8), contiguous, intent(out) :: rc(1:,1:,1:)

    call this%compute_diffusion(t, kappa, tc, rc) ! diffusion term

  end subroutine compute_residual


  subroutine compute_diffusion(this, t, kappa, tc, div)

    class(htpc_fv_box_model), intent(inout) :: this
    real(r8), intent(in) :: t
    real(r8), contiguous, intent(in) :: kappa(0:,0:,0:), tc(0:,0:,0:)
    real(r8), contiguous, intent(out) :: div(1:,1:,1:)

    ! tc has ghosts, and we don't want any for the qs
    real(r8) :: qx(this%nx(1)+1, this%nx(2),   this%nx(3))
    real(r8) :: qy(this%nx(1),   this%nx(2)+1, this%nx(3))
    real(r8) :: qz(this%nx(1),   this%nx(2),   this%nx(3)+1)
    integer  :: i, j, k

    call this%compute_flux(t, kappa, tc, qx, qy, qz)
    do k = 1, this%nx(3)
      do j = 1, this%nx(2)
        do i = 1, this%nx(1)
          div(i,j,k) = qx(i+1,j,k) - qx(i,j,k) + qy(i,j+1,k) - qy(i,j,k) + qz(i,j,k+1) - qz(i,j,k)
        end do
      end do
    end do

  end subroutine compute_diffusion


  subroutine compute_hdot(this, t, kappa, tc, hd)
    class(htpc_fv_box_model), intent(inout) :: this
    real(r8), intent(in) :: t
    real(r8), contiguous, intent(in) :: kappa(0:,0:,0:), tc(0:,0:,0:)
    real(r8), contiguous, intent(out) :: hd(1:,1:,1:)
    call this%compute_diffusion(t, kappa, tc, hd)
    hd = - hd / product(this%dx)
  end subroutine compute_hdot


  subroutine compute_flux(this, t, kappa, tc, qx, qy, qz)

    class(htpc_fv_box_model), intent(inout) :: this
    real(r8), intent(in) :: t
    real(r8), contiguous, intent(in) :: kappa(0:,0:,0:), tc(0:,0:,0:)
    real(r8), contiguous, intent(out) :: qx(:,:,:), qy(:,:,:), qz(:,:,:)

    integer :: i, j, k, dlo(3), dhi(3)
    real(r8) :: kappaf, grad_temp, ax, ay, az

    ax = this%dx(2) * this%dx(3)
    ay = this%dx(1) * this%dx(3)
    az = this%dx(1) * this%dx(2)

    dlo = 1 + merge(1, 0, this%lo_is_bndry)
    dhi = this%nx - merge(1, 0, this%hi_is_bndry)

    call bcs

    do k = 1, this%nx(3)
      do j = 1, this%nx(2)
        do i = dlo(1), dhi(1) + 1
          kappaf = 2 / (1/kappa(i,j,k) + 1/kappa(i-1,j,k))
          grad_temp = (tc(i,j,k) - tc(i-1,j,k)) / this%dx(1)
          qx(i,j,k) = - kappaf * grad_temp * ax
        end do
      end do
    end do

    do k = 1, this%nx(3)
      do j = dlo(2), dhi(2) + 1
        do i = 1, this%nx(1)
          kappaf = 2 / (1/kappa(i,j,k) + 1/kappa(i,j-1,k))
          grad_temp = (tc(i,j,k) - tc(i,j-1,k)) / this%dx(2)
          qy(i,j,k) = - kappaf * grad_temp * ay
        end do
      end do
    end do

    do k = dlo(3), dhi(3) + 1
      do j = 1, this%nx(2)
        do i = 1, this%nx(1)
          kappaf = 2 / (1/kappa(i,j,k) + 1/kappa(i,j,k-1))
          grad_temp = (tc(i,j,k) - tc(i,j,k-1)) / this%dx(3)
          qz(i,j,k) = - kappaf * grad_temp * az
        end do
      end do
    end do

  contains

    subroutine bcs()

      integer :: nx, ny, nz

      nx = this%nx(1)
      ny = this%nx(2)
      nz = this%nx(3)

      if (this%lo_is_bndry(1)) qx(1,:,:) = 0
      if (this%hi_is_bndry(1)) qx(nx+1,:,:) = 0
      if (this%lo_is_bndry(2)) qy(:,1,:) = 0
      if (this%hi_is_bndry(2)) qy(:,ny+1,:) = 0
      if (this%lo_is_bndry(3)) qz(:,:,1) = 0
      if (this%hi_is_bndry(3)) qz(:,:,nz+1) = 0

      !! Dirichlet Temperature
      !! Face conductivity is approximated by the cell-centered conductivity
      if (allocated(this%bc_dir_xlo)) then
        call this%bc_dir_xlo%compute(t)
        qx(1,:,:) = - kappa(1,1:ny,1:nz) * (tc(1,1:ny,1:nz) - this%bc_dir_xlo%value) * 2 / this%dx(1) * ax
      end if
      if (allocated(this%bc_dir_xhi)) then
        call this%bc_dir_xhi%compute(t)
        qx(nx+1,:,:) = - kappa(nx,1:ny,1:nz) * (this%bc_dir_xhi%value - tc(nx,1:ny,1:nz)) * 2 / this%dx(1) * ax
      end if
      if (allocated(this%bc_dir_ylo)) then
        call this%bc_dir_ylo%compute(t)
        qy(:,1,:) = - kappa(1:nx,1,1:nz) * (tc(1:nx,1,1:nz) - this%bc_dir_ylo%value) * 2 / this%dx(2) * ay
      end if
      if (allocated(this%bc_dir_yhi)) then
        call this%bc_dir_yhi%compute(t)
        qy(:,ny+1,:) = - kappa(1:nx,ny,1:nz) * (this%bc_dir_yhi%value - tc(1:nx,ny,1:nz)) * 2 / this%dx(2) * ay
      end if
      if (allocated(this%bc_dir_zlo)) then
        call this%bc_dir_zlo%compute(t)
        qz(:,:,1) = - kappa(1:nx,1:ny,1) * (tc(1:nx,1:ny,1) - this%bc_dir_zlo%value) * 2 / this%dx(3) * az
      end if
      if (allocated(this%bc_dir_zhi)) then
        call this%bc_dir_zhi%compute(t)
        qz(:,:,nz+1) = - kappa(1:nx,1:ny,nz) * (this%bc_dir_zhi%value - tc(1:nx,1:ny,nz)) * 2 / this%dx(3) * az
      end if

      !! Flux
      if (allocated(this%bc_flux_xlo)) then
        call this%bc_flux_xlo%compute(t)
        qx(1,:,:) = -this%bc_flux_xlo%value * ax
      end if
      if (allocated(this%bc_flux_xhi)) then
        call this%bc_flux_xhi%compute(t)
        qx(nx+1,:,:) = this%bc_flux_xhi%value * ax
      end if
      if (allocated(this%bc_flux_ylo)) then
        call this%bc_flux_ylo%compute(t)
        qy(:,1,:) = -this%bc_flux_ylo%value * ay
      end if
      if (allocated(this%bc_flux_yhi)) then
        call this%bc_flux_yhi%compute(t)
        qy(:,ny+1,:) = this%bc_flux_yhi%value * ay
      end if
      if (allocated(this%bc_flux_zlo)) then
        call this%bc_flux_zlo%compute(t)
        qz(:,:,1) = -this%bc_flux_zlo%value * az
      end if
      if (allocated(this%bc_flux_zhi)) then
        call this%bc_flux_zhi%compute(t)
        qz(:,:,nz+1) = this%bc_flux_zhi%value * az
      end if

      !! Heat Transfer Coefficient (HTC)
      !! Face temperature is approximated by the cell-centered temperature
      !! The face area is already incorporated into %VALUE arrays.
      if (allocated(this%bc_htc_xlo)) then
        call this%bc_htc_xlo%compute(t, tc(1,1:ny,1:nz))
        qx(1,:,:) = qx(1,:,:) - this%bc_htc_xlo%value
      end if
      if (allocated(this%bc_htc_xhi)) then
        call this%bc_htc_xhi%compute(t, tc(nx,1:ny,1:nz))
        qx(nx+1,:,:) = qx(nx+1,:,:) + this%bc_htc_xhi%value
      end if
      if (allocated(this%bc_htc_ylo)) then
        call this%bc_htc_ylo%compute(t, tc(1:nx,1,1:nz))
        qy(:,1,:) = qy(:,1,:) - this%bc_htc_ylo%value
      end if
      if (allocated(this%bc_htc_yhi)) then
        call this%bc_htc_yhi%compute(t, tc(1:nx,ny,1:nz))
        qy(:,ny+1,:) = qy(:,ny+1,:) + this%bc_htc_yhi%value
      end if
      if (allocated(this%bc_htc_zlo)) then
        call this%bc_htc_zlo%compute(t, tc(1:nx,1:ny,1))
        qz(:,:,1) = qz(:,:,1) - this%bc_htc_zlo%value
      end if
      if (allocated(this%bc_htc_zhi)) then
        call this%bc_htc_zhi%compute(t, tc(1:nx,1:ny,nz))
        qz(:,:,nz+1) = qz(:,:,nz+1) + this%bc_htc_zhi%value
      end if

      !! Radiation
      !! Face temperature is approximated by the cell-centered temperature
      !! The face area is already incorporated into %VALUE arrays.
      if (allocated(this%bc_rad_zhi)) then
        call this%bc_rad_zhi%compute(t, tc(1:nx,1:ny,nz))
        qz(:,:,nz+1) = qz(:,:,nz+1) + this%bc_rad_zhi%value
      end if

      !! Evaporation
      !! Face temperature is approximated by the cell-centered temperature
      !! The face area is already incorporated into %VALUE arrays.
      if (allocated(this%bc_evap_zhi)) then
        call this%bc_evap_zhi%compute(t, tc(1:nx,1:ny,nz))
        qz(:,:,nz+1) = qz(:,:,nz+1) + this%bc_evap_zhi%value
      end if

    end subroutine bcs

  end subroutine compute_flux

end module htpc_fv_box_model_type
