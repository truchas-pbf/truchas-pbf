!!
!! HTPC_FV_IDAESOL_MODEL_TYPE
!!
!! This module defines an extension of the IDAESOL_MODEL abstract class that
!! implements the methods required by the ODE integrator. It bundles several
!! different computational pieces for the heat transfer/phase change model
!! and adapts them to the required interface.
!!
!! TODO: This shares everything but the base types with the non-fv version.
!!       Can we reuse the code?
!!
!! Zach Jibben <zjibben@lanl.gov>
!! January 2020
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module htpc_fv_idaesol_model_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use idaesol_type, only: idaesol_model
  use htpc_fv_model_type
  use htpc_fv_precon_type
  use htpc_fv_amrex_precon_type
  use htpc_fv_norm_type
  use htpc_fv_vector_type
  use vector_class
  implicit none
  private

  type, extends(idaesol_model), public :: htpc_fv_idaesol_model
    type(htpc_fv_model),  pointer :: model =>  null() ! reference only -- not owned
    type(htpc_fv_amrex_precon), pointer :: precon => null() ! reference only -- not owned
    type(htpc_fv_norm),   pointer :: norm   => null() ! reference only -- not owned
  contains
    procedure :: init
    !! Deferred procedures from IDAESOL_MODEL
    procedure :: alloc_vector
    procedure :: compute_f
    procedure :: apply_precon
    procedure :: compute_precon
    procedure :: du_norm
    procedure :: schk
  end type htpc_fv_idaesol_model

contains

  subroutine init(this, model, precon, norm)
    class(htpc_fv_idaesol_model), intent(out) :: this
    type(htpc_fv_model),  intent(in), target :: model
    type(htpc_fv_amrex_precon), intent(in), target :: precon
    type(htpc_fv_norm),   intent(in), target :: norm
    this%model => model
    this%precon => precon
    this%norm => norm
    ASSERT(precon%model_associated(model))
  end subroutine init

  subroutine alloc_vector(this, vec)
    class(htpc_fv_idaesol_model), intent(in) :: this
    class(vector), allocatable, intent(out) :: vec
    type(htpc_fv_vector), allocatable :: tmp
    allocate(tmp)
    call this%model%init_vector(tmp)
    call move_alloc(tmp, vec)
  end subroutine alloc_vector

  subroutine compute_f(this, t, u, udot, f)
    class(htpc_fv_idaesol_model) :: this
    real(r8), intent(in) :: t
    class(vector), intent(inout) :: u, udot, f
    select type (u)
    type is (htpc_fv_vector)
    select type (udot)
    type is (htpc_fv_vector)
    select type (f)
    type is (htpc_fv_vector)
    call this%model%compute_residual(t, u, udot, f)
    end select
    end select
    end select
  end subroutine compute_f

  subroutine apply_precon(this, t, u, f)
    class(htpc_fv_idaesol_model) :: this
    real(r8), intent(in) :: t
    class(vector), intent(inout) :: u, f
    select type (u)
    type is (htpc_fv_vector)
    select type (f)
    type is (htpc_fv_vector)
    call this%precon%apply(f)
    end select
    end select
  end subroutine apply_precon

  subroutine compute_precon(this, t, u, dt)
    class(htpc_fv_idaesol_model) :: this
    real(r8), intent(in) :: t, dt
    class(vector), intent(inout) :: u
    select type (u)
    type is (htpc_fv_vector)
    call this%precon%compute(t, u, dt)
    end select
  end subroutine compute_precon

  subroutine du_norm(this, u, du, error)
    class(htpc_fv_idaesol_model) :: this
    class(vector), intent(in) :: u, du
    real(r8), intent(out) :: error
    select type (u)
    type is (htpc_fv_vector)
    select type (du)
    type is (htpc_fv_vector)
    call this%norm%compute(u, du, error)
    end select
    end select
  end subroutine du_norm

  subroutine schk(this, u, stage, errc)
    class(htpc_fv_idaesol_model) :: this
    class(vector), intent(in) :: u
    integer, intent(in)  :: stage
    integer, intent(out) :: errc
    errc = 0  ! solution is fine
  end subroutine schk

end module htpc_fv_idaesol_model_type
