!!
!! HTPC_MFD_NORM_TYPE
!!
!! This module defines a derived type that encapsulates the norm used for
!! solution increments of the heat transfer/phase change model.
!!
!! Neil N. Carlson <nnc@lanl.gov>
!! Adapted from Pececillo, January 2018
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module htpc_mfd_norm_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use htpc_mfd_model_type
  use htpc_mfd_vector_type
  implicit none
  private

  type, public :: htpc_mfd_norm
    private
    type(htpc_mfd_model), pointer :: model => null()  ! reference only -- not owned
    real(r8) :: temp_atol ! absolute temperature tolerance
    real(r8) :: temp_rtol ! relative temperature tolerance
    real(r8) :: enth_atol ! absolute enthalpy density tolerance
    real(r8) :: enth_rtol ! relative enthalpy density tolerance
  contains
    procedure :: init
    procedure :: compute
  end type htpc_mfd_norm

contains

  subroutine init(this, model, params, stat, errmsg)

    use parameter_list_type

    class(htpc_mfd_norm), intent(out) :: this
    type(htpc_mfd_model), intent(in), target :: model
    type(parameter_list), intent(inout) :: params
    integer, intent(out) :: stat
    character(:), allocatable, intent(out) :: errmsg

    character(:), allocatable :: context

    this%model => model

    context = 'processing ' // params%name() // ': '
    call params%get('temp-abs-tol', this%temp_atol, default=0.0_r8, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    call params%get('temp-rel-tol', this%temp_rtol, default=0.0_r8, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    if (this%temp_atol < 0.0_r8) then
      stat = 1
      errmsg = context // '"temp-abs-tol" must be >= 0.0'
      return
    end if
    if (this%temp_rtol < 0.0_r8) then
      stat = 1
      errmsg = context // '"temp-rel-tol" must be >= 0.0'
      return
    end if
    if (this%temp_atol == 0.0_r8 .and. this%temp_rtol == 0.0_r8) then
      stat = 1
      errmsg = context // '"temp-abs-tol" and "temp-rel-tol" cannot both be 0.0'
      return
    end if

    call params%get('enth-abs-tol', this%enth_atol, default=0.0_r8, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    call params%get('enth-rel-tol', this%enth_rtol, default=this%temp_rtol, stat=stat, errmsg=errmsg)
    if (stat /= 0) then
      errmsg = context // errmsg
      return
    end if
    if (this%enth_atol < 0.0_r8) then
      stat = 1
      errmsg = context // '"enth-abs-tol" must be >= 0.0'
      return
    end if
    if (this%enth_rtol < 0.0_r8) then
      stat = 1
      errmsg = context // '"enth-rel-tol" must be >= 0.0'
      return
    end if
    if (this%enth_atol == 0.0_r8 .and. this%enth_rtol == 0.0_r8) then
      stat = 1
      errmsg = context // '"enth-abs-tol" and "enth-rel-tol" cannot both be 0.0'
      return
    end if

  end subroutine init

  subroutine compute(this, u, du, du_norm)
    use amrex_base_module
    class(htpc_mfd_norm), intent(in) :: this
    type(htpc_mfd_vector), intent(in) :: u, du
    real(r8), intent(out) :: du_norm
    du_norm = max(du%tc_max_bal_norm(u, this%temp_atol, this%temp_rtol), &
                  du%tx_max_bal_norm(u, this%temp_atol, this%temp_rtol), &
                  du%ty_max_bal_norm(u, this%temp_atol, this%temp_rtol), &
                  du%tz_max_bal_norm(u, this%temp_atol, this%temp_rtol), &
                  du%hc_max_bal_norm(u, this%enth_atol, this%enth_rtol))
    call amrex_parallel_reduce_max(du_norm)
  end subroutine compute

end module htpc_mfd_norm_type
