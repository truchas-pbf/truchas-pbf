!!
!! HTPC_FV_SOLVER_TYPE
!!
!! Zach Jibben <zjibben@lanl.gov>
!! January 2020
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module htpc_fv_solver_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use htpc_solver_class
  use htpc_fv_model_type
  use htpc_fv_precon_type
  use htpc_fv_amrex_precon_type
  use htpc_fv_norm_type
  use htpc_fv_vector_type
  use htpc_fv_idaesol_model_type
  use idaesol_type
  use amrex_base_module, only: amrex_multifab
  use string_utilities, only: i_to_c
  use simlog_type
  use timer_tree_type
  implicit none
  private

  type, extends(htpc_solver), public :: htpc_fv_solver
    private
    type(simlog) :: log
    type(htpc_fv_model), pointer :: model => null()  ! not owned
    type(htpc_fv_amrex_precon), pointer :: precon => null()
    type(htpc_fv_norm), pointer :: norm => null()
    type(htpc_fv_idaesol_model) :: integ_model
    type(htpc_fv_vector) :: u
    type(idaesol) :: integ
    integer :: lun = 0  ! logical unit for integrator output
    real(r8) :: t
    logical :: state_is_pending = .false.
    !! For state save/restore
    type(htpc_fv_solver), pointer :: cache => null()
  contains
    procedure :: init
    !generic :: set_initial_state => set_initial_state1, set_initial_state2, start
    procedure :: start
    procedure :: time
    procedure :: get_vol_frac
    procedure :: get_liq_frac
    procedure :: get_temp_soln
    procedure :: get_enth_soln
    procedure :: get_temp_grad
    procedure :: write_metrics
    procedure :: step
    procedure :: commit_pending_state
    procedure :: set_initial_state1, set_initial_state2
    procedure :: integrate
    !TODO: implement these
    ! procedure, private :: get_interpolated_solution
    ! procedure, private :: save_state
    ! procedure, private :: restore_state
    final :: htpc_fv_solver_delete
  end type htpc_fv_solver

  !FIXME: Do they conflict with HTPC_MFD_SOLVER_TYPE exporting them?
  ! !! Export integration return statuses from IDAESOL_TYPE;
  ! !! INTEGRATE returns one of these values.
  ! public :: SOLVED_TO_TOUT, SOLVED_TO_NSTEP
  ! public :: BAD_INPUT, STEP_FAILED, STEP_SIZE_TOO_SMALL

contains

  !! Final subroutine for HTPC_FV_SOLVER objects.
  recursive subroutine htpc_fv_solver_delete(this)
    type(htpc_fv_solver), intent(inout) :: this
    if (associated(this%precon)) deallocate(this%precon)
    if (associated(this%norm)) deallocate(this%norm)
    if (associated(this%cache)) deallocate(this%cache)
    if (this%lun /= 0) close(this%lun)
  end subroutine htpc_fv_solver_delete

  subroutine init(this, env, model, params)
    
    use truchas_env_type
    use parameter_list_type
    use htpc_model_class

    class(htpc_fv_solver), intent(out) :: this
    type(truchas_env), intent(in) :: env
    class(htpc_model), intent(in), target :: model
    type(parameter_list), intent(inout) :: params

    integer :: stat
    character(:), allocatable :: errmsg
    logical :: flag

    call start_timer('heat')

    select type (model)
    type is (htpc_fv_model)
      this%model => model
    end select
    INSIST(associated(this%model))
    this%log = env%log

    ! "num-cycles"
    ! "strong-threshold" (optional)
    ! "coarsen-type" (optional)
    allocate(this%precon)
    call this%precon%init(this%model, params, stat, errmsg)
    if (stat /= 0) call this%log%fatal(errmsg)

    ! "temp-abs-tol" (optional)
    ! "temp-rel-tol" (optional)
    allocate(this%norm)
    call this%norm%init(params, stat, errmsg)
    if (stat /= 0) call this%log%fatal(errmsg)

    call this%integ_model%init(this%model, this%precon, this%norm)

    ! "nlk-max-iter"
    ! "nlk-tol"
    ! "nlk-max-vec"
    ! "nlk-vec-tol"
    call this%integ%init(this%integ_model, params, stat, errmsg)
    if (stat /= 0) call this%log%fatal(errmsg)

    call params%get('verbose-stepping', flag, default=.false.)
    if (flag .and. env%rank == 0) then
      open(newunit=this%lun,file=env%root//'bdf2.log')
      call this%integ%set_verbose_stepping(this%lun)
    end if

    call this%model%init_vector(this%u)

    call stop_timer('heat')

  end subroutine init

  subroutine set_initial_state1(this, t, temp, dt)

    class(htpc_fv_solver), intent(inout) :: this
    real(r8), intent(in) :: t, dt
    type(amrex_multifab), intent(in) :: temp

    type(htpc_fv_vector) :: udot

    call start_timer('heat')
    this%t = t
    call this%u%tc%setval(0.0_r8) ! ensure domain ghosts are defined
    call this%u%tc%copy(temp, 1, 1, 1, 0)
    call this%u%tc%fill_boundary(this%model%mesh%geom)
    call udot%init(this%u)
    call this%model%compute_udot(t, dt, this%u, udot)
    call this%integ%set_initial_state(t, this%u, udot)
    call stop_timer('heat')

  end subroutine set_initial_state1

  subroutine set_initial_state2(this, t, temp)

    class(htpc_fv_solver), intent(inout) :: this
    real(r8), intent(in) :: t
    type(amrex_multifab), intent(in) :: temp
    
    call start_timer('heat')
    this%t = t
    call this%u%tc%setval(0.0_r8) ! ensure domain ghosts are defined
    call this%u%tc%copy(temp, 1, 1, 1, 0)
    call this%u%tc%fill_boundary(this%model%mesh%geom)
    call this%model%compute_cell_enth(this%u)
    call stop_timer('heat')

  end subroutine set_initial_state2

  !! Starts or restarts the integrator
  subroutine start(this, dt)
    class(htpc_fv_solver), intent(inout) :: this
    real(r8), intent(in) :: dt
    type(htpc_fv_vector) :: udot
    call start_timer('heat')
    call udot%init(this%u)
    call this%model%compute_udot(this%t, dt, this%u, udot)
    call this%integ%set_initial_state(this%t, this%u, udot)
    call stop_timer('heat')
  end subroutine start

  !! Returns the current integration time.
  real(r8) function time(this)
    class(htpc_fv_solver), intent(in) :: this
    time = this%integ%last_time()
  end function time


  !! Returns the current temperature solution.
  subroutine get_temp_soln(this, temp, n)
    class(htpc_fv_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: temp
    integer, intent(in) :: n
    call temp%copy(this%u%tc, 1, n, 1, 0)  !TODO: ghost or no ghost?
    call temp%fill_boundary(this%model%mesh%geom)
  end subroutine get_temp_soln


  !! Returns the gradient of the current temperature solution.
  subroutine get_temp_grad(this, grad)
    class(htpc_fv_solver), intent(inout) :: this
    type(amrex_multifab), intent(inout) :: grad
    call this%model%get_temp_grad(this%u, grad)
  end subroutine get_temp_grad


  !! Returns the current enthalpy solution.
  subroutine get_enth_soln(this, enth, n)
    class(htpc_fv_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: enth
    integer, intent(in) :: n
    call enth%copy(this%u%hc, 1, n, 1, 0)
  end subroutine get_enth_soln


  !TODO: implement this
  ! !! Returns the solution U interpolated to time T.  This should only
  ! !! be called when the integrator has first stepped across time T, so
  ! !! that T lies within an interval of very recent time steps where
  ! !! solution data is currently available.
  ! subroutine get_interpolated_solution(this, t, temp)
  !   class(htpc_fv_solver), intent(in) :: this
  !   real(r8), intent(in)  :: t
  !   type(amrex_multifab), intent(inout) :: temp
  !   real(r8), allocatable :: u(:)
  !   real(r8), pointer, contiguous :: view(:)
  !   allocate(u(this%model%num_dof()))
  !   call this%integ%get_interpolated_state(t, u)
  !   call this%model%get_cell_temp_view(u, view)
  !   call this%model%mesh%copy_to_multifab(view, temp, 1)
  ! end subroutine get_interpolated_solution


  subroutine write_metrics(this, string)
    class(htpc_fv_solver), intent(in) :: this
    character(*), intent(out) :: string(:)
    ASSERT(size(string) == 2)
    call this%integ%write_metrics(string)
  end subroutine write_metrics


  subroutine get_vol_frac(this, vfrac, n)
    class(htpc_fv_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: vfrac
    integer, intent(in) :: n
    call this%get_liq_frac(vfrac, n+1)
    call vfrac%setval(1.0_r8, n, 1, 0)
    call vfrac%subtract(vfrac, n+1, n, 1, 0)
  end subroutine get_vol_frac


  !! NB: uses no temperature ghost data, no returns any ghost data
  subroutine get_liq_frac(this, lfrac, n)

    use amrex_base_module

    class(htpc_fv_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: lfrac
    integer, intent(in) :: n

    type(amrex_mfiter) :: mfi
    type(amrex_box) :: bx
    real(r8), pointer, contiguous :: dp1(:,:,:,:), dp2(:,:,:,:)
    integer :: ix, iy, iz

    call amrex_mfiter_build(mfi, lfrac)
    do while (mfi%next())
      bx = mfi%tilebox()
      dp1 => this%u%tc%dataptr(mfi)
      dp2 => lfrac%dataptr(mfi)
      do iz = bx%lo(3), bx%hi(3)
        do iy = bx%lo(2), bx%hi(2)
          do ix = bx%lo(1), bx%hi(1)
            dp2(ix,iy,iz,n) = this%model%mat%liquid_frac(dp1(ix,iy,iz,1))
          end do
        end do
      end do
    end do
    call lfrac%fill_boundary(this%model%mesh%geom)

  end subroutine get_liq_frac


  subroutine step(this, t, hnext, stat)

    class(htpc_fv_solver), intent(inout) :: this
    real(r8), intent(in) :: t
    real(r8), intent(out) :: hnext
    integer, intent(out) :: stat

    call start_timer('heat')

    call this%integ%step(t, this%u, hnext, stat)
    if (stat == 0) then
      this%t = t
      this%state_is_pending = .true.
    else
      call this%integ%get_last_state_copy(this%u) ! restore last good state
      this%state_is_pending = .false.
      select case (stat)
      case (1)
        call this%log%info('HTPC_FV_SOLVER: step failed: nonlinear iteration failure', VERB_NOISY)
      case (2)
        call this%log%info('HTPC_FV_SOLVER: step rejected: excessive predictor error', VERB_NOISY)
      case (3)
        call this%log%info('HTPC_FV_SOLVER: step failed: inadmissable predicted solution', VERB_NOISY)
      case default
        call this%log%info('HTPC_FV_SOLVER: step failed: unrecognized status: '//i_to_c(stat), VERB_NOISY)
      end select
    end if

    call stop_timer('heat')

  end subroutine step


  subroutine commit_pending_state(this)
    class(htpc_fv_solver), intent(inout) :: this
    INSIST(this%state_is_pending)
    call this%integ%commit_state(this%t, this%u)
    this%state_is_pending = .false.
  end subroutine commit_pending_state

  !! This delegates to the IDAESOL integration driver.  A target time (TOUT)
  !! and/or (maximum) number of steps (NSTEP) is specified and the driver
  !! integrates until the target time or number of steps has been reached.
  !! The driver will adjust the time step as needed, and attempt to recover
  !! from failed steps by decreasing the time step if necessary.  The minimum
  !! and maximum step sizes (HMIN/HMAX) can be specified; if not, there is no
  !! limit.  The maximum number of attempts (MTRY) at a time step can also be
  !! specified; it defaults to a reasonable value.  The integration status is
  !! returned in STATUS; the possible values from IDAESOL are exported (see
  !! above).  The input value of HNEXT is the initial time step the driver
  !! will attempt to use.  Its return value is the time step the driver would
  !! use on the next step if it were continuing to integrate.  For the first
  !! call, HNEXT should be set to the (user-specified) initial time step, but
  !! thereafter the return value should normally be used for the next call.
  !! It permissible to change it, but there is little reason to do so in this
  !! multi-step driver scenario.
  subroutine integrate(this, hnext, status, nstep, tout, hmin, hmax, mtry)
    class(htpc_fv_solver), intent(inout) :: this
    real(r8), intent(inout) :: hnext
    integer, intent(out) :: status
    integer,  intent(in), optional :: nstep, mtry
    real(r8), intent(in), optional :: tout, hmin, hmax
    call this%integ%integrate(hnext, status, nstep, tout, hmin, hmax, mtry)
    call this%integ%get_last_state_copy(this%u)
  end subroutine integrate

  !TODO: Implement these
  ! !! These subroutines save the current mutable state of the HTPC_FV_SOLVER object
  ! !! to an internal cache for later restoration.
  ! !! TODO: The save/restore of the preconditioner state might be more properly
  ! !! invoked by the IDAESOL component which manages it through the IDAESOL_MODEL
  ! !! object.
  ! subroutine save_state(this)
  !   class(htpc_fv_solver), intent(inout) :: this
  !   if (associated(this%precon)) call this%precon%save_state
  !   call this%integ%save_state
  !   if (.not.associated(this%cache)) allocate(this%cache)
  !   call copy_state(this, this%cache)
  ! end subroutine save_state

  ! subroutine restore_state(this)
  !   class(htpc_fv_solver), intent(inout) :: this
  !   if (associated(this%precon)) call this%precon%restore_state
  !   call this%integ%restore_state
  !   if (associated(this%cache)) call copy_state(this%cache, this)
  ! end subroutine restore_state

  ! subroutine copy_state(src, dest)
  !   class(htpc_fv_solver), intent(in) :: src
  !   class(htpc_fv_solver), intent(inout) :: dest
  !   dest%t = src%t
  !   dest%u = src%u
  ! end subroutine copy_state

end module htpc_fv_solver_type
