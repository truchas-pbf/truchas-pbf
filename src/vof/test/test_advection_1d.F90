!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program test_advection_1d

#ifdef NAGFOR
  use,intrinsic :: f90_unix, only: exit
#endif
  use,intrinsic :: iso_fortran_env, only: output_unit
  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use mpi
  use amrex_base_module
  use parameter_list_type
  use truchas_amrex_init_proc
  use amrex_mesh_type
  use logging_services
  implicit none

  integer :: nproc, this_rank, ierr, status = 0
  type(parameter_list) :: params
#if defined(__GFORTRAN__)
  procedure(amrex_finalize), pointer :: pp
#endif
  real(r8), parameter :: PI = 3.1415926535897932_r8

  call truchas_amrex_init(params)

#if defined(__GFORTRAN__)
  pp => amrex_finalize
  call LS_initialize([output_unit], verbosity=LS_VERB_NOISY, finalize=pp)
#else
  call LS_initialize([output_unit], verbosity=LS_VERB_NOISY, finalize=amrex_finalize)
#endif

  call MPI_Comm_rank(MPI_COMM_WORLD, this_rank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)

  call advection_test()

  call amrex_finalize()
  call exit(status)

contains

  subroutine advection_test()

    use region_class
    use material_geometry_type
    use region_factories, only: alloc_fill_region
    use scalar_func_class
    use scalar_func_containers, only: scalar_func_box
    use scalar_func_factories, only: alloc_const_scalar_func, alloc_piecewise_scalar_func
    use vof_init
    use vof_solver_type

    type(parameter_list) :: params
    type(amrex_mesh) :: mesh
    type(material_geometry) :: matl_geom
    type(region_box) :: rgn(2)
    type(scalar_func_box) :: subfunc(2)
    class(scalar_func), allocatable :: matl_index
    type(amrex_multifab) :: vof

    ! set up mesh
    call params%set('lo', [0,0,0])
    call params%set('hi', [31,31,31])
    call params%set('box-size', 16)
    call params%set('prob-lo', [0.0_r8, 0.0_r8, 0.0_r8])
    call params%set('prob-hi', [1.0_r8, 1.0_r8, 1.0_r8])
    call mesh%init(params)

    ! set up pure liquid domain
    call alloc_fill_region(rgn(1)%r)
    call alloc_fill_region(rgn(2)%r)

    call alloc_const_scalar_func(subfunc(1)%f, 1.0_r8)
    call alloc_const_scalar_func(subfunc(2)%f, 2.0_r8)

    call alloc_piecewise_scalar_func(matl_index, subfunc, rgn)
    call matl_geom%init(matl_index)

    ! initialize vof
    call mesh%multifab_build(vof, nc=1, ng=1)
    call vof_initialize(mesh, matl_geom, 6, 2, vof)

    ! run tests
    call advection_dir_test(1, vof, mesh)

  end subroutine advection_test

  subroutine advection_dir_test(dir, vof, mesh)

    use vof_solver_type

    integer, intent(in) :: dir
    type(amrex_multifab), intent(in) :: vof
    type(amrex_mesh), intent(in) :: mesh

    type(parameter_list) :: vof_params
    type(amrex_multifab) :: fluxing_velocity(3)
    real(r8), pointer :: volfluxx_data(:,:,:,:), volfluxy_data(:,:,:,:), volfluxz_data(:,:,:,:)
    type(vof_solver) :: vofsolver
    type(amrex_mfiter) :: mfi
    type(amrex_box) :: bx
    integer :: ix,iy,iz, n
    real(r8) :: dt, max_vel, linf, linf_global, volflux_ex
    logical, allocatable :: fluid_in_box(:)

    allocate(fluid_in_box(size(mesh%box)))
    fluid_in_box = .true.

    ! initialize fluxing velocity = everywhere 1 in direction dir and 0 otherwise
    call mesh%multifab_build(fluxing_velocity(1), nc=1, ng=1, nodal=xface_nodal)
    call mesh%multifab_build(fluxing_velocity(2), nc=1, ng=1, nodal=yface_nodal)
    call mesh%multifab_build(fluxing_velocity(3), nc=1, ng=1, nodal=zface_nodal)

    call fluxing_velocity(1)%setval(1.0_r8)
    call fluxing_velocity(2)%setval(0.8_r8)
    call fluxing_velocity(3)%setval(0.6_r8)

    do n = 1,3
      call fluxing_velocity(n)%override_sync(mesh%geom, mesh%face_owner(n))
      call fluxing_velocity(n)%fill_boundary(mesh%geom)
    end do

    ! calculate advection step
    max_vel = max(fluxing_velocity(1)%max(1),fluxing_velocity(2)%max(1),fluxing_velocity(3)%max(1))
    dt = 0.5_r8 * minval(mesh%geom%dx) / max_vel / 2
    call vofsolver%init(mesh, vof_params)
    call vofsolver%compute_volume_flux(dt, fluid_in_box, fluxing_velocity, vof)

    ! find max error
    volflux_ex = dt * max_vel * mesh%geom%dx(2) * mesh%geom%dx(3)
    linf = 0; n = 0
    call mesh%mfiter_build(mfi)
    do while (mfi%next())
      n = n + 1
      bx = mfi%tilebox()

      volfluxx_data => vofsolver%volume_flux(1)%dataptr(mfi)
      volfluxy_data => vofsolver%volume_flux(2)%dataptr(mfi)
      volfluxz_data => vofsolver%volume_flux(3)%dataptr(mfi)

      do iz = bx%lo(3),bx%hi(3)+1
        do iy = bx%lo(2),bx%hi(2)+1
          do ix = bx%lo(1),bx%hi(1)+1
            if (iy <= bx%hi(2) .and. iz <= bx%hi(3)) linf = &
                max(linf, abs(volfluxx_data(ix,iy,iz,1) - volflux_ex) / volflux_ex)
            if (ix <= bx%hi(1) .and. iz <= bx%hi(3)) linf = &
                max(linf, abs(volfluxy_data(ix,iy,iz,1) - 0.8_r8*volflux_ex) / (0.8_r8*volflux_ex))
            if (ix <= bx%hi(1) .and. iy <= bx%hi(2)) linf = &
                max(linf, abs(volfluxz_data(ix,iy,iz,1) - 0.6_r8*volflux_ex) / (0.6_r8*volflux_ex))
          end do
        end do
      end do
    end do

    call MPI_Reduce(linf, linf_global, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD, ierr)

    if (this_rank == 0) then
      print "(a,es15.5)", 'linf: ', linf_global
      if (linf_global /= 0) status = 1
    end if
    call MPI_Bcast(status, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)

    ! call amrex_string_build(label(1), 'temp')
    ! call amrex_write_plotfile('plt0', 1, [vof], label, [mesh%geom], 0.0_r8, [1], [1])

  end subroutine advection_dir_test

end program test_advection_1d
