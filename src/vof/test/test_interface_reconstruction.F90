!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program test_interface_reconstruction

#ifdef NAGFOR
  use,intrinsic :: f90_unix, only: exit
#endif
  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use outward_volflux_function
  use plane_type
  implicit none

  integer :: status = 0
  type(plane) :: P, plic
  type(multimat_cell) :: cell

  call cell%init([0.0_r8, 0.0_r8, 0.0_r8], [1.0_r8, 1.0_r8, 1.0_r8], [0.5_r8, 0.5_r8])

  P%normal = [1.0_r8, 0.0_r8, 0.0_r8]
  P%normal = P%normal / norm2(P%normal)
  P%rho = 0.5_r8

  print *, cell%volume_behind_plane(P)

  plic = cell%interface_reconstruction(P%normal, 0.5_r8)

  print *, plic%rho

  print *

  P%normal = 1 / sqrt(3.0_r8)
  !P%normal = P%normal / norm2(P%normal)
  plic = cell%interface_reconstruction(P%normal, 0.125_r8)

  print "(es14.4, '       ', 3es14.4)", plic%rho, plic%normal
  plic%rho = (6 * 0.125_r8)**(1/3.0_r8) / sqrt(3.0_r8)
  print "(es14.4, '       ', 3es14.4)", plic%rho, plic%normal
  print *, product(plic%rho / plic%normal) / 6, cell%volume_behind_plane(plic)

  call exit(status)

end program test_interface_reconstruction
