!!
!! LOCATE_PLANE_ND_MODULE
!!
!! This module provides a plane reconstruction
!! subroutine for the nested dissection method.
!!
!! Zechariah J. Jibben <zjibben@lanl.gov>
!! March 2016
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#include "f90_assert.fpp"

module locate_plane_function

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use plane_type
  use brent_root_class
  use truncation_volume_type
  implicit none
  private

  public :: locate_plane

  ! define type for error function
  type, extends(brent_root) :: vof_error_func
    real(r8) :: target_volume, cell_volume
    type(truncation_volume) :: trunc_vol
  contains
    procedure :: init => func_init
    procedure :: f => func_signed_eval
  end type vof_error_func

contains

  ! given a cell, normal, and a volume, return a plane that cuts off that volume in that cell
  type(plane) function locate_plane(nodex, cell_volume, normal, target_vol, cutvof, maxiter)

    real(r8), intent(in) :: nodex(:,:), cell_volume, normal(:), target_vol, cutvof
    integer, intent(in), optional :: maxiter

    real(r8) :: rho_min, rho_max
    integer :: ierr
    type(vof_error_func) :: vof_error

    ASSERT(size(normal)==3)

    call vof_error%init(nodex, normal, target_vol, cell_volume)
    call rho_bracket(vof_error, normal, nodex, rho_min, rho_max, ierr) ! get rho bounds
    INSIST(ierr == 0)

    ! start Brent's iterations
    ! note ~30 iterations seem to be necessary to pass current unit tests
    locate_plane%normal = normal
    vof_error%feps = cutvof / 2; vof_error%maxitr = 50
    if (present(maxiter)) vof_error%maxitr = maxiter
    call vof_error%find_root(rho_min, rho_max, locate_plane%rho, ierr)
    INSIST(ierr == 0)

  end function locate_plane

  ! loop through all the cell vertices, checking if a plane
  ! intersecting each one is above or below the target volume,
  ! thereby bracketing the allowed range of plane constants
  subroutine rho_bracket(vof_error, norm, nodex, rho_min, rho_max, ierr)

    type(vof_error_func), intent(inout) :: vof_error
    real(r8), intent(in) :: norm(:), nodex(:,:)
    real(r8), intent(out) :: rho_min, rho_max
    integer, intent(out) :: ierr

    real(r8) :: err_min,err_max,err, rho
    integer :: i
    integer, parameter :: iter_max = 10

    ierr = 0
    err_min = -huge(1.0_r8); rho_min = -huge(1.0_r8)
    err_max =  huge(1.0_r8); rho_max =  huge(1.0_r8)

    ! find the outer bounds
    do i = 1, size(nodex, dim=2)
      rho = dot_product(nodex(:,i), norm)

      if (rho <= rho_min .or. rho >= rho_max) cycle

      err = vof_error%f(rho)

      if (0 < err .and. err < err_max) then
        err_max = err
        rho_max = rho
      else if (err_min < err .and. err < 0) then
        err_min = err
        rho_min = rho
      else if (err == 0) then
        err_min = err
        err_max = err

        rho_min = rho
        rho_max = rho
        exit
      end if
    end do

    ! ensure the bounds were set
    if (.not.(rho_min /= -huge(1.0_r8) .and. rho_max /= huge(1.0_r8))) ierr = 1
    ! if (rho_min==-huge(1.0_r8) .or. rho_max==huge(1.0_r8)) then
    !   print '(a,3f10.3)', 'normal: ', norm
    !   do i = 1,size(volume_error%x, dim=2)
    !     rho = dot_product(volume_error%x(:,i),norm)
    !     err = volume_error%signed_eval(rho)
    !     print '(a,i3,3es14.4)', 'vertex, rho, error: ', i, rho, err
    !   end do
    !   print '(a,2es14.4)', 'rho min,max: ', rho_min, rho_max
    !   ierr = 1
    ! end if

  end subroutine rho_bracket

  !! error function procedures

  subroutine func_init(this, nodex, normal, target_volume, cell_volume)
    class(vof_error_func), intent(out) :: this
    real(r8), intent(in) :: nodex(:,:), normal(:), target_volume, cell_volume
    call this%trunc_vol%init(nodex, normal)
    this%target_volume = target_volume
    this%cell_volume = cell_volume
  end subroutine func_init

  real(r8) function func_signed_eval(this, x)
    class(vof_error_func), intent(inout) :: this
    real(r8), intent(in) :: x
    func_signed_eval = (this%trunc_vol%volume(x) - this%target_volume) / this%cell_volume
  end function func_signed_eval

end module locate_plane_function
