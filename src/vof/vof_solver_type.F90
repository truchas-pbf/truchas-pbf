!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module vof_solver_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use amrex_mesh_type
  use amrex_base_module, only: amrex_mfiter, amrex_multifab
  use vof_box_type
  use timer_tree_type
  implicit none
  private

  type, public :: vof_solver
    type(amrex_mesh), pointer :: mesh => null() !! reference only -- not owned
#ifdef GNU_PR82996
    type(amrex_multifab), allocatable :: volume_flux(:)
#else
    type(amrex_multifab) :: volume_flux(3)
#endif
    integer :: nsubcycle = 2
    type(vof_box), allocatable :: box(:)
    real(r8) :: dt
  contains
    procedure :: init
    procedure :: compute_interface_normal
    procedure :: compute_volume_flux
    procedure :: advect
    procedure :: advect_delta
  end type vof_solver

contains

  ! TODO: put this in a central location
  subroutine get_component_ptr(dp, n, cdp)
    real(r8), pointer, contiguous, intent(in) :: dp(:,:,:,:)
    integer, intent(in) :: n
    real(r8), pointer, contiguous, intent(out) :: cdp(:,:,:)
    cdp(lbound(dp,1):,lbound(dp,2):,lbound(dp,3):) => dp(:,:,:,n)
  end subroutine get_component_ptr

  subroutine init(this, mesh, params)

    use parameter_list_type

    class(vof_solver), intent(out) :: this
    type(amrex_mesh), target, intent(in) :: mesh
    type(parameter_list), intent(inout) :: params

    type(amrex_mfiter) :: mfi
    integer :: n
    real(r8), pointer, contiguous :: volfluxx(:,:,:) => null(), volfluxy(:,:,:) => null(), &
        volfluxz(:,:,:) => null()

#ifdef GNU_PR82996
    allocate(this%volume_flux(3))
#endif

    this%mesh => mesh

    call this%mesh%multifab_build(this%volume_flux(1), nc=1, ng=1, nodal=xface_nodal)
    call this%mesh%multifab_build(this%volume_flux(2), nc=1, ng=1, nodal=yface_nodal)
    call this%mesh%multifab_build(this%volume_flux(3), nc=1, ng=1, nodal=zface_nodal)

    allocate(this%box(mesh%nbox))
    call this%mesh%mfiter_build(mfi)
    n = 0
    do while (mfi%next())
      n = n + 1

      call get_component_ptr(this%volume_flux(1)%dataptr(mfi), 1, volfluxx)
      call get_component_ptr(this%volume_flux(2)%dataptr(mfi), 1, volfluxy)
      call get_component_ptr(this%volume_flux(3)%dataptr(mfi), 1, volfluxz)

      call this%box(n)%init(this%mesh%geom%dx, mesh%box(n), volfluxx, volfluxy, volfluxz)
    end do

  end subroutine init

  subroutine advect(this, scalar)

    class(vof_solver), intent(in) :: this
    type(amrex_multifab), intent(inout) :: scalar

    type(amrex_multifab) :: scalar_delta

    call this%mesh%multifab_build(scalar_delta, nc=1, ng=1)
    call this%advect_delta(scalar, scalar_delta)
    call scalar%add(scalar_delta, 1, 1, 1, 0)
    call scalar%fill_boundary(this%mesh%geom)

  end subroutine advect

  subroutine advect_delta(this, scalar, scalar_delta)

    class(vof_solver), intent(in) :: this
    type(amrex_multifab), intent(in) :: scalar
    type(amrex_multifab), intent(inout) :: scalar_delta

    real(r8), pointer, contiguous :: scalarp(:,:,:), scalar_deltap(:,:,:)
    type(amrex_mfiter) :: mfi
    integer :: n

    call this%mesh%mfiter_build(mfi)
    n = 0
    do while (mfi%next())
      n = n + 1

      call get_component_ptr(scalar%dataptr(mfi), 1, scalarp)
      call get_component_ptr(scalar_delta%dataptr(mfi), 1, scalar_deltap)

      call this%box(n)%advect_delta(scalarp, scalar_deltap)
    end do
    !FIXME: scalar_data expected to have 1 ghost, but ghost values are not set

  end subroutine advect_delta

  subroutine compute_volume_flux(this, dt, fluid_in_box, fluxing_velocity, vol_frac)

    class(vof_solver), intent(inout) :: this
    real(r8), intent(in) :: dt
    logical, intent(in) :: fluid_in_box(:)
    type(amrex_multifab), intent(in) :: fluxing_velocity(3), vol_frac

    type(amrex_mfiter) :: mfi
    type(amrex_multifab) :: interface_normal
    real(r8), pointer, contiguous :: volfrac(:,:,:) => null(), int_norm(:,:,:,:) => null(), &
        velfx(:,:,:) => null(), velfy(:,:,:) => null(), velfz(:,:,:) => null(), &
        volfluxx(:,:,:) => null(), volfluxy(:,:,:) => null(), volfluxz(:,:,:) => null()
    integer :: n, d
    real(r8) :: subdt

    call start_timer('volume flux')

    this%dt = dt

    call this%volume_flux(1)%setval(0.0_r8)
    call this%volume_flux(2)%setval(0.0_r8)
    call this%volume_flux(3)%setval(0.0_r8)

    call this%mesh%multifab_build(interface_normal, nc=3, ng=1)

    ! Perform pseudo-subcycling. Since we only have liquid & solid,
    ! the volume fractions don't change. This means the the interface
    ! normals don't need to be updated every subcycle. It also means
    ! the volflux from each subcycle is the same, so we only need to
    ! compute one subcycle and scale it.
    subdt = dt / this%nsubcycle

    call this%compute_interface_normal(vol_frac, interface_normal)

    call this%mesh%mfiter_build(mfi)
    n = 0
    do while (mfi%next())
      n = n + 1
      if (.not.fluid_in_box(n)) cycle
      call get_component_ptr(vol_frac%dataptr(mfi), 1, volfrac)

      call get_component_ptr(fluxing_velocity(1)%dataptr(mfi), 1, velfx)
      call get_component_ptr(fluxing_velocity(2)%dataptr(mfi), 1, velfy)
      call get_component_ptr(fluxing_velocity(3)%dataptr(mfi), 1, velfz)

      call get_component_ptr(this%volume_flux(1)%dataptr(mfi), 1, volfluxx)
      call get_component_ptr(this%volume_flux(2)%dataptr(mfi), 1, volfluxy)
      call get_component_ptr(this%volume_flux(3)%dataptr(mfi), 1, volfluxz)

      int_norm => interface_normal%dataptr(mfi)

      call this%box(n)%compute_volume_flux(subdt, volfrac, int_norm, velfx, velfy, velfz, &
          volfluxx, volfluxy, volfluxz)
    end do

    do d = 1, 3
      call this%volume_flux(d)%mult(real(this%nsubcycle, r8), 1, 1, 1) ! scale the subcycle flux
      call this%volume_flux(d)%override_sync(this%mesh%geom, this%mesh%face_owner(d))
      call this%volume_flux(d)%fill_boundary(this%mesh%geom)
    end do

    call stop_timer('volume flux')

  end subroutine compute_volume_flux

  subroutine compute_interface_normal(this, vol_frac, interface_normal)

    class(vof_solver), intent(in) :: this
    type(amrex_multifab), intent(in) :: vol_frac
    type(amrex_multifab), intent(inout) :: interface_normal

    integer :: n
    type(amrex_mfiter) :: mfi
    real(r8), pointer, contiguous :: volfrac_box(:,:,:) => null(), int_norm_box(:,:,:,:) => null()

    call start_timer('normals')

    call this%mesh%mfiter_build(mfi)
    n = 0
    do while (mfi%next())
      n = n + 1
      call get_component_ptr(vol_frac%dataptr(mfi), 1, volfrac_box)
      int_norm_box => interface_normal%dataptr(mfi)

      call this%box(n)%compute_interface_normal(volfrac_box, int_norm_box)
    end do

    call interface_normal%fill_boundary(this%mesh%geom)

    call stop_timer('normals')

  end subroutine compute_interface_normal

end module vof_solver_type
