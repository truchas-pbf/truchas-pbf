# Check if a python package is available, send fatal error if it isn't,
# or if the found version is less than a given minimum.

function(find_python_package PACKAGE)

  set(options REQUIRED)
  set(oneValueArgs PACKAGE MIN_VERSION)
  set(multiValueArgs)
  cmake_parse_arguments(FIND_PYTHON_PACKAGE "${options}" "${oneValueArgs}"
    "${multiValueArgs}" ${ARGN})

  if(NOT FIND_PYTHON_PACKAGE_REQUIRED)
    set(exit_status WARNING)
  else()
    set(exit_status FATAL_ERROR)
  endif()

  if(NOT DEFINED PYTHON_EXECUTABLE)
    message(STATUS "${PYTHON_EXECUTABLE}")
    message(${exit_status} "Python executable not defined. Could not search for ${PACKAGE}")
    set(exit_code 1)
  else()
    execute_process(
      COMMAND ${PYTHON_EXECUTABLE} -c
      "import ${PACKAGE}; print(${PACKAGE}.__version__, end='')"
      RESULT_VARIABLE exit_code
      OUTPUT_VARIABLE found_package_version
      ERROR_QUIET)
  endif()

  if(exit_code)
    message(${exit_status} "Python module ${PACKAGE} not found")
    set(python_${PACKAGE}_FOUND FALSE PARENT_SCOPE)
  elseif(found_package_version VERSION_LESS FIND_PYTHON_PACKAGE_MIN_VERSION)
    message(${exit_status} "Could NOT find Python module ${PACKAGE}: Found \
unsuitable version \"${found_package_version}\", but required is at least \
\"${FIND_PYTHON_PACKAGE_MIN_VERSION}\"")
    set(python_${PACKAGE}_FOUND FALSE PARENT_SCOPE)
  else()
    message(STATUS "Found Python module ${PACKAGE} (found version ${found_package_version})")
    set(python_${PACKAGE}_FOUND TRUE PARENT_SCOPE)
  endif()

endfunction(find_python_package)
