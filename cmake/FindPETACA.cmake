# This module finds the Fortran Petaca library. If successful, it defines the
# imported library target petaca. It is generally sufficient to include petaca
# as a target link library; CMake will automatically handle adding the compile
# include flags needed for the .mod files, as well as adding dependent link
# libraries (yajl).
#
# To provide the module with hints on where to find your Petaca and YAJL
# installations you have several options. You can include the root installation
# directories in the setting of the CMAKE_PREFIX_PATH variable, or you can set
# the environment variables or CMake variables PETACA_ROOT and YAJL_ROOT.

find_package(YAJL "2.0.4" REQUIRED)

if(NOT PETACA_ROOT)
  set(PETACA_ROOT $ENV{PETACA_ROOT})
endif()
if(PETACA_ROOT)
  set(petaca_search_opts NO_DEFAULT_PATH)
else()
  set(petaca_search_opts)
endif()

find_library(PETACA_LIBRARY petaca HINTS ${PETACA_ROOT}
             ${petaca_search_opts} PATH_SUFFIXES lib)

# Module files are installed with the library file.
if(PETACA_LIBRARY)
  get_filename_component(PETACA_INCLUDE_DIR ${PETACA_LIBRARY} DIRECTORY)
endif()

# No version number is currently available
set(PETACA_VERSION PETACA_VERSION-NOTFOUND)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PETACA REQUIRED_VARS PETACA_LIBRARY)

if(PETACA_FOUND)
  set(PETACA_LIBRARIES ${PETACA_LIBRARY} ${YAJL_LIBRARY})
  set(PETACA_INCLUDE_DIRS ${PETACA_INCLUDE_DIR})
  if(NOT TARGET petaca)
    add_library(petaca UNKNOWN IMPORTED)
    set_target_properties(petaca PROPERTIES
        IMPORTED_LOCATION "${PETACA_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${PETACA_INCLUDE_DIRS}"
        INTERFACE_LINK_LIBRARIES YAJL::YAJL)
  endif()
endif()
