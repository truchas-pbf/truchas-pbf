Truchas-PBF
================================================================================
Truchas-PBF is a spin-off of the open source Truchas code that is being
developed specifically for modeling metal powder bed fusion (PBF) additive
manufacturing processes. It is based on the AMReX block-structured adaptive
mesh refinement (AMR) library, although at this time it is limited to using
static block-structured AMR meshes.

## Building Truchas-PBF (Updated Apr 2024)

We currently build on Linux x86_64 platforms using either the Intel OneAPI
compilers or GNU compilers. We currently use:
* Intel OneAPI 2024.1 ifx and icx/icpx compilers.
* Gfortran and gcc/g++ version 12.3.0.
* Apple Clang 12+ with Gfortran 12.3.0 or 12.4.0.

Somewhat older versions of those compilers may work as well.

### Building via Spack

We provide a Spack repo for easy building of dependencies. All that is needed is
an acceptable compiler combination in your `compilers.yaml`. Then, add our Spack
repo, create an environment, and build the dependencies. You may want to add
some postprocessing tools to your environment. Here are the steps:

```sh
spack repo add /path/to/truchas-pbf/spack-repo
spack env create tpbf-env
spack env activate tpbf-env

### OPTIONAL POSTPROCESSING TOOLS ###
spack add py-scipy
spack add py-matplotlib
spack add py-yt -astropy
spack install
### OPTIONAL POSTPROCESSING TOOLS ###

spack add truchas-pbf
spack install --only dependencies
```

After this, skip to the "Build Truchas-PBF" instructions below.

### Dependencies

* MPI. In principle any MPI implementation should work. Currently MPICH 4.1
  (https://mpich.org) and OpenMPI 4.1 (https://www.open-mpi.org) are tested.
  The version you use must have been compiled using the same compilers you will
  use to build Hypre, AMReX, and Truchas-PBF below.

* YAJL version 2.0.4 or later. This JSON parsing library is available as a
  standard binary package in most any Linux distribution. If necessary, the
  source is available at http://lloyd.github.io/yajl/.

* CMake version 3.20.2 or later. This is also a standard binary package in any
  Linux distribution. If necessary, it can be very easily built from source
  available at https://cmake.org/download.

### Step-by-Step Instructions

These instructions assume that the MPI compiler wrappers are in your path
(`mpicc`, `mpicxx`, `mpifort`).

#### Build and Install Hypre
Cd to the directory where you want to build, and download Hypre v2.31.0 from
https://github.com/hypre-space/hypre/releases/tag/v2.31.0 into that directory.
Decide where you want to install Hypre; the following instructions will refer
to that directory as `/your/hypre/root`. Note that you cannot move the files
to a new location after they have been installed.
```
tar zxf hypre-2.31.0.tar.gz
cd hypre-2.31.0/src
./configure CC=mpicc CXX=mpicxx FC=mpifort --prefix=/your/hypre/root
make
make install
```
Hypre versions 2.29.0 or later should work as well.

#### Build and Install AMReX
Cd to the directory where you want to build, and decide where you want to
install AMReX. The following instructions will refer to that directory as
`/your/amrex/root`. Again, you cannot move the files after they have been
installed. First clone the AMReX repository and checkout the desired version
(tag). Any relatively recent version, 23.12 or later, on the default branch
should work; we currently test with the latest 24.04 version. Then continue
to configure, compile, and install:

```
git clone https://github.com/AMReX-Codes/amrex.git
cd amrex
git checkout 24.04
mkdir build
cd build
cmake -D AMReX_FORTRAN=YES \
      -D AMReX_FORTRAN_INTERFACES=YES \
      -D AMReX_HYPRE=YES \
      -D AMReX_PLOTFILE_TOOLS=YES \
      -D HYPRE_ROOT=/your/hypre/root \
      -D CMAKE_INSTALL_PREFIX=/your/amrex/root \
      -D CMAKE_BUILD_TYPE=Release \
      ..
make
make install
```

#### Build and Install Petaca
cd to the directory where you want to build, and decide where you want to
install Petaca.  The following instructions will refer to that directory as
`/your/petaca/root`. Ensure that your `FC` and `CC` environment variables
are set to your Fortran and C compiler executables.
```
git clone https://github.com/nncarlson/petaca.git
cd petaca
git checkout v23.11
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/your/petaca/root ..
make
make install
```
Note that Truchas-PBF has not been updated yet to use the latest Petaca
version, so the `git checkout v23.11` is essential.

#### Build Truchas-PBF
The `cmake` step below needs to be told where to find your Hypre, AMReX,
Petaca installations. There are two ways to do this. One is to set the and
environment variables `HYPRE_ROOT`, `AMREX_ROOT`, and `PETACA_ROOT` to the
installation directories you used above. The following instructions assume
you have done this. An alternative is to specify the locations on the `cmake`
command line with arguments such as `-D HYPRE_ROOT=/your/hypre/root`.

Ensure that your `FC`, `CC`, and `CXX` environment variables are set to
your Fortran, C and C++ compiler executables. **Do not use the MPI compiler
wrappers here.** (Or set the `CMAKE_Fortran_COMPILER`, `CMAKE_C_COMPILER`,
and `CMAKE_CXX_COMPILER` CMake variables on the `cmake` command line.)
```
cd /your/build/directory
git clone https://gitlab.com/truchas-pbf/truchas-pbf.git
cd truchas-pbf
mkdir build
cd build
cmake ..
make
```
The usual CMake variables can be set on the `cmake` command line, such as
`CMAKE_INSTALL_PREFIX`. The default build type is `Release`. CMake doesn't
necessarily do a good job of setting appropriate Fortran compiler flags for
the different build types, so you may want to specify them explicitly with
the `CMAKE_Fortran_FLAGS` variable, or alternatively, use one of the platform
config files described next.

##### Platform cmake config files
The `config` subdirectory contains some CMake config files that set the
compilers and suggested compiler flags for the `Debug` and `Release` build
types for CMake. To use them, replace the `cmake` command above with
```
cmake .. -C ../config/linux-gcc.cmake -DCMAKE_BUILD_TYPE=Debug
```
for a debug build, or
```
cmake .. -C ../config/linux-gcc.cmake
```
for an optimized build (the default) using the GNU GCC compilers. There is an
analogous config file for the Intel compilers.

### Testing the Truchas-PBF Build

There is a collection of unit tests that test many of the lower level
components of Truchas-PBF. There are also integration tests running the `tpbf`
executable and testing the output. These are both hooked into the CTest test
harness. To run them, cd to the cmake build directory (where you ran `cmake`)
and simply give the command `ctest`. All tests should pass.

Integration tests rely on Python 3 and the `yt` python package. If you don't
have this installed, you can install with:

```
pip3 install --user yt
```

Make sure no modules are loaded, so any libraries are compiled with the system
gcc.

### Running Examples

Currently the main Truchas-PBF executable is called `tpbf` and it is located
in the `htpc` subdirectory of the cmake build directory.  This executable
simulates coupled heat transfer, phase change, and in the melt pool, fluid
flow. A variety of boundary conditions are available, and it supports fairly
general functions for material properties and boundary condition data.

Running `tpbf` goes like this:
```
mpiexec -np <nproc> /path/to/tpbf <input-file>
```
It takes a single argument which is the name of an input file.  The input
file is a JSON format file.  Examples can be found in the subdirectories
of the `examples` directory.

A simulation generates a bunch of `plt*` directories that contain VisIt
visualization files (these are generated by AMReX).  See
https://amrex-codes.github.io/amrex/docs_html/Visualization_Chapter.html
for info and tips, especially for how to combine the multiple directories
into a single header file.

#### Input File Format
Coming ...
