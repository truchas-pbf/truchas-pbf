// Spot weld demo problem (5 micron resolution)
//
// Quarter symmetry. Uses laser properties and In625 material properties from
// the NIST single trace demo problem. Laser on for 2 ms. Surface tension
// coefficient reduced by a factor of 10.
//
// Model is posed in mm, g, ms, kelvin units.
//
// Conversion factors from MKS units:
//   1 Joule [kg m^2 / s^2] = 1e3 g mm^2 / ms^2
//   1 Watt [J/s] = 1 [g mm^2 / ms^3]
//   1 W/m^2 = 1e-6 [g mm^2 / s^3 mm^2]
//   1 N [kg m / s^2] = 1 g mm / ms^2
//   1 Pa [N/m^2] = 1e-6 (g-mm/ms^2)/mm^2
//   1 Pa-s = 1 kg / m s = 1e-3 g/mm-ms

{
   "physical-constants": {"stefan-boltzmann": 5.67e-14},
   "mesh": {
      "prob-lo": [0.0,  0.0, -0.96],
      "prob-hi": [0.96, 0.96, 0.0],
      "box-size": 8,

      // 10 um resolution / 7 sec (AMD 2920X, 12 processes)
      //"num-levels": 5,
      //"lo": [0,0,0,  0,0,4,   0,0,12,    0,0,34,   0,0,78],
      //"hi": [5,5,5,  7,7,11,  11,11,23,  15,15,47, 21,21,95]

      // 5 um resolution / 22 sec
      "num-levels": 6,
      "lo": [0,0,0,  0,0,4,   0,0,12,    0,0,34,   0,0,78,   0,0,166],
      "hi": [5,5,5,  7,7,11,  11,11,23,  15,15,47, 21,21,95, 31,31,191]

      // 2.5 um resolution / 500 sec
      //"num-levels": 7,
      //"lo": [0,0,0,  0,0,4,   0,0,12,    0,0,34,   0,0,78,   0,0,166,   0,0,338],
      //"hi": [5,5,5,  7,7,11,  11,11,23,  15,15,47, 21,21,95, 31,31,191, 57,57,383]

      // 1.25 um resolution / 200 min
      //"num-levels": 8,
      //"lo": [0,0,0,  0,0,4,   0,0,12,    0,0,34,   0,0,78,   0,0,166,   0,0,338,  0,0,680],
      //"hi": [5,5,5,  7,7,11,  11,11,23,  15,15,47, 21,21,95, 31,31,191, 57,57,383, 111,111,767]
   },
   "laser-scan-path": {
      "laser": {
         "type": "gaussian",
         "power": 100.0, // [W]
         "sigma": 0.035  // [mm]
      },
      "laser-absorp": 0.4,
      "laser-time-constant": 0.01,
      "scan-path": {
         "start-coord": [0.0, 0.0],
         "command-string": "[[\"setflag\",0],[\"dwell\",2.0],[\"clrflag\",0]]"
      }
   },
   "model": {
      "material": {
         "density": 7.57e-3,
         "solid": {
            "specific-heat": {"type":"polynomial", "poly-center":273.0,
                              "poly-coef":[428.38, 0.23638], "poly-powers":[0, 1]},
            "conductivity": {"type":"polynomial", "poly-center":270.0,
                             "poly-coef":[12.3e-3, 1.472e-5], "poly-powers":[0, 1]}
         },
         "liquid": {
            "specific-heat": 750.65,
            "conductivity": {"type":"polynomial", "poly-center":270.0,
                             "poly-coef":[8.92e-3, 1.474e-5], "poly-powers":[0, 1]},
            "viscosity": {"type": "polynomial", "poly-center": 273.0,
                          "poly-coef": [4.0493e-6, -1.9397e-2, 34.725],
                          "poly-powers": [0, -1, -2]},
            "density-deviation": {"type": "polynomial", "poly-center": 273.0,
                                  "poly-coef": [0.13657, -9.0197e-5, -7.9917e-9],
                                  "poly-powers": [0, 1, 2]}
         },
         "sigma-func-type": "smooth-step",
         "solidus-temp": 1410.0, // eutectic
         "liquidus-temp": 1616.0,
         "latent-heat": 2.1754e5  // [g-mm^2/ms^2/g]
      },
      "heat": {
         "bc": {
            "top": {
               "type": "flux",
               "sides": ["zhi"],
               "data": "laser"
            },
            "top-rad": {
               "type": "radiation",
               "sides": ["zhi"],
               "emissivity": 0.6,
               "ambient-temp": 300.0
            },
            "adiabatic": {
               "type": "flux",
               "sides": ["xlo", "xhi", "ylo", "yhi", "zlo"],
               "data": 0.0
            }
         }
      },
      "flow":{"inviscid":false, "body-force-density":9.81e-3}
      //"disable-flow": true,
   },
   "solver": {
      "microstructure": {
         "exaca": {"liquidus-temp": 1616.0},
	 "gv":{ "theta1": 0.05, "theta2":0.95, "theta": 0.1}
      },
      "heat": {
         "temp-rel-tol": 1.0e-2, // 1.0e-2 to run faster; 1.0e-3 is better
         "num-cycles": 4,
         "nlk-max-itr": 5,
         "nlk-tol": 0.01
      },
      "flow":{
         "viscous-implicitness": 1.0,
         "courant-number": 0.5,
         "bc": {
            "symmetry": {"type": "symmetry", "sides":["xlo","ylo"],
                         "xdata":0.0, "ydata":0.0, "zdata":0.0},
            "solid-sides": {"type": "velocity", "sides":["xhi","yhi","zlo"],
                            "xdata":0.0, "ydata":0.0, "zdata":0.0},
            "surface": {"type":"marangoni", "sides":["zhi"],
                        "dsigma": -0.11e-7} // [(g-mm/ms^2)/(mm-K)]
         },
         "pressure-solver":{
            //"tol": 1e-6,         // hypre default
            //"max-ds-iter": 1000, // hypre default
            //"max-pc-iter": 200,  // hypre default
            //"conv-rate-tol": 0.9 // hypre default
         },
         "viscous-solver":{
            //"tol": 1e-6,         // hypre default
            //"max-ds-iter": 1000, // hypre default
            //"max-pc-iter": 200,  // hypre default
            //"conv-rate-tol": 0.9 // hypre default
         }
      }
   },
   "sim-control": {
      "initial-time": 0.0,
      "initial-time-step": 1.0e-6,
      "min-time-step": 1.0e-9,
      "output-times": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0,
                       1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0,
                       2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0,
                       3.1, 3.2, 3.3, 3.4, 3.5],
      // ExaCA requires solidification data in MKS units
      "exaca-time-scale-factor": 1.0e-3, // seconds per model unit of time
      "exaca-coord-scale-factor": 1.0e-3 // meters per model unit of length
   },
   "initial-temperature": {
      "type": "constant",
      "value": 300.0
   },

   // This sublist is consumed by AMREX_INIT
   "amrex": {
      "amrex": {"fpe_trap_invalid": 1}
   }
}
