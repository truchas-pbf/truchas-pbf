# Spot Weld on Inconel 625 Plate

The "ht" input files solve heat transfer/phase change only, while the
remaining input files include surface tension driven flow in the melt
pool. There are input files for four different resolutions in the
neighborhood of the melt pool:

* 10 micron. Coarse grid. Fast, but solution quality is somewhat lacking,
  especially for flow.
* 5 micron. Standard resolution for resolving heat transfer, phase change,
  and flow in the melt pool. Gives a decent quality solution.
* 2.5 micron. Fine grid. Slower, more so with flow, but gives a high-quality
  solution (unnecessarily so).
* 1.25 micron. Ultra fine grid. Slower still, but extremely slow with flow.

The resolution required by the ExaCA code is finer than that required by
Truchas-PBF. There is currently no tool for interpolating the ExaCA data
produced by Truchas-PBF on its mesh to the finer ExaCA mesh, so the finer
1.25 and 2.5 micron resolution input files can be used to produce ExaCA data
at its desired resolution. 

Note that every halving of the resolution increases the number of degrees of
freedom by a factor of roughly 8, and with flow the time step is halved due
to the flow Courant limit. Run times generally increase accordingly.
