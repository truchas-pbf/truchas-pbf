#!/usr/bin/env python3

# 1D HTC Heat Conduction (X/Y/Z axes)
# This solves $u_t = k u_{xx}$ in $[a,1-a]$ with the HTC boundary conditions
# $k u_x = \alpha (u-1)$ at $x=a$ and $-k u_x = \alpha (u-1)$ at $x=1-a$.
# This has the solution $u(x,t) = 1 + e^{-k\pi^2 t} \sin\pi x$, when the HTC
# $\alpha = k\pi/\tan\pi a$. For this simulation we take $a=1/8$, $k=1/\pi^2$.
#
# We immerse this 1D problem in a 3D problem in the obvious way, solving on
# a cube domain, picking one of the coordinate directions for "x" and applying
# no flux conditions on the other boundaries.
#
# The initial condition is $u(x) = 1 + \sin\pi x$, and the solution is output
# at $t=1/2, 1, 2$.  The maximum of $u$ is $1 + e^{-t}$.

import os
import subprocess
from math import pi
import numpy as np
import truchaspbf as tpbf


nfail = 0
for d in ("x", "y", "z"):
    case = f"{d}htc"
    stdout, output = tpbf.truchas_pbf(4, f"{case}.json")

    # Initial solution
    # yt fails if osid isn't a literal variable.
    # odata = output[sid].all_data() will cause a "ReferenceError: weakly-referenced
    # object no longer exists" exception.
    for osid in output:
        odata = osid.all_data()
        s = odata["index", d].to_ndarray() # need to strip unit information
        
        t = float(osid.current_time)
        u = 1 + np.exp(-t) * np.sin(pi*s)
        if tpbf.linf_above_tol(odata["temp"], u, f"{case}-temp-final", 1e-2): nfail += 1
    print()

tpbf.report_summary(nfail)
assert nfail == 0
