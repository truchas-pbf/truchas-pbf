#!/usr/bin/env python3

import os
import subprocess
import truchaspbf as tpbf


nfail = 0
stdout, output = tpbf.truchas_pbf(4, "nat-conv.json")
golden_dir = os.path.join(tpbf.test_source_dir(), "golden")

time, abs_error, rel_error = tpbf.comparison_dict(".", golden_dir)
for t, a, r in zip(time, abs_error, rel_error):
    if tpbf.report_test(f"temp-{t}", r["temp"], 1e-7, "relmax"): nfail += 1
    if tpbf.report_test(f"pressure-{t}", a["pressure"], 1e-11, "max"): nfail += 1
    if tpbf.report_test(f"velocity-x-{t}", a["velocity-x"], 1e-8, "max"): nfail += 1
    if tpbf.report_test(f"velocity-z-{t}", a["velocity-z"], 1e-8, "max"): nfail += 1
    print()

tpbf.report_summary(nfail)
assert nfail == 0
