#!/usr/bin/env python3

# 1D Heat Conduction on [-1,1] (X/Y/Z axes)
# Using flux-dirichlet, dirichlet-flux, and dirichlet-dirichlet BCs.
# Boundary conditions:
#     FD: 2u'(-1) = 6, u(1) = 3
#     DF: u(-1) = 1, -2u'(1) = 2
# Initial solution: u(s) = 2 + s
# Steady solution:  u(s) = (2 + s) + (1 - s^2)
#                   max u = 3.25 at s = 0.5

import os
import subprocess
import truchaspbf as tpbf


nfail = 0
for bc in ("fd", "df", "dd"):
    for d in ("x", "y", "z"):
        case = f"{d}{bc}"
        stdout, output = tpbf.truchas_pbf(4, f"{case}.json")

        # Initial solution
        # yt fails if osid isn't a literal variable.
        # odata = output[sid].all_data() will cause a "ReferenceError: weakly-referenced
        # object no longer exists" exception.
        osid = output[0]
        odata = osid.all_data()

        s = odata["index", d].to_ndarray() # need to strip unit information

        u = 2 + s
        if tpbf.linf_above_tol(odata["temp"], u, f"{case}-temp-initial", 1e-12): nfail += 1

        # Steady solution
        osid = output[-1]
        odata = osid.all_data()

        u = (2 + s) + (1 - s**2)
        if tpbf.linf_above_tol(odata["temp"], u, f"{case}-temp-final", 1e-2): nfail += 1
        print()

tpbf.report_summary(nfail)
assert nfail == 0
