set(CMAKE_C_COMPILER gcc CACHE STRING "C compiler")
set(CMAKE_CXX_COMPILER g++ CACHE STRING "C++ compiler")
set(CMAKE_Fortran_COMPILER gfortran CACHE STRING "Fortran compiler")
set(CMAKE_Fortran_FLAGS_DEBUG "-fimplicit-none -g -fcheck=all" CACHE
    STRING "Flags used by the compiler during debug builds")
set(CMAKE_Fortran_FLAGS_RELWITHDEBINFO "-fimplicit-none -g -O3 -DNDEBUG"
    CACHE STRING "Flags used by the compiler during release builds")
set(CMAKE_Fortran_FLAGS_RELEASE "-fimplicit-none -O3 -DNDEBUG"
    CACHE STRING "Flags used by the compiler during release builds")
